const mix = require('laravel-mix');

mix.webpackConfig(require('./webpack.config'));

mix.combine([
  'resources/js/libs/1-jquery-3.3.1.min.js',
  'resources/js/libs/2-tiny-slider.js',
  'resources/js/libs/all.fine-uploader.min.js',
  'resources/js/libs/datatables.min.js',
  'resources/js/libs/datepicker.js',
  'resources/js/libs/datepicker.en.js',
  'resources/js/libs/ion.rangeSlider.js',
  'resources/js/libs/jquery.magnific-popup.js',
  'resources/js/libs/lightslider.js',
  'resources/js/libs/PNotify.js',
  'resources/js/libs/PNotifyButtons.js',
  'resources/js/libs/popper.min.js',
  'resources/js/libs/quill.min.js',
  'resources/js/libs/select2.min.js',
  'resources/js/libs/ion.rangeSlider.js',
  'resources/js/libs/tippy.min.js',
  'resources/js/libs/imask.js',
], 'public/js/vendor.js');

mix
  .js('./resources/assets/core/js/spa.js', 'js/core-spa.js')
  .sass('./resources/assets/core/scss/style.scss', 'css/core-style.css')
  .sass('./resources/assets/core/scss/stripe.scss', 'css/stripe.css')

  .js('./resources/assets/admin/js/spa.js', 'js/admin-spa.js')
  .sass('./resources/assets/admin/scss/style.scss', 'css/admin-style.css')

  .js('./resources/js/app.js', 'js/app.js')
  .js('./resources/js/search.js', 'js/search.js')
  .js('./resources/js/tour-detail.js', 'js/tour-detail.js')
  .js('./resources/js/payments.js', 'js/payments.js')
  .js('./resources/js/stripe.js', 'js/stripe.js')

  .version()

mix.copyDirectory('./resources/assets/core/images', 'public/images')

mix.webpackConfig({
  output: {
    chunkFilename: 'js/chunks/[name].[chunkhash].js'
  }
})
