<?php

namespace Database\Factories;

use App\Services\FileService;
use Illuminate\Database\Eloquent\Factories\Factory;

class FileFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $userId = null;

        $this->states->each(function ($callable) use (&$userId){
            $result = $callable();

            if (data_get($result, 'user_id')) {
                $userId = $result['user_id'];
            }
        });

        $indexes = range(1, 36);
        $index = $indexes[array_rand($indexes)];

        $filename = app()->make(FileService::class)->uploadFile(\File::get(public_path("/images/demo/$index.jpg")), 'jpg', $userId);

        return [
            'filename' => $filename,
            'extension' => 'jpg',
            'name' => $this->faker->name,
            'size' => '2MB',
        ];
    }
}
