<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'email' => $this->faker->unique()->safeEmail(),
            'phone' => $this->faker->phoneNumber,
            'first_name' => $this->faker->firstName,
            'last_name' => $this->faker->lastName,
            'day_of_birth' => rand(1,31),
            'month_of_birth' =>rand(1,12),
            'year_of_birth' => rand(1900,2020),
            'gender' => $this->faker->randomElement(['male', 'female']),
            'password' => \Hash::make('secret'), // password
            'remember_token' => Str::random(10),
        ];
    }
}
