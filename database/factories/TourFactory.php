<?php

namespace Database\Factories;

use App\Models\Tour;
use Illuminate\Database\Eloquent\Factories\Factory;

class TourFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'city_location' => $this->faker->city,
            'city_meet' => $this->faker->city,
            'country_meet' => 'usa',
            'state_meet' => $this->faker->country,
            'address_meet' => $this->faker->address,
            'app_meet' => $this->faker->address,

            'category_id' => rand(1, 5),
            'price' => rand(30, 150),
            'group_max_size' => rand(5, 30),
            'group_min_age' => rand(10, 21),
            'duration' => mt_rand(75, 1250) / 100,

            'has_alcohol' => false,

            'notes' => $this->faker->text(450),
            'experience_description' => $this->faker->text(450),
            'places_description' => $this->faker->text(450),
            'directions_description' => $this->faker->text(450),
            'additional_requirements' => $this->faker->text(450),

            'equip_required_type' => Tour::EQUIP_TYPE_DOES_NOT_EQUIPMENT
        ];
    }
}
