<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::factory()->afterCreating(fn ($user) => $user->assignRole('admin'))->create([
            'email' => 'admin@gmail.com'
        ]);

        User::factory()->count(500)->afterCreating(fn ($user) => $user->assignRole('user'))->create();
    }
}
