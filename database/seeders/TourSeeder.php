<?php

namespace Database\Seeders;

use App\Models\File;
use App\Models\Tour;
use App\Models\TourVerification;
use App\Models\User;
use Illuminate\Database\Seeder;

class TourSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (User::all() as $user) {
            $coverPhoto = File::factory()->create(['user_id' => $user->id]);
            $galleryPhotos = File::factory()->count(rand(1, 8))->state(['user_id' => $user->id]);

            Tour::factory()
                ->for($coverPhoto, 'cover_photo')
                ->has($galleryPhotos, 'gallery_photos')
                ->afterCreating(function (Tour $tour) use ($user){
                    switch ($user->id % 6) {
                        case 1: {
                            $tour = $this->setCreating($tour);
                            break;
                        }
                        case 2:
                        {
                            $tour = $this->setFinished($tour);
                            $tour->verification_status = Tour::VERIFICATION_STATUS_ACCEPTED;
                            $tour->verification->status = TourVerification::STATUS_ACCEPTED;
                            break;
                        }
                        case 3:
                        {
                            $tour = $this->setFinished($tour);
                            $tour->verification_status = Tour::VERIFICATION_STATUS_DECLINED;
                            $tour->verification->status = TourVerification::STATUS_DECLINED;
                            $tour->decline_message = 'Decline Message';
                            $tour->verification->decline_message = 'Decline Message';
                            break;
                        }
                        case 4:
                        {
                            $tour = $this->setFinished($tour);
                            $tour->verification_status = Tour::VERIFICATION_STATUS_ACCEPTED;
                            $tour->verification->status = TourVerification::STATUS_DECLINED;
                            $tour->verification->decline_message = 'Decline Message';
                            break;
                        }
                        case 5:
                        {
                            $tour = $this->setFinished($tour);
                            $tour->verification_status = Tour::VERIFICATION_STATUS_DECLINED;
                            $tour->verification->status = TourVerification::STATUS_ACCEPTED;
                            $tour->decline_message = 'Decline Message';
                            break;
                        }
                    }

                    $tour->save();
                    $tour->verification ? $tour->verification->save() : null;
                })
                ->create([
                    'user_id' => $user->id,
                    'verification_status' => Tour::VERIFICATION_STATUS_PENDING,
                ]);
        }
    }

    function setFinished(Tour $tour)
    {
        $this->createVerificationData($tour);
        $tour->status = Tour::STATUS_FINISHED;
        $tour->step = 12;

        return $tour;
    }

    function setCreating(Tour $tour)
    {
        $tour->status = Tour::STATUS_CREATING;
        $tour->step = 10;

        return $tour;
    }

    private function createVerificationData(Tour $tour)
    {
        $verification = TourVerification::create([
            'tour_id' => $tour->id,
            'type' => TourVerification::TYPE_DRIVER_LICENSE,
            'status' => TourVerification::STATUS_PENDING,
            'submitted_at' => now()
        ]);

        $verification->photos()->sync([
            TourVerification::PHOTO_TYPE_FRONT_INDEX => File::factory()->create(['user_id' => $tour->user_id])['id'],
            TourVerification::PHOTO_TYPE_BACK_INDEX => File::factory()->create(['user_id' => $tour->user_id])['id'],
            TourVerification::PHOTO_TYPE_WITH_ID_INDEX => File::factory()->create(['user_id' => $tour->user_id])['id'],
        ]);

        return $verification;
    }
}
