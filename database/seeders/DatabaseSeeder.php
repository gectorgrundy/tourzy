<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \Storage::disk('uploads')->deleteDirectory('files');

        Role::create(['name' => 'admin']);
        Role::create(['name' => 'user']);


        $this->call([
            UserSeeder::class,
            CategorySeeder::class,
            TourSeeder::class,
            TourScheduleSeeder::class,
        ]);

    }
}
