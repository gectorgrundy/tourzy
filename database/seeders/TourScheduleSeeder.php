<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Tour;
use App\Models\TourSchedule;
use DateInterval;
use DatePeriod;
use DateTime;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;

class TourScheduleSeeder extends Seeder
{
    const DATE_FORMAT = "Y-m-d";

    public function run()
    {
        $dates = [];

        $current = date(self::DATE_FORMAT);

        $final = date(self::DATE_FORMAT, strtotime("+1 month", strtotime($current)));

        $period = new DatePeriod(
            new DateTime(date($current)),
            new DateInterval('P1D'),
            new DateTime($final)
        );

        foreach ($period as $key => $value) {
            $dates[] = $value->format(self::DATE_FORMAT);
        }
        Tour::all()->each(function (Tour $tour) use ($dates) {
            $data = [];

            foreach (Arr::random($dates, 15) as $date) {
                $data[] = [
                    "date" => $date,
                    "scheduledTimeSlots" => [13, 15, 17, 20, 22]
                ];
            }

            TourSchedule::query()->create([
                'tour_id' => $tour->id,
                'data' => $data,
                'date_from' => $data[0]['date'],
                'date_to' => end($data)['date'],
            ]);
        });
    }
}
