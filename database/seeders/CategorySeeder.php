<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [
                'title' => 'Arts and Entertainment',
                'slug' => 'arts-and-entertainment',
            ],
            [
                'title' => 'Classes and Workshops',
                'slug' => 'classes-and-workshops',
            ],
            [
                'title' => 'Food and Drink',
                'slug' => 'food-and-drink',
            ],
            [
                'title' => 'History and Local causes',
                'slug' => 'history-and-local-causes',
            ],
            [
                'title' => 'Sports and Outdoors',
                'slug' => 'sports-and-outdoors',
            ],
        ];

        foreach ($categories as $category) {
            Category::create($category);
        }
    }
}
