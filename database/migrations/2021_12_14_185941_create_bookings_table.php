<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->id();
            $table->integer('tour_id');
            $table->integer('user_id');
            $table->integer('status');
            $table->integer('activation_status');
            $table->date('date');
            $table->integer('time');
            $table->integer('guest_count');
            $table->string('confirmation_token');
            $table->string('cancel_message')->nullable();
            $table->string('cancel_reason')->nullable();

            $table->string('name');
            $table->float('price');
            $table->float('duration');
            $table->text('experience_description');
            $table->text('places_description');
            $table->integer('equip_required_type');
            $table->string('equipment_what_to_bring')->nullable();

            $table->timestamp('finished_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
