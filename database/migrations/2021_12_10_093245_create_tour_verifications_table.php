<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTourVerificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tour_verifications', function (Blueprint $table) {
            $table->id();
            $table->integer('tour_id');
            $table->integer('type')->default(\App\Models\TourVerification::TYPE_DRIVER_LICENSE);
            $table->integer('status')->default(\App\Models\TourVerification::STATUS_PENDING);
            $table->text('decline_message')->nullable();
            $table->timestamp('submitted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tour_verifications');
    }
}
