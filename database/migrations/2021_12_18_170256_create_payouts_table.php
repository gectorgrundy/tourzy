<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePayoutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payouts', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->integer('booking_id')->nullable();
            $table->float('amount');
            $table->integer('type')->nullable();
            $table->integer('status')->nullable();
            $table->string('cancel_reason')->nullable();
            $table->string('cancel_message')->nullable();
            $table->string('decline_message')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payouts');
    }
}
