<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateToursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tours', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->integer('category_id')->nullable();
            $table->string('custom_category_title')->nullable();
            $table->integer('cover_photo_id')->nullable();
            $table->integer('status')->nullable();
            $table->integer('verification_status')->nullable();
            $table->string('name')->nullable();
            $table->string('youtube_video_link')->nullable();
            $table->string('country_meet')->nullable();
            $table->string('city_meet')->nullable();
            $table->string('state_meet')->nullable();
            $table->string('address_meet')->nullable();
            $table->string('app_meet')->nullable();
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->integer('group_max_size')->nullable();
            $table->integer('group_min_age')->nullable();
            $table->integer('equip_required_type')->nullable();
            $table->string('equipment_what_to_bring')->nullable();
            $table->float('price')->nullable();
            $table->float('duration')->nullable();
            $table->string('city_location')->nullable();
            $table->text('notes')->nullable();
            $table->text('experience_description')->nullable();
            $table->text('places_description')->nullable();
            $table->text('directions_description')->nullable();
            $table->text('additional_requirements')->nullable();
            $table->boolean('has_alcohol')->nullable();
            $table->string('decline_message')->nullable();
            $table->integer('step')->default(1);
            $table->float('rating')->default(0);
            $table->integer('visit_count')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tours');
    }
}
