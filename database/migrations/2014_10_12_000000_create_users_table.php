<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('email')->unique();
            $table->float('balance')->default(0);
            $table->integer('avatar_id')->nullable();
            $table->string('first_name');
            $table->string('last_name');
            $table->smallInteger('day_of_birth');
            $table->smallInteger('month_of_birth');
            $table->smallInteger('year_of_birth');
            $table->string('phone')->nullable();
            $table->string('gender')->nullable();;
            $table->string('password')->nullable();;
            $table->string('block_message')->nullable();

            $table->boolean('is_login_verified')->default(false);
            $table->boolean('is_blocked')->default(false);
            $table->rememberToken();
            $table->timestamp('email_verified_at')->nullable();
            $table->timestamp('last_login_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
