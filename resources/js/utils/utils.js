export function showMessage(message) {
  var messageCover = document.querySelector('.message.message-cover');
  var messageContent = document.querySelector('.message .message-cover-content');
  if (message && messageCover) {
    messageContent.innerHTML = message;
    messageCover.classList.add('active');
    var timer = setTimeout(function () {
      _clear();
    }, 2500);
    messageCover.addEventListener('click', function () {
      clearTimeout(timer);
      _clear();
    })
  }

  function _clear() {
    messageContent.innerHTML = '';
    messageCover.classList.remove('active');
  }
}

export function addActive(selector) {
  if (selector) jQuery("[data-item=\"" + selector + "\"]").addClass('active');
  jQuery('.overlay').addClass('active');
}

export function removeActive(selector, hideOverlay = false) {
  if (selector) jQuery("[data-item=\"" + selector + "\"]").removeClass('active');
  if (hideOverlay) jQuery('.overlay').removeClass('active');
}

export function setValue(value, input) {
  if (!value || !input) return;
  input.value = value;
}

export function formatAmount(num) {
  let res = num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  if (res.split(".").length === 1) {
    res = `${res}.00`;
  }

  return res;
}

export function copyTextToClipboard(text) {
  if (!navigator.clipboard) {
    fallbackCopyTextToClipboard(text);
    return;
  }
  navigator.clipboard.writeText(text).then(function () {
    console.log('Async: Copying to clipboard was successful!');
  }, function (err) {
    console.error('Async: Could not copy text: ', err);
  });
}

function fallbackCopyTextToClipboard(text) {
  var textArea = document.createElement("textarea");
  textArea.value = text;

  // Avoid scrolling to bottom
  textArea.style.top = "0";
  textArea.style.left = "0";
  textArea.style.position = "fixed";

  document.body.appendChild(textArea);
  textArea.focus();
  textArea.select();

  try {
    var successful = document.execCommand('copy');
    var msg = successful ? 'successful' : 'unsuccessful';
    console.log('Fallback: Copying text command was ' + msg);
  } catch (err) {
    console.error('Fallback: Oops, unable to copy', err);
  }

  document.body.removeChild(textArea);
}

export function getParameterByName(name, url) {
  if (!url) url = window.location.href;
  name = name.replace(/[\[\]]/g, '\\$&');
  var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
    results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, ' '));
};