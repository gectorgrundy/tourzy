import {searchTours} from '@core-api/tour';
import {showNotification} from './utils/notify';
import {
  formatAmount,
  getParameterByName
} from './utils/utils';

const searchTourFilters = [];

if (getParameterByName('email_verify')) {
  showNotification('Your email was verified', 'Success');
}

$('.filter-item .selected-item-wrapper').click(function () {
  $(this)
    .closest('.filter-item').toggleClass('active')
    .closest(".input-wrapper")
    .siblings().find(".filter-item").removeClass('active');
});

$(".filter-item").on("click", function (event) {
  event.stopPropagation();
});

$("body").on("click", function (event) {
  $(".filter-item").removeClass('active');
});

$(".filter-item .single-select-item").click(function () {
  $(this).addClass('active').siblings().removeClass('active');

  const filterValue = $(this).attr('data-value');

  const $filterItem = $(this).closest('.filter-item');
  const filterType = $filterItem.attr('data-type');

  if (filterType && filterValue) {
    searchTourFilters[filterType] = {
      value: filterValue,
      text: $(this).text(),
    };
  }
});

$(".filter-item .multiple-select-item").click(function () {
  $(this).toggleClass('active');

  const filterValue = $(this).attr('data-value');

  const $filterItem = $(this).closest('.filter-item');
  const filterType = $filterItem.attr('data-type');

  if (filterType && filterValue) {
    if ($(this).hasClass('active')) {
      if (searchTourFilters[filterType]) {
        searchTourFilters[filterType].push({
          value: filterValue,
          text: $(this).text(),
        });
      } else {
        searchTourFilters[filterType] = [{
          value: filterValue,
          text: $(this).text(),
        }];
      }
    } else {
      searchTourFilters[filterType] = searchTourFilters[filterType].filter(e => e.value !== filterValue);
    }
  }
});

const getFilterValue = ($filterItem) => {
  const filtersValue = [];

  $filterItem.find('.select-item').toArray().forEach((selectItem) => {
    const $selectItem = $(selectItem);
    const selectItemType = $selectItem.attr('data-item-type');

    let value = null;
    switch (selectItemType) {
      case 'checkbox':
        const $checkbox = $selectItem.find('input[type="checkbox"]');

        if ($checkbox.is(':checked')) {
          value = {
            text: $selectItem.find('label > span').text(),
            value: $selectItem.attr('data-value')
          };
        }

        break;

      case 'datepicker':
        value = searchTourFilters[$(selectItem).closest(".filter-item").attr("data-type")];
        break;

      case 'select':
        value = $selectItem.is('.active') ? {
          value: $selectItem.attr('data-value'),
          text: $selectItem.text(),
        } : null;
        break;
    }

    if (value !== null) {
      filtersValue.push(value);
    }
  });

  return filtersValue;
};


const resetFilterValue = ($filterItem) => {
  $filterItem.find('.select-item').toArray().forEach((selectItem) => {
    const $selectItem = $(selectItem);
    const selectItemType = $selectItem.attr('data-item-type');

    searchTourFilters[selectItemType] = null;

    let value = null;
    switch (selectItemType) {
      case 'checkbox':
        const $checkbox = $selectItem.find('input[type="checkbox"]').prop('checked', false);
        break;

      case 'datepicker':
        $selectItem.datepicker().data('datepicker').clear();
        break;
    }
  });
};

$(".filter-item .btn-reset-filter").click(function () {
  const $filterItem = $(this).closest('.filter-item');
  const filterType = $filterItem.attr('data-type');

  resetFilterValue($filterItem);

  searchTourFilters[filterType] = null;

  const placeholder = $filterItem.attr('data-placeholder');
  $filterItem.find('.selected-item-wrapper').text(placeholder);

  $filterItem.removeClass('active');

  getNewPosts();
});

function formatDate(date) {
  var monthNames = [
    "January", "February", "March",
    "April", "May", "June", "July",
    "August", "September", "October",
    "November", "December"
  ];

  var day = date.getDate();
  var monthIndex = date.getMonth();
  var year = date.getFullYear();

  return day + ' ' + monthNames[monthIndex] + ' ' + year;
}

document.addEventListener('datepicker_data_select', (e) => {
  if (e.detail.type === 'search-tour-dates' && e.detail.date && e.detail.date.length === 2) {
    searchTourFilters.dates = {
      value: e.detail.date.map((date) => {
        return date.getFullYear() + "/" + ("0" + (date.getMonth() + 1)).slice(-2) + "/" + ("0" + date.getDate()).slice(-2)
      }),
      text: `${formatDate(new Date(e.detail.date[0]))} - ${formatDate(new Date(e.detail.date[1]))}`
    };

  }
});

$(".filter-item .btn-save-filter").click(function () {
  const $filterItem = $(this).closest('.filter-item');

  const filterType = $filterItem.attr('data-type');
  const filtersValue = getFilterValue($filterItem);

  if (!filtersValue || filtersValue.length === 0) {
    $filterItem.find('.btn-reset-filter').click();
  } else {
    $filterItem.find('.selected-item-wrapper').text(filtersValue.map(e => e.text).join(", "));

    searchTourFilters[filterType] = filtersValue;

    $filterItem.removeClass('active');

    getNewPosts();
  }
});


let priceChangedTimer = null;
const priceChanged = () => {
  getNewPosts();
}

const priceRangeFrom = $("#price-range").attr('data-from');
const priceRangeTo = $("#price-range").attr('data-to');

$("#price-range").ionRangeSlider({
  type: "double",
  grid: true,
  min: priceRangeFrom || 0,
  max: priceRangeTo || 300,
  from: priceRangeFrom || 0,
  to: priceRangeTo || 300,
  prefix: "",
  onChange: (data) => {
    searchTourFilters.price = {
      from: data.from_pretty,
      to: data.to_pretty,
    };

    clearTimeout(priceChangedTimer);
    priceChangedTimer = setTimeout(priceChanged, 1500);
  }
});

if (getParameterByName('category')) {
  $('.filter-item[data-type="category"]').find(`[data-value="${getParameterByName('category')}"] input[type="checkbox"]`).prop('checked', true);
}

$(function () {
  const sortFilter$ = $(".sort-filter");

  if (sortFilter$.length > 0) {
    searchTourFilters['sortParam'] = sortFilter$.attr('data-active-option');
    searchTourFilters['sortBy'] = sortFilter$.attr('data-sort-by');

    sortFilter$.find('.filter-trigger').click(function () {
      $(this).closest('.sort-filter').find('.sort-options').addClass('active');
    });

    sortFilter$.find('.sort-options li').click(function () {
      $(this).closest('.sort-options').removeClass('active');
      $(this).addClass('active').siblings().removeClass('active');

      $(this).closest('.sort-filter').attr('data-active-option', $(this).attr('data-value'));
      $(this).closest('.sort-filter').attr('data-sort-by', ($(this).attr('data-sort-by') || 'DESC'));
      $(this).closest('.sort-filter').find('.filter-trigger').text($(this).text());

      searchTourFilters['sortParam'] = [{value: sortFilter$.attr('data-active-option')}];
      searchTourFilters['sortBy'] = [{value: sortFilter$.attr('data-sort-by')}];

      getNewPosts();
    });
  }
});

let isLoadingSearchTours = false;

const renderTours = (tours) => {
  const $toursContainer = $(".tours-container");
  const oldHtml = $toursContainer.html();
  const emptyResult = $(".empty-search-result");


  if (tours.length === 0) {
    $toursContainer.html('');
    emptyResult.removeClass('hidden');
    emptyResult.closest(".dashboard-container").addClass('show-empty-search-result');
  } else {
    emptyResult.addClass('hidden');
    emptyResult.closest(".dashboard-container").removeClass('show-empty-search-result');

    $toursContainer.html(`${oldHtml} ${tours.map((tour) => {
      var rating = '';
      [1, 2, 3, 4, 5].forEach(i => {
        rating += `<img class="${tour.rating >= i ? '' : 'inactive'}" src="/static/img/tour-details/icons/icon-star.svg">`
      })

      return `
							<div class="tour-box">
								<a href="/tour/detail/${tour.id}" class="block-link"></a>
								<div class="img-wrapper">
									<div class="tag">${tour.category ? tour.category.title : tour.custom_category_title}</div>
									<img src="${tour.cover_photo_src}" alt="">
								</div>

								<div class="text-wrapper">
									<p class="title">${tour.name || ''}</p>

									<div class="show-sm">
										<div class="reviews-mob-view fw ai-center jc-space-between">
											<div class="reviews-count">
												<img src="/static/img/tour-details/icons/icon-star.svg" alt="">
												<span class="color-green">${tour.rating} (${tour.reviews_count || 0}<span class="reviews-none"> reviews</span>)</span>
											</div>

											<span class="time">
												${tour.duration} hours
											</span>
										</div>
									</div>

									<div class="reviews-total-info small hide-sm">
										<div class="stars-wrapper">
										   ${rating}
										</div>

										<span class="color-grey">
											(${tour.reviews_count || 0} reviews)
										</span>
									</div>

									<p class="country">${tour.city_location}</p>

									<div class="fw jc-space-between ai-center">
										<p class="price">
											<span class="dollar-sign">$</span><span class="number">${formatAmount(tour.price)}</span>
											per person
										</p>

										<p class="time hide-sm">
											${tour.duration} hours
										</p>
									</div>
								</div>
							</div>
						`;
      }).join("")}
    `);
  }

  _setToursVars();
  _hideLoader();
};

const _showLoader = () => {
  isLoadingSearchTours = true;
  $(".ball-loader-wrapper").addClass('active');
};

const _hideLoader = () => {
  setTimeout(() => {
    isLoadingSearchTours = false;
  }, 1000);
  $(".ball-loader-wrapper").removeClass('active');
};

const prepareSearchParams = () => {
  let apiData = {};

  if (searchTourFilters.sortBy) {
    apiData.sort_direction = searchTourFilters.sortBy[0].value
  }

  if (searchTourFilters.sortParam) {
    apiData.sort_by = searchTourFilters.sortParam[0].value
  }

  if ($('input[name="search-tour"]').val().length > 0) {
    apiData.search = $('input[name="search-tour"]').val();
  }

  if (searchTourFilters.category) {
    apiData.categories = searchTourFilters.category.map(i => i.value)
  }

  if (searchTourFilters['guest-count']) {
    apiData.guest_count = searchTourFilters['guest-count'][0].value
  }

  if (searchTourFilters.dates) {
    apiData.date = {
      from: searchTourFilters.dates[0].value[0],
      to: searchTourFilters.dates[0].value[1],
    }
  }

  if (searchTourFilters.price) {
    apiData.price = {
      from: searchTourFilters.price.from,
      to: searchTourFilters.price.to,
    };

  } else {
    const mobFilterPriceFrom = $('.mobile-filter-wrapper input[name="price-from"]').val();
    const mobFilterPriceTo = $('.mobile-filter-wrapper input[name="price-to"]').val();

    if (mobFilterPriceFrom !== '' && !Number.isNaN(+mobFilterPriceFrom)) {
      apiData.price_from = +mobFilterPriceFrom;
    }

    if (mobFilterPriceTo !== '' && !Number.isNaN(+mobFilterPriceTo)) {
      apiData.price_to = +mobFilterPriceTo;
    }
  }

  return apiData
}
const resetPagination = () => {
  tourPaginationData.page = 1
  tourPaginationData.more = true
}
const getNewPosts = (isNewPage = false) => {
  _showLoader();

  let apiData = prepareSearchParams();

  if (!isNewPage) {
    $(".tours-container").html('')
    resetPagination()
  }

  searchTours({...apiData, page: tourPaginationData.page}).then(response => {
    tourPaginationData.page = tourPaginationData.page + 1
    tourPaginationData.more = response.more

    renderTours(response.items)

    _hideLoader();
  })
}

let toursContainerHeight = $(".tours-container").outerHeight();
let toursContainerOffsetTop = $(".tours-container").offset().top;

const _setToursVars = () => {
  toursContainerHeight = $(".tours-container").outerHeight();
  toursContainerOffsetTop = $(".tours-container").offset().top;
}

let lastScroll = null;

$(window).scroll(function () {
  const scrolledTop = $(window).scrollTop();
  if (!isLoadingSearchTours && (lastScroll !== null && scrolledTop > lastScroll) && (scrolledTop + $(window).height()) > toursContainerOffsetTop + (toursContainerHeight / 100 * 75)) {
    if (tourPaginationData.more) {
      if ($(".empty-search-result").hasClass('hidden')) {
        getNewPosts(true);
      }
    }
  }
  lastScroll = scrolledTop;
});


$(".filter-toggler").click(() => {
  $(".mobile-filter-wrapper").toggleClass('active');
});

$(".btn-reset-filters").click(() => {
  $(".btn-reset-filter").toArray().forEach((resetBtn) => {
    const $filterItem = $(resetBtn).closest('.filter-item');
    resetFilterValue($filterItem);

    const filterType = $filterItem.attr('data-type');
    searchTourFilters[filterType] = null;

    const placeholder = $filterItem.attr('data-placeholder');
    $filterItem.find('.selected-item-wrapper').text(placeholder);

    $filterItem.removeClass('active');

  });

  $('.mobile-filter-wrapper input[name="price-from"]').val('');
  $('.mobile-filter-wrapper input[name="price-to"]').val('');

  getNewPosts();
});

$(".btn-save-filters").click(() => {
  getNewPosts();
});


$(function () {
  $(".search-form-row .clear-icon").click((e) => {
    $(e.target).closest(".search-form-row").find('input').val('');
  });
});
