import {getReviews} from '@core-api/review'

$(document).on('click','a.show-more', {} ,function(e){
  e.preventDefault();

  let content = $(this).parent('.review-content');
  content.find('.less-text').hide()
  content.find('a.show-more').hide()
  content.find('.full-text').show()
  content.find('a.show-less').show()
})
$(document).on('click','a.show-less', {} ,function(e){
  e.preventDefault();
  let content = $(this).parent('.review-content');

  content.find('.less-text').show()
  content.find('a.show-more').show()
  content.find('.full-text').hide()
  content.find('a.show-less').hide()
})

$('#load-reviews').on('click', function (e) {
  e.preventDefault();
  let $this = $(this);
  reviewPaginationData.page += 1

  getReviews({
    tour_id: reviewPaginationData.tourId,
    page: reviewPaginationData.page
  }).then(response => {
    console.log(response)
    renderReviews(response.reviews)

    if (!response.more){
      console.log(1)
      $this.hide()
    }
  })
});

function renderReviews(reviews) {
  const $reviewsContainer = $(".reviews-detail-row");
  const oldHtml = $reviewsContainer.html();
  $reviewsContainer.empty().append(`${oldHtml} ${ reviews.map((review) => renderReview(review)).join("")}`);
}

function renderReview(review) {
  var rating = '';
  [1, 2, 3, 4, 5].forEach(i => {
    rating += `<img class="${review.rating >= i ? '' : 'inactive'}" src="/static/img/tour-details/icons/icon-star.svg">`
  })
  var avatar = review.user.avatar ? `<img src='${review.user.avatar.src}'>` : '<img src="/static/img/default-avatar.svg">'
  var text = review.text.length < 200 ? `<p>${review.text}</p>` : `
            <p class="less-text">${review.text.slice(0, 200)} ...</p>
            <p class="full-text" style="display: none;">${review.text}</p>
            <a class="show-more" href="#">show more</a>
            <a class="show-less" style="display: none;" href="#">show less</a>`

  return `
    <div class="review-detail">
        <div class="review-details">
            <div class="fw">
                <div class="img-wrapper">
                    ${avatar}
                </div>
                <div class="text-wrapper">
                    <div class="hero-name">${review.user.first_name} ${review.user.last_name}</div>
                    <div class="hero-stars">${rating}</div>
                </div>
            </div>

            <div class="date">
                ${review.created_at}
            </div>
        </div>
        <div class="review-content">${text}</div>
    </div>`;
}