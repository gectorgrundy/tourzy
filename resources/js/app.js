import Validate from "./utils/validate";
import {showMessage, addActive, removeActive} from './utils/utils';
import {showNotification} from './utils/notify';
import {register, login, resetPassword, forgotPassword} from '@core-api/auth';

$(function () {
  if (notificationToShow) {
    showNotification(notificationToShow.text, null, notificationToShow.type)
  }
  if (typeof checkEmail !== 'undefined') {
    addActive('check-email')
  }
  if (typeof showResetPasswordModal !== 'undefined') {
    console.log(1)
    addActive('reset-password')
  }
  if (typeof showEmailVerifiedModal !== 'undefined') {
    addActive('email-verified')
  }
  if (typeof showLoginModal !== 'undefined') {
    addActive('login')
  }
  if (typeof showBookingCongratulationModal !== 'undefined') {
    addActive('booking-congratulation')
  }
})

function scrollTo($element) {
  if (!$element || $element && $element.length === 0) return;
  jQuery('html, body').animate({
    scrollTop: $element.offset().top
  }, 2000);
}

jQuery("[data-scroll]").click(function () {
  const selector = jQuery(this).attr("data-target");
  if (selector) scrollTo(jQuery(selector));
});

const optionsSlider = {
  "items": 4, "mouseDrag": false, "nav": false, "slideBy": 1, "fixedWidth": 319, "responsive": {
    "400": {
      "items": 1, "slideBy": "page",
    }, "767": {
      "items": 2, "slideBy": "page",
    }, "1024": {
      "items": 3, "slideBy": "page",
    }, "1280": {
      "items": 4, "slideBy": 1,
    }
  }
};

const optionsAllSlider = Object.assign({}, optionsSlider);
optionsAllSlider.container = '.all-slider-cover';
const prevAll = document.querySelector('.four-block .prev-item');
const nextAll = document.querySelector('.four-block .next-item');
if (prevAll && nextAll) {
  optionsAllSlider.prevButton = prevAll;
  optionsAllSlider.nextButton = nextAll;
} else {
  optionsAllSlider.controls = false;
}

try {
  const allSlider = tns(optionsAllSlider);
} catch (e) {
}

const optionsPopularSlider = Object.assign({}, optionsSlider);
optionsPopularSlider.container = '.popular-slider-cover';
const prevPopular = document.querySelector('.second-block .prev-item');
const nextPopular = document.querySelector('.second-block .next-item');
if (prevPopular && nextPopular) {
  optionsPopularSlider.prevButton = prevPopular;
  optionsPopularSlider.nextButton = nextPopular;
} else {
  optionsPopularSlider.controls = false;
}

try {
  const popularSlider = tns(optionsPopularSlider);
} catch (e) {
}

const optionsCategorySlider = Object.assign({}, optionsSlider);
optionsCategorySlider.container = '.tour-category-slider-cover';
optionsCategorySlider.loop = false;
const prevCategory = document.querySelector('.third-block .prev-item');
const nextCategory = document.querySelector('.third-block .next-item');
if (prevCategory && nextCategory) {
  optionsCategorySlider.prevButton = prevCategory;
  optionsCategorySlider.nextButton = nextCategory;
} else {
  optionsCategorySlider.controls = false;
}

try {
  window['category-slider'] = tns(optionsCategorySlider);
} catch (e) {
}

jQuery("[data-close]").click(function (e) {
  const selector = jQuery(this).attr("data-close-target");
  if (selector) {
    const selectorArray = selector.split('|');
    for (let i = 0; i < selectorArray.length; i++) {
      jQuery("[data-item=\"" + selectorArray[i] + "\"]").removeClass('active');
    }
  }
  if (!this.hasAttribute("data-action")) jQuery('.overlay').removeClass('active');
  e.preventDefault();
});

jQuery("[data-action]").click(function (e) {
  const selector = jQuery(this).attr("data-target");
  if (selector) jQuery("[data-item=\"" + selector + "\"]").addClass('active');
  jQuery('.overlay').addClass('active');
  e.preventDefault();
});

jQuery('.show-password').click(function (e) {
  const toggleText = jQuery(this).attr("data-toggle-text");
  const innerText = jQuery(this).html();
  jQuery(this).html(toggleText);
  jQuery(this).attr("data-toggle-text", innerText);
  const password = jQuery(this).parent().find('[type="password"]');
  const text = jQuery(this).parent().find('[type="text"]');
  if (password) password.attr("type", "text");
  if (text) text.attr("type", "password");
  e.preventDefault();
});

jQuery('[data-like-select]').each(function (index, element) {
  const target = jQuery(element).data('like-select-target');
  const input = jQuery('[data-like-select-item="' + target + '"]');
  const pattern = input.data('like-select-pattern');
  const placeholder = jQuery(element).find('.like-select-placeholder');
  jQuery(element).find('.like-select-options').click(function (e) {
    const content = jQuery(this).html();
    jQuery(element).find('.like-select-options').removeClass('active-select');
    jQuery(this).addClass('active-select');
    if (placeholder) {
      placeholder.html(content);
      placeholder.addClass('value');
    }
    if (input) {
      if (pattern) {
        checkValueExist(target, pattern, input);
      }
    }
  });
});

function checkValueExist(target, pattern, input) {
  const likeSelect = jQuery('[data-like-select-target="' + target + '"]');
  let validate = 0;
  let value = pattern;
  likeSelect.each(function (index, element) {
    let checkValue = jQuery(element).find('.active-select');
    const elementKey = jQuery(element).data('like-select');
    if (checkValue.length == 0) {
      validate++;
      jQuery(element).removeClass('success');
    } else {
      jQuery(element).addClass('success');
      const content = checkValue[0].innerHTML;
      value = setDataByPattern(value, elementKey, content);
    }
  });
  if (value !== pattern && validate == 0) {
    input.val(value);
    //let data = Date.parse(value).toISOString();
    var event = new Event('input', {
      bubbles: true, cancelable: true,
    });
    input[0].dispatchEvent(event);
  }
}

function setDataByPattern(pattern, replace, replaceTo) {
  if (pattern && replace) {
    let regExp = new RegExp('\\{' + replace + '\\}', 'gi');
    pattern = pattern.replace(regExp, replaceTo);
  }
  return pattern;
}

function checkHeaderPosition() {
  var scrollTop = jQuery(this).scrollTop();
  var header = jQuery('.header-cover');
  var headerHeight = (header.length > 0) ? header.height() : 0;
  var firstBlock = jQuery('.full-size-block');
  var firstBlockHeight = (firstBlock.length > 0) ? firstBlock.height() : 0;
  if (firstBlock.length > 0 && scrollTop) {
    if ((firstBlockHeight - headerHeight) >= scrollTop) {
      header.removeClass('active-header');
    } else {
      header.addClass('active-header');
    }
  }
}

jQuery(window).scroll(checkHeaderPosition);
checkHeaderPosition();

$(function () {
  $('.button-goto-search').click(() => {
    window.location.href = $('.form-search').attr('action');
  });

  tippy('.tippy', {});

  $('.option-box .option-item:not(.inactive)').click(function () {
    $(this).addClass('active').siblings().removeClass('active');
  });

  $(".checkbox").click(function () {
    $(this).find("input").get(0).checked = !$(this).find("input").get(0).checked;
  });

  if ($('.detail-tour-container .slider-wrapper ul li').length > 3) {
    const $detailTourSlider = $('.detail-tour-container .slider-wrapper ul').lightSlider({
      item: 3,
      loop: true,
      slideMove: 1,
      easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
      pager: false,
      controls: false,
      speed: 600,
      responsive: [{
        breakpoint: 800, settings: {
          item: 3, slideMove: 1, slideMargin: 6,
        }
      }, {
        breakpoint: 480, settings: {
          item: 2, slideMove: 1
        }
      }]
    });

    $('.detail-tour-container .control-item.prev').click(function () {
      $detailTourSlider.goToPrevSlide();
    });
    $('.detail-tour-container .control-item.next').click(function () {
      $detailTourSlider.goToNextSlide();
    });
  } else {
    $('.detail-tour-container .control-item.prev').hide();
    $('.detail-tour-container .control-item.next').hide();

    $('.detail-tour-section .tour-info').addClass('has-' + $('.detail-tour-container .slider-wrapper ul li').length);
    if ($('.detail-tour-container .slider-wrapper ul li').length === 2) {
      $(".tour-info").prepend(`<p class="h1 mb-24">${$(".bottom-bar .h1").text().trim()}</p>`);
    }
  }

  const getParameterByName = (name, url) => {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'), results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
  }

  if (getParameterByName('login') === "true") {
    $('[data-action][href="#login"]').click();
  }

  if (getParameterByName('signup') === "true") {
    $('[data-action][href="#sign"]').click();
  }

  if (getParameterByName('search-tour')) {
    $('input[name="search-tour"]').val(getParameterByName('search-tour'));
  }

  // window.history.pushState({}, document.title, window.location.href.split('?')[0]);

  $('.open-modal').magnificPopup({
    type: 'inline', mainClass: 'mfp-fade', midClick: true, removalDelay: 300, callbacks: {
      close: function () {
        // $('form input[name="redirect_to"]').val("/tools/");
      }
    }
  });
});

var validateLogin = new Validate('data-validate', '#login_form');
$("#login_form").submit(function (event) {
  event.preventDefault();
  validateLogin.checkValidate();
  if (0 === Object.keys(validateLogin.errors).length) {
    var loginForm = $("#login_form")

    loginForm.find('.btn').addClass('loading');

    var data = {};
    $(this).find('input[name]').each(function (index, element) {
      data[element.name] = element.value;
    })

    login(data)
      .then(response => {
        window.location.reload();
      })
      .catch(error => {
        showMessage(error.message);
        loginForm.find('.btn').removeClass('loading');
      })
  }
});

var validateSign = new Validate('data-validate', '#sign_form');
var $registerForm = $("#sign_form")
$registerForm.submit(function (event) {
  event.preventDefault();

  validateSign.checkValidate();
  if (0 === Object.keys(validateSign.errors).length) {
    $registerForm.find('.btn').addClass('loading');

    var data = {};
    $(this).find('input[name]').each(function (index, element) {
      data[element.name] = element.value;
    })

    register(data)
      .then(response => {
        window.location.reload();
      })
      .catch(error => {
        $registerForm.closest(".form-cover").find(".form-warning").addClass("show");
        $registerForm.find('.btn').removeClass('loading');
      })
  }
});

var validateForgot = new Validate('data-validate', '#forgot_form');
$("#forgot_form").submit(function (event) {
  event.preventDefault();
  validateForgot.checkValidate();

  var $resetForm = $("#forgot_form");
  var $resetFormWarning = $("#forgot_form").closest(".form-cover").find(".form-warning");

  if (0 === Object.keys(validateForgot.errors).length) {
    $resetForm.find('.btn').addClass('loading');
    $resetFormWarning.removeClass("show");

    forgotPassword({email: $resetForm.find('input[name="email"]').val()})
      .then(response => {
        $resetForm.find('input[name="email"]').val('')
        $resetForm.find('.btn').removeClass('loading');

        removeActive('forgot', true);

        showNotification('Reset password link was sent to your e-mail address. Please, check your inbox')
      })
      .catch(error => {
        $resetForm.find('.btn').removeClass('loading');
        $resetFormWarning.find("p").text(error.errors.email[0]);
        $resetFormWarning.addClass("show");

      })
  }
});
var validateResetPassword = new Validate('data-validate', '#reset-password-form');
$("#reset-password-form").submit(function (event) {
  event.preventDefault();
  validateResetPassword.checkValidate();

  var $resetPasswordForm = $("#reset-password-form");
  var $resetPasswordFormWarning = $resetPasswordForm.closest(".form-cover").find(".form-warning");

  if (0 === Object.keys(validateResetPassword.errors).length) {
    $resetPasswordForm.find('.btn').addClass('loading');

    resetPassword({
      password: $resetPasswordForm.find('input[name="password"]').val(),
      password_confirmation: $resetPasswordForm.find('input[name="password"]').val(),
      token: $resetPasswordForm.find('input[name="token"]').val(),
      email: $resetPasswordForm.find('input[name="email"]').val()
    })
      .then(response => {
        $resetPasswordForm.find('.btn').removeClass('loading');

        removeActive('reset-password', true);
        addActive('login');

        showNotification('Your password was successfully changed')
      })
      .catch(error => {
        console.log(error)
        $resetPasswordForm.find('.btn').removeClass('loading');

        $resetPasswordFormWarning.find("p").text(error.message);
        $resetPasswordFormWarning.addClass("show");
      })
  }
});


require('./components/datepicker')
require('./components/textarea')
require('./components/select2')








