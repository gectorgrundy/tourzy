import {createBooking, cancelBooking} from '@core-api/booking'
import {contactHost} from '@core-api/user'
import {showNotification} from './utils/notify';
import {
  showMessage,
  formatAmount,
  copyTextToClipboard,
  getParameterByName
} from './utils/utils';

document.addEventListener("google-maps-ready", function () {

  var map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: -33.8688, lng: 151.2195},
    zoom: 14
  });

  const geocoder = new google.maps.Geocoder();
  geocoder.geocode({'address': $(".address-meet").text()}, function (results, status) {
    if (status == 'OK') {
      map.setCenter(results[0].geometry.location);
      var marker = new google.maps.Marker({
        map: map,
        position: results[0].geometry.location,
        visible: false,
      });

      var circle = new google.maps.Circle({
        map: map,
        radius: 5 * 100,
        fillColor: '#AA0000'
      });
      circle.bindTo('center', marker, 'position');
    } else {
      $(".meeting-place").hide();
    }
  });
});

document.addEventListener('datepicker_data_select', (e) => {
  if (e.detail.type === 'detail-page-choose-date' && e.detail.date) {
    $(".open-modal[href='#book']").click();
    $('.is-datepicker[data-control-name="book"]').data('datepicker').selectDate(e.detail.date);
  }
});

const formatDate = (date) => {
  var year = date.getFullYear();
  var month = date.getMonth() + 1;
  var day = date.getDate();
  var hours = date.getHours();
  var minutes = date.getMinutes();
  var seconds = date.getSeconds();

  return year + "-" + month + "-" + day + " " + hours + ":" + minutes + ":" + seconds;
}

//Tour Details
let openBook = false;
if (getParameterByName('book')) {
  openBook = true;
}
$(function () {
  $(document).scroll(function () {
    var scrolled = $(this).scrollTop();

    if (scrolled > 160) {
      $(".bottom-bar").addClass('visible');
    } else {
      $(".bottom-bar").removeClass('visible');
    }
  });

  if (openBook) {
    $("a[href='#book']").click();
  }
});

//Contact Host
$(function () {
  $('#contact-host button').click(function (e) {
    let popup = $('#contact-host');
    let btn = popup.find('button');
    let messageInput = popup.find('textarea');
    let tourIdInput = popup.find('[name="tour-id"]');

    btn.addClass("loading");

    if (!messageInput.val().length) {
      popup.find('.general-input-wrapper').addClass("has-error");
      popup.find('.validation-message').text('Please provide message');
      btn.removeClass("loading");

      return;
    }
    if (messageInput.val().length > 450) {
      popup.find('.general-input-wrapper').addClass("has-error");
      popup.find('.validation-message').text('Please provide value smaller then 450 chars');
      btn.removeClass("loading");

      return;
    }

    popup.find('.general-input-wrapper').removeClass("has-error");

    contactHost({tour_id: tourIdInput.val(), message: messageInput.val()}).then(response => {
      $.magnificPopup.close();

      messageInput.val('')
      btn.removeClass("loading");

      showNotification(`Message Was Successfully Sent`);
    })
  })
})

//Booking
$(function () {
  const bookParams = {
    date: null,
    guestCount: null,
  };
  let bookingId = null

  const updateBookingPriceSummary = () => {
    if (bookParams.guestCount !== null && tour.price) {
      const $bookSummaryBlock = $('.book-summary-block');

      const discount = bookParams.discountAmount || 0;

      $bookSummaryBlock.find('.price').text(`$${formatAmount(tour.price)}`);
      $bookSummaryBlock.find('.guests-count').text(`${bookParams.guestCount} guest${bookParams.guestCount > 1 ? 's' : ''}`);
      $bookSummaryBlock.find('.total-price-general').text(`$${formatAmount(bookParams.guestCount * tour.price)}`);
      $bookSummaryBlock.find('.total-tax').text(`$${bookParams.guestCount * 7}`);
      $bookSummaryBlock.find('.total-price').text(`$${formatAmount(bookParams.guestCount * (7 + tour.price) - discount)}`);

      if (discount !== 0) {
        $bookSummaryBlock
          .find('.discount-row').removeClass('hidden')
          .find('.total-discount').text(`$${discount}`);
      } else {
        $bookSummaryBlock
          .find('.discount-row').addClass('hidden');
      }
    }
  };

  document.addEventListener('datepicker_data_select', (e) => {
    if (e.detail.type === 'book') {
      bookParams.date = e.detail.date;
      $(".choosed-date").text(bookParams.date.toString().split(" ").slice(1, 4).join(" "));
      renderAvailableSlots();
    }
  });

  $('select[name="numberOfGuests"]').change((e) => {
    const choosedOption = $(this).find('option:selected').val();
    bookParams.guestCount = choosedOption;

    updateBookingPriceSummary();
    renderAvailableSlots();
  });

  const renderSlots = (slots) => {
    const $slotContainer = $('.time-input-wrapper');
    $slotContainer.empty();

    slots.forEach((slot) => {
      $slotContainer.append(`
				<div class="time-option ${slot.isDisabled ? 'disabled' : ''}" data-time-option="${slot.time}">
	        		<p class="time">${slot.time}</p>
	        		<p class="slots"><span class="available">${slot.available}</span> slot${slot.available > 1 ? 's' : ''} left</p>
	        	</div>
			`);
    });

    $slotContainer.find('.time-option').click(function () {

      let available = parseInt($(this).find('.available').text())
      let numberOfGuests = $("select[name='numberOfGuests']");

      numberOfGuests.find('option').each(function () {
        $(this).val() > available ? $(this).attr("disabled", "disabled") : $(this).removeAttr('disabled')
      })

      if (!numberOfGuests.val()) {
        numberOfGuests.val(available).change()

        return $slotContainer.find(".time-option[data-time-option='" + $(this).find('.time').text() + "']").click()
      }

      $(this)
        .addClass('active')
        .siblings().removeClass('active');

      bookParams.time = $(this).find('.time').text();

      $('.book-summary-block').addClass('active');


    });

    $slotContainer.closest('.time-input').addClass('active');
  };

  const renderAvailableSlots = () => {
    if (bookParams.date !== null && bookParams.guestCount !== null) {
      if (typeof sheduledDates !== 'undefined') {
        let sheduledDate = sheduledDates.find(sheduledDate => sheduledDate.date.getTime() === bookParams.date.getTime());

        console.log(sheduledDate)
        sheduledDate = sheduledDate.scheduledTimeSlots || [];
        sheduledDate.sort((a, b) => a.time - b.time);

        let slotsData = sheduledDate.map(sheduledTimeSlot => ({
          time: ('0' + sheduledTimeSlot.time).slice(-2) + ':00',
          available: sheduledTimeSlot.availableSlots > 0 ? sheduledTimeSlot.availableSlots : 0,
        }));

        slotsData = slotsData.map((slot) => {
          return {
            ...slot,
            isDisabled: !slot.available,
          }
        });

        // console.log(slotsData);

        renderSlots(slotsData);
      }
    }
  };

  $('.submit-book-request').click(function () {
    if ($('[data-target="login"]').length > 0) {
      $.magnificPopup.close();
      $('[data-target="login"]').click();
    }

    if (USER_AGE !== -1 && USER_AGE < MIN_AGE) {
      showNotification(`Sorry, your age does not meet the minimum age requirements of the tour`, 'Error', 'fail');
      return;
    }

    if ($(this).hasClass('loading')) {
      return;
    }

    if (bookParams.date) {
      const dateArray = (bookParams.date).toString().split(" ");
      $(".pay-summary-block .date.display-inline-block").text(`${dateArray[2]} ${dateArray[1]} ${dateArray[3]} ${bookParams.time}`)
    }

    $(this).addClass('loading');

    createBooking({
      guest_count: bookParams.guestCount,
      time: bookParams.time,
      date: bookParams.date.getFullYear() + "-" + ("0" + (bookParams.date.getMonth() + 1)).slice(-2) + "-" + ("0" + bookParams.date.getDate()).slice(-2),
      tour_id: tour.id,
    })
      .then(response => {
        $(this).removeClass('loading');

        bookingId = response.booking.id
        $.magnificPopup.open({items: {src: '#pay'}});
      })
  });


  const $payPopup = $("#pay");

  $payPopup.find('.option-box .option-item:not(.inactive)').click(() => {
    console.log(1)
    $payPopup.find('.submit-pay').removeClass('disabled');
    bookParams.payMethod = $payPopup.find('.option-box .option-item.active').attr('data-type');
  });

  $payPopup.find('.discount-wrapper input[data-control-name="hasPromocode"]').change(function () {
    const $promocodeInputWrapper = $payPopup
      .find('.discount-wrapper input[data-control-name="promocodeValue"]')
      .closest('.opacity-transition');
    if ($(this).is(':checked')) {
      $promocodeInputWrapper
        .removeClass('opacity0')
        .addClass('opacity100');
    } else {
      $promocodeInputWrapper
        .removeClass('opacity100')
        .addClass('opacity0');
    }
  });

  let promocodeCheckRequest = null;
  const checkPromocode = () => {
    const promocodeValue = $payPopup.find('[data-control-name="promocodeValue"]').val();

    if (promocodeValue && promocodeValue.trim().length > 0) {
      $payPopup.find('.submit-pay').addClass('disabled').addClass('loading');

      $.post(
        'https://jsonplaceholder.typicode.com/posts',
        {
          ...bookParams,
          tourId: tour.id,
          promocodeValue,
        },
        (response) => {
          response = {
            isValid: true,
            discountAmount: Math.floor(Math.random() * 100) + 1,
          }

          if (bookParams.payMethod) {
            $payPopup.find('.submit-pay').removeClass('disabled')
          }

          $payPopup.find('.submit-pay').removeClass('loading');

          if (response && response.isValid && response.discountAmount) {
            bookParams.discountAmount = response.discountAmount;
            bookParams.promocodeValue = promocodeValue;

            updateBookingPriceSummary();
          }
        }
      );
    }
  };

  let promocodeTypingTimer = null;
  const donePromocodeTypingInterval = 2000;

  $payPopup.find('[data-control-name="promocodeValue"]').on('keyup paste', () => {
    clearTimeout(promocodeTypingTimer);
    promocodeTypingTimer = setTimeout(checkPromocode, donePromocodeTypingInterval);
  });

  $payPopup.find('.submit-pay').click(() => {
    const serialize = (obj) => {
      var str = [];
      for (var p in obj)
        if (obj.hasOwnProperty(p)) {
          str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        }
      return str.join("&");
    };

    const payParams = {
      pay_method: bookParams.payMethod,
      booking_id: bookingId,
    }

    location.href = `/tour-payment?${serialize(payParams)}`;
  });
});

//Booking Details
$(function () {

  $(".download-qr").click(() => {
    $(".qr-code").click();
  });

  $(".copy-confirmation-code").click((e) => {
    e.preventDefault();
    copyTextToClipboard($(".confirmation-code").text());
    showNotification("Code copied", "Success")
    return false;
  });

  const validateStandardCancelInput = (input) => {
    let isValid = true;
    const $input = $(input);
    const inputValue = $input.val();

    if (inputValue.length == 0) {
      $input.closest(".general-input-wrapper").find(".validation-message").text('Please fill this field');
      isValid = false;
    }

    if (isValid) {
      $input.closest(".general-input-wrapper").removeClass("has-error");
    } else {
      $input.closest(".general-input-wrapper").addClass("has-error");
    }

    return isValid;
  };

  $("#cancel input").on("change input", (e) => {
    if ($(e.target).closest(".general-input-wrapper").hasClass("has-error")) {
      validateStandardCancelInput(e.target);
    }
  });

  const validateAllCancelInputs = () => {
    let isInputsValid = true;

    $("#cancel input").toArray().forEach((input) => {
      console.log(input);
      if (!validateStandardCancelInput(input)) {
        isInputsValid = false;
      }
    });

    return isInputsValid;
  }

  $(".popup#cancel .submit-cancel").click(function () {
    if (!validateAllCancelInputs()) {
      return;
    }

    let popupCancel = $(".popup#cancel")

    const cancelDataObject = {
      bank_name: popupCancel.find('[name="bank-name"]').val(),
      account_number: popupCancel.find('[name="account-number"]').val(),
      account_holder_name: popupCancel.find('[name="account-holder-name"]').val(),
      routing_number: popupCancel.find('[name="routing-number"]').val(),
      message: popupCancel.find('[name="cancel-comment"]').val(),
      reason: popupCancel.find('[data-control-name="cancel-reason"] input:checked').val() || '',
      id: popupCancel.find('[name="booking-id"]').val(),
    };

    cancelBooking(cancelDataObject).then(response => {
      window.location.reload()
    })
  });
});

require('./components/reviews')

