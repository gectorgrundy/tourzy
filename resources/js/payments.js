import {withdrawal} from '@core-api/payout';

$(document).ready(function () {
  $('.data-table').DataTable({
    "paging": false,
    "language": {
      "emptyTable": "No data available"
    },
    "order": [[ 0, "desc" ]]
  });
});

var mask = IMask($('[data-control-name="card-number"]').get(0), {
  mask: '0000 0000 0000 0000',
  lazy: true
});

var mask = IMask($('[data-control-name="withdraw-sum"]').get(0), {
  mask: '0000000000000000',
  lazy: true
});

const validateSum = () => {
  let isValid = true;
  const $withdrawInput = $('[data-control-name="withdraw-sum"]');
  const withdrawValue = $withdrawInput.val();
  const currentBalance = +$(".balance").text().replace("$", "");

  if (withdrawValue.trim().length === 0) {
    $withdrawInput.closest(".general-input-wrapper").find(".validation-message").text('Please, enter the sum of withdrawal');
    isValid = false;
  }

  if ((+withdrawValue) > currentBalance) {
    $withdrawInput.closest(".general-input-wrapper").find(".validation-message").text('Withdrawal sum is bigger than your account balance');
    isValid = false;
  }

  if (isValid) {
    $withdrawInput.closest(".general-input-wrapper").removeClass("has-error");
  } else {
    $withdrawInput.closest(".general-input-wrapper").addClass("has-error");
  }

  return isValid;
};

const validateCard = () => {
  let isValid = true;
  const $cardInput = $('[data-control-name="card-number"]');
  const cardValue = $cardInput.val();

  if (cardValue.length !== 19) {
    $cardInput.closest(".general-input-wrapper").find(".validation-message").text('The card nisContentEditableumber should be 16 digit');
    isValid = false;
  }

  if (isValid) {
    $cardInput.closest(".general-input-wrapper").removeClass("has-error");
  } else {
    $cardInput.closest(".general-input-wrapper").addClass("has-error");
  }

  return isValid;
};

const validateStandardWithdrawInput = (input) => {
  let isValid = true;
  const $input = $(input);
  const inputValue = $input.val();

  if (inputValue.length == 0) {
    $input.closest(".general-input-wrapper").find(".validation-message").text('Please fill this field');
    isValid = false;
  }

  if (isValid) {
    $input.closest(".general-input-wrapper").removeClass("has-error");
  } else {
    $input.closest(".general-input-wrapper").addClass("has-error");
  }

  return isValid;
};

const validateWithdrawInput = (input) => {
  let isValid = null;

  switch ($(input).attr('data-control-name')) {
    case 'withdraw-sum':
      isValid = validateSum();
      break;
    default:
      isValid = validateStandardWithdrawInput(input);
      break;
  }

  return isValid;
}

$("#withdraw input").on("change input", (e) => {
  if ($(e.target).closest(".general-input-wrapper").hasClass("has-error")) {
    validateWithdrawInput(e.target);
  }
});

const validateAllWithdrawInputs = () => {
  let isInputsValid = true;

  $("#withdraw input").toArray().forEach((input) => {
    if (!validateWithdrawInput(input)) {
      isInputsValid = false;
    }
  });

  return isInputsValid;
}

$(".submit-withdrawal-request").click((e) => {
  if (validateAllWithdrawInputs()) {

    const withdrawDataObject = {
      bank_name: $(".popup#withdraw").find('[name="bank-name"]').val(),
      account_number: $(".popup#withdraw").find('[name="account-number"]').val(),
      account_holder_name: $(".popup#withdraw").find('[name="account-holder-name"]').val(),
      routing_number: $(".popup#withdraw").find('[name="routing-number"]').val(),
      withdraw_sum: $(".popup#withdraw").find('[name="withdraw-sum"]').val(),
    };

    withdrawal(withdrawDataObject).then(response => {
      window.location.reload();
    })
}
});