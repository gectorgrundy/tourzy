import VSelect from 'vue-select'
import Spinner from '@common/components/Spinner'
import FeatherIcon from '@core/components/FeatherIcon.vue'

VSelect.props.label.default = 'text'


export default Vue => {
  Vue.component('spinner', Spinner);
  Vue.component(FeatherIcon.name, FeatherIcon)
  Vue.component('v-select', VSelect)
}