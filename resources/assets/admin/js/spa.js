import Vue from 'vue'

import VueMoment from 'vue-moment'
Vue.use(VueMoment);

import { BootstrapVue, BootstrapVueIcons } from 'bootstrap-vue'
Vue.use(BootstrapVue)
Vue.use(BootstrapVueIcons)

import GlobalComponents from './global-components'
Vue.use(GlobalComponents);

import UserListPage from './pages/user/list'
import UserSinglePage from './pages/user/single'
import TourListPage from './pages/tour/list'
import PayoutListPage from './pages/payout/list'

const spa = document.querySelector('#spa');

if (spa) {
  let app = new Vue({
    el: spa,
    components: {
      UserListPage,
      UserSinglePage,
      TourListPage,
      PayoutListPage,
    },
  });
}