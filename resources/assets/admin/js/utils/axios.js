import Vue from 'vue'

// axios
import axios from 'axios'

const axiosIns = axios.create({
  // You can add your headers here
  // ================================
  baseURL: '/api/admin',
  // timeout: 1000,
  // headers: {'X-Custom-Header': 'foobar'}
})


axiosIns.interceptors.request.use(
  config => {
    return config
  },
  error => {
    return Promise.reject(error)
  }
)

axiosIns.interceptors.response.use(
  response => {
    return response.data
  },

  error => {
    const { config, response } = error

    if (response.status !== 401) {
      return Promise.reject(Object.assign({}, error).response.data)
    }

    throw error;

  }
)

Vue.prototype.$http = axiosIns

export default axiosIns
