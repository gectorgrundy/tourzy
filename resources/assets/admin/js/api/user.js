import request from '@admin/utils/axios'

export function getUserList(params) {
  return request({
    url: '/user/list',
    method: 'get',
    params: params
  })
}

export function getUserSingle(id) {
  return request({
    url: '/user/single',
    method: 'get',
    params: {id}
  })
}

export function toggleBlock(data) {
  return request({
    url: '/user/toggle-block',
    method: 'post',
    data: data
  })
}