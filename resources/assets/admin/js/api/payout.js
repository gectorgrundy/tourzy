import request from '@admin/utils/axios'

export function getPayoutList(params) {
  return request({
    url: '/payout/list',
    method: 'get',
    params: params
  })
}

export function hostWithdrawalAccept(params) {
  return request({
    url: '/payout/host-withdrawal/accept',
    method: 'post',
    params: params
  })
}
export function hostWithdrawalDecline(params) {
  return request({
    url: '/payout/host-withdrawal/decline',
    method: 'post',
    params: params
  })
}

export function cancelBookingAccept(params) {
  return request({
    url: '/payout/cancel-booking/accept',
    method: 'post',
    params: params
  })
}

export function cancelBookingDecline(params) {
  return request({
    url: '/payout/cancel-booking/decline',
    method: 'post',
    params: params
  })
}
