import request from '@admin/utils/axios'

export function tourVerificationApprove(id) {
  return request({
    url: '/tour-verification/approve',
    method: 'post',
    data: {id}
  })
}

export function tourVerificationDecline(data) {
  return request({
    url: '/tour-verification/decline',
    method: 'post',
    data: data
  })
}