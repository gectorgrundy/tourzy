import Vue from 'vue'
import vSelect from 'vue-select'

vSelect.props.components.default = () => ({
  Deselect: {
    render: h => h('feather-icon', {props: {size: '16', icon: 'XIcon', fill: '#A4A4A4'}}),
  },
  OpenIndicator: {
    render: h => h('svg', {
      attrs: {
        xmlns: "http://www.w3.org/2000/svg",
        width: "14",
        height: "8",
        fill: "none",
      }
    }, [h("path", {
      attrs: {
        d: "M7.464 7.314l5.844-5.717a.634.634 0 000-.909.665.665 0 00-.927 0L7 5.952 1.62.689a.666.666 0 00-.928 0 .634.634 0 000 .909l5.844 5.717a.673.673 0 00.928 0z"
      }
    })]),
  },
})
vSelect.props.label.default = 'text'

Vue.component(vSelect)
