export default {
  data() {
    return {
      TOUR_STATUSES: {
        STATUS_CREATING: 0,
        STATUS_VALIDATING_BY_VERIFY_ID: 1,
        STATUS_VALIDATING_BY_ADMIN: 2,
        STATUS_FINISHED: 3
      },
      TOUR_VERIFICATION_STATUSES: {
        VERIFICATION_STATUS_PENDING: 0,
        VERIFICATION_STATUS_ACCEPTED: 1,
        VERIFICATION_STATUS_DECLINED: 2,
      },
      VERIFICATION_DATA_STATUSES: {
        VERIFICATION_DATA_STATUS_PENDING: 1,
        VERIFICATION_DATA_STATUS_ACCEPTED: 2,
        VERIFICATION_DATA_STATUS_DECLINED: 3,
      }
    }
  }
}