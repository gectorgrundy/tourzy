import * as VueGoogleMaps from 'vue2-google-maps'
import Spinner from '@common/components/Spinner'
import InputCounter from '@common/components/InputCounter'
import FeatherIcon from '@core/components/FeatherIcon.vue'

export default Vue => {
  Vue.component('spinner', Spinner);
  Vue.component('input-counter', InputCounter);
  Vue.component(FeatherIcon.name, FeatherIcon)

  Vue.use(VueGoogleMaps, {
    load: {
      key: 'AIzaSyASgG8Oos8QgTNMgy9hC382dcp5lyhsFGs',
      libraries: 'places',
    },
  })
}