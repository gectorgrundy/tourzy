import request from '@core/utils/axios'

export function sendPaymentIntent(booking_id) {
  return request({
    url: '/stripe/create-payment-intent',
    method: 'post',
    data: {booking_id}
  })
}

export function webHookDev(payment_intent) {
  return request({
    url: '/stripe/web-hook-dev',
    method: 'post',
    data: {payment_intent}
  })
}

