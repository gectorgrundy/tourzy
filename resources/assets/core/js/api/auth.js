import request from '@core/utils/axios'

export function login(data) {
  return request({
    url: '/auth/login',
    method: 'post',
    data: data
  })
}
export function register(data) {
  return request({
    url: '/auth/register',
    method: 'post',
    data: data
  })
}
export function forgotPassword(data) {
  return request({
    url: '/auth/forgot-password',
    method: 'post',
    data: data
  })
}

export function resetPassword(data) {
  return request({
    url: '/auth/reset-password',
    method: 'post',
    data: data
  })
}
