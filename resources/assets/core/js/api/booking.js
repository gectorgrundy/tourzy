import request from '@core/utils/axios'

export function createBooking(data) {
  return request({
    url: '/booking',
    method: 'post',
    data: data
  })
}

export function cancelBooking(data) {
  return request({
    url: '/booking/cancel',
    method: 'post',
    data: data
  })
}

export function activateBooking(data) {
  return request({
    url: '/booking/activate',
    method: 'post',
    data: data
  })
}

