import request from '@core/utils/axios'

export function getVerificationData() {
  return request({
    url: '/tour-verification',
    method: 'get',
  })
}
export function saveVerificationData(data) {
  return request({
    url: '/tour-verification',
    method: 'put',
    data: data
  })
}