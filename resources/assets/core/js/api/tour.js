import request from '@core/utils/axios'

export function getMyTour() {
  return request({
    url: '/tour/my-tour',
    method: 'get',
  })
}

export function getTourData() {
  return request({
    url: '/tour',
    method: 'get',
  })
}

export function saveTour(data) {
  return request({
    url: '/tour/save',
    method: 'put',
    data: data
  })
}

export function searchTours(data) {
  return request({
    url: '/tour/search',
    method: 'post',
    data: data
  })
}