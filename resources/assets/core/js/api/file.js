import request from '@core/utils/axios'

export function getFiles() {
  return request({
    url: '/file',
    method: 'get',
  })
}

export function deleteFile(id) {
  return request({
    url: '/file',
    method: 'delete',
    data: {id}
  })
}
