import request from '@core/utils/axios'

export function getMyTourSchedule() {
  return request({
    url: '/tour-schedule',
    method: 'get',
  })
}

export function saveMyTourSchedule(schedules) {
  return request({
    url: '/tour-schedule',
    method: 'put',
    data: {schedules}
  })
}
