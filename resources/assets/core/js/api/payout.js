import request from '@core/utils/axios'

export function withdrawal(data) {
  return request({
    url: '/payout/withdrawal',
    method: 'post',
    data: data
  })
}
