import request from '@core/utils/axios'

export function getSettings() {
  return request({
    url: '/settings',
    method: 'get',
  })
}

export function saveSettings(data) {
  return request({
    url: '/settings',
    method: 'post',
    data: data
  })
}

export function contactHost(data){
  return request({
    url: '/contact-host',
    method: 'post',
    data: data
  })
}