import request from '@core/utils/axios'

export function getReviews(data) {
  return request({
    url: '/review',
    method: 'get',
    params: data
  })
}
