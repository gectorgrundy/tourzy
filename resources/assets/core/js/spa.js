import Vue from 'vue'

import VueMoment from 'vue-moment'
Vue.use(VueMoment);

import('./libs/vue-select')
import('./libs/toastification')

import GlobalComponents from './global-components'
Vue.use(GlobalComponents);

import { BootstrapVue, BootstrapVueIcons } from 'bootstrap-vue'
Vue.use(BootstrapVue)
Vue.use(BootstrapVueIcons)

import MyTourListPage from './pages/my-tour/list'
import MyTourCreatePage from './pages/my-tour/create'
import MyTourEditPage from './pages/my-tour/edit'
import MySchedulePage from './pages/my-schedule'
import TourVerificationPage from './pages/tour-verification'
import UserSettingsPage from './pages/settings'

const spa = document.querySelector('#spa');

if (spa) {
  let app = new Vue({
    el: spa,
    components: {
      MyTourListPage,
      MyTourCreatePage,
      MyTourEditPage,
      MySchedulePage,
      TourVerificationPage,
      UserSettingsPage,
    },
  });
}