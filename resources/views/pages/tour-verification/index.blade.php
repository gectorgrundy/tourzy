@extends('layouts.app')

@section('content')
  <div id="spa">
    <tour-verification-page />
  </div>
@endsection

@push('scripts')
  <script src="{{ mix('js/core-spa.js') }}"></script>
@endpush