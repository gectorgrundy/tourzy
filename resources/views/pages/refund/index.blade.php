@extends('layouts.app')

@section('content')
  <div class="dashboard-container detail-tour-container search-page">
    <h1>Refund Policy</h1>

    <p>In order to be eligible for a refund, you must cancel your tour at least 24 hours in advance. If your tour is within 24 hours, you will not be eligible for a refund.</p>
    <p>If you cancel at least 24 hours in advance, the money will be refunded to the original payment method you’ve used during the purchase. For credit card payments it may take 5 to 10 business days for a refund to show up on your credit card statement.</p>
    <p>After the tour takes place, if there are any outstanding issues that you feel need to be resolved, please reach out to our customer support team and we will provide you with further help.&nbsp;</p>
    <p>If anything is unclear or you have more questions, feel free to contact our customer support team.</p>
    <p>&nbsp;</p>
    <p><br></p>
  </div>
@endsection