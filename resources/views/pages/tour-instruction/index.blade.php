@extends('layouts.app')

@section('content')
  <div class="dashboard-container detail-tour-container instruction-container">
    @include('pages.tour-instruction.steps.1')
    @include('pages.tour-instruction.steps.2')
    @include('pages.tour-instruction.steps.3')
  </div>
@endsection
@push('scripts')
  <script>
    $(function() {
      $(".instruction-step").eq(0).addClass('active');

      $(".instruction-step").find('.next-step').click(function() {
        const $instructionStep = $(this).closest(".instruction-step");

        if ( $instructionStep.next().length > 0 ) {
          $instructionStep.removeClass('active')
          $instructionStep.next().addClass('active');
        } else {
          window.location.href = "{{ route('my-tour-create') }}";
        }
      });

      $(".instruction-step").find('.prev-step').click(function() {
        const $instructionStep = $(this).closest(".instruction-step");

        if ( $instructionStep.prev().length > 0 ) {
          $instructionStep.removeClass('active')
          $instructionStep.prev().addClass('active');
        } else {
          window.location.href = "{{ route('my-tour-create') }}";
        }
      });
    });
  </script>
@endpush