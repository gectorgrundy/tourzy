<div class="instruction-step">
  <div class="show-sm flex-100">
    <div class="progress-bar mb-30">
      <div class="bar" style="width: 66%"></div>
    </div>
  </div>

  <div class="content-wrapper">
    <div class="mb-30"></div>
    <div class="progress-bar mb-30 hide-sm">
      <div class="bar" style="width: 66%"></div>
    </div>

    <h1 class="mb-30">Hi there!</h1>
    <h2 class="mb-8">What we are looking for in an experience?</h2>

    <div class="experience-box-wrapper mb-40">
      <div class="experience-box">
        It’s led by a knowledgeable and passionate host
      </div>
      <div class="experience-box">
        Guests participate hands-on, or are immersed in an activity
      </div>
      <div class="experience-box">
        It gives guests access to a special place or community
      </div>
      <div class="experience-box">
        It tells the story of a host’s unique perspective
      </div>
    </div>

    <div class="fw jc-space-between ai-center hide-sm">
      <button class="prev-step simple-link orange">
        <img src="{{ asset('/images/icons/arrow-orange.svg') }}">
        Back
      </button>

      <button class="btn next-step filled orange ta-center w280">
        Got it
      </button>
    </div>
  </div>
  <div class="img-wrapper hide-xs">
    <img class="hide-sm" src="{{ asset('/images/instruction/steps/2.png') }}">
    <img class="show-sm w100p mb-24" src="{{ asset('/images/instruction/steps/2-mob.png') }}">
  </div>

  <div class="show-sm flex-100">
    <div class="fw jc-space-between ai-center">
      <button class="prev-step simple-link orange">
        <img src="{{ asset('/images/icons/arrow-orange.svg') }}">
        Back
      </button>

      <button class="btn next-step filled orange ta-center w280">
        Got it
      </button>
    </div>
  </div>
</div>