<div class="instruction-step">
  <div class="show-sm flex-100">
    <div class="progress-bar mb-30">
      <div class="bar" style="width: 33%"></div>
    </div>
  </div>

  <div class="content-wrapper">
    <div class="mb-30"></div>
    <div class="progress-bar mb-30 hide-sm">
      <div class="bar" style="width: 33%"></div>
    </div>

    <h1 class="mb-30">Hi there!</h1>
    <h2 class="mb-8">Here’s an overview of the process:</h2>

    <div class="step-box-wrapper mb-40">
      <div class="step-box" data-number="1">
        <p class="title small-header">Learn about our expectations</p>
        <p class="content">Find out what makes experiences different and what Tourzy is looking for.</p>
      </div>

      <div class="step-box" data-number="2">
        <p class="title small-header">Create your experience</p>
        <p class="content">Add photos, descriptions, and other details to be reviewed by Tourzy.</p>
      </div>

      <div class="step-box" data-number="3">
        <p class="title small-header">Verify your ID</p>
        <p class="content">Someone from Tourzy will review your experience page. If it meets our quality standards, you’ll get to add availability and start hosting!</p>
      </div>
    </div>

    <p class="ta-center hide-sm">
      <button class="btn next-step filled orange ta-center w280">
        Got it
      </button>
    </p>
  </div>

  <div class="img-wrapper hide-xs">
    <img class="hide-sm" src="{{ asset('/images/instruction/steps/1.png') }}">
    <img class="show-sm" src="{{ asset('/images/instruction/steps/1-mob.png') }}">
  </div>

  <p class="ta-center show-sm flex-100">
    <button class="btn next-step filled orange ta-center w280">
      Got it
    </button>
  </p>
</div>