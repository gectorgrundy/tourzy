@php
  $status = $booking->status;
  $activationStatus = $booking->activation_status;
  $isPast = isset($isPast);
@endphp

<div class="purchased-tour-box">
  <div class="fw">
    <div class="img-wrapper">
      <a href="{{ route('my-booking', ['booking' => $booking->id]) }}">
        <img src="{{ image_url( $booking->tour->cover_photo, 'medium')  }}" alt="">
      </a>
    </div>
    <div class="text-wrapper">
      <div>
        <p class="mg-bottom-8">
          <a class="color-black" href="{{ route('my-booking', ['booking' => $booking->id]) }}">
            {{ $booking->name }}
          </a>
        </p>
        <p><span class="font-weight-lighter">Confirmation code:</span> {{ $booking->confirmation_token }}</p>
      </div>

      <div class="list-wrapper">
        <div class="list-item">
          <img src="{{ asset('/images/purchased-tours/icons/verified.svg') }}">


          @if($status === \App\Models\Booking::STATUS_SUCCESS && $activationStatus === \App\Models\Booking::ACTIVATION_STATUS_PENDING)
            <p>Order confirmed</p>
          @elseif($status === \App\Models\Booking::STATUS_SUCCESS && $activationStatus === \App\Models\Booking::ACTIVATION_STATUS_SUCCESS)
            <p>Visited</p>
          @elseif($status === \App\Models\Booking::STATUS_SUCCESS && $activationStatus === \App\Models\Booking::ACTIVATION_STATUS_CANCELED)
            <p>Cancellation Pending</p>
          @elseif($status === \App\Models\Booking::STATUS_CANCELED)
            <p>Cancelled</p>
          @elseif($status === \App\Models\Booking::STATUS_OUTDATED)
            <p>Outdated</p>
          @endif
        </div>

        <div class="list-item">
          <img src="{{ asset('/images/purchased-tours/icons/location.svg') }}">
          <p>{{ $booking->tour->city_location }}</p>
        </div>
        <div class="list-item">
          <img src="{{ asset('/images/purchased-tours/icons/clock.svg') }}">
          <p>{{ $booking->duration }} hours</p>
        </div>

        <div class="list-item">
          <img src="{{ asset('/images/purchased-tours/icons/calendar.svg') }}">
          <p>{{ $booking->date->format('d.m.Y')}} {{ $booking->time . ':00' }}</p>
        </div>
      </div>

    </div>
  </div>

  <div class="btns-wrapper">
    @if($isPast)
      <p class="ta-center">
        <a href="{{ route('my-booking', ['booking' => $booking->id]) }}" class="simple-link orange">
          See details
        </a>
      </p>

      <a href="{{ route('tour-detail', ['tour' => $booking->tour->id]) }}?book=true"
         class="btn unfilled orange ta-center w280">
        Book again
      </a>

      @if($booking->status === \App\Models\Booking::ACTIVATION_STATUS_SUCCESS && !$booking->current_user_review)
        <a href="{{ route('my-booking', ['booking' => $booking->id]) }}?feedback=true" class="btn filled orange ta-center w280">
          Leave a feedback
        </a>
      @endif
    @else

      <a target="_blank" class="btn unfilled orange ta-center w280" href="{{google_calendar_link($booking)}}">
        + Add to calendar
      </a>
      <a href="{{ route('my-booking', ['booking' => $booking->id])}}" class="btn filled orange ta-center w280">
        See details
      </a>

    @endif

  </div>

</div>
