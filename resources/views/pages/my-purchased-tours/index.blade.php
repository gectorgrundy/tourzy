@extends('layouts.app')

@section('content')
  @if($future->count() || $past->count())
    <div class="dashboard-container detail-tour-container">
      <h1 class="mb-32">My tours</h1>

      @if($future->count())
        <div class="tours-block mb-100">
          <h2 class="color-green mb-8">Future tours</h2>
          <div>
            @foreach($future as $booking)
              @include('pages.my-purchased-tours.booking-box', ['booking' => $booking])
            @endforeach
          </div>
        </div>
      @endif
      @if($past->count())
        <div class="tours-block mb-100">
          <h2 class="color-green mb-8">Past tours</h2>
          <div>
            @foreach($past as $booking)
              @include('pages.my-purchased-tours.booking-box', ['booking' => $booking, 'isPast' => true])
            @endforeach
          </div>
        </div>
      @endif

    </div>
  @else
    <div class="no-purchased-tours">
      <div class="dashboard-container detail-tour-container">
        <div class="w640">
          <h1 class="mb-24">Unfortunately, you don't have purches tours.</h1>
          <p class="mb-40">But don't worry, you can choose tour what you like and whatever you like on out search page.</p>

          <a href="{{route('search')}}" class="btn next-step ta-center filled orange w280">Go to search page</a>
        </div>
      </div>
    </div>
  @endif
@endsection
