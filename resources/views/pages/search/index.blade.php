@extends('layouts.app')

@section('content')

  <div class="dashboard-container detail-tour-container search-page">
    <div class="search-form-row fw full-width mb-24 show-md">
      <form class="full-width" action="{{ route('search') }}" method="GET">
        <div class="full-width general-input-wrapper input-wrapper search-input-wrapper">
          <input class="input-control" type="text" name="search-tour" placeholder="{{ current_city() }}" required>

          <svg class="search-icon" width="16" height="16" viewBox="0 0 16 16" fill="none"
               xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" clip-rule="evenodd"
                  d="M1.83031 1.83059C4.27125 -0.610195 8.24407 -0.610195 10.685 1.83059C12.8593 4.00522 13.0921 7.38994 11.3941 9.82966C11.6369 9.88098 11.8692 9.99897 12.0571 10.1863L15.6119 13.7419C16.1294 14.2578 16.1294 15.0948 15.6119 15.6123C15.0965 16.1292 14.2594 16.1292 13.7413 15.6123L10.1866 12.0577C9.99765 11.8688 9.88018 11.6371 9.82885 11.3942C7.3895 13.0911 4.00403 12.8583 1.83031 10.6852C-0.610103 8.24392 -0.610103 4.2719 1.83031 1.83059ZM2.94536 9.45443C4.73935 11.2489 7.65986 11.2489 9.45438 9.45443C11.2489 7.66 11.2489 4.74013 9.45438 2.9457C7.65986 1.15178 4.73935 1.15178 2.94536 2.9457C1.15189 4.74013 1.15189 7.66 2.94536 9.45443Z"
                  fill="#A4A4A4"/>
          </svg>

          <svg class="clear-icon" width="12" height="12" viewBox="0 0 12 12" fill="none"
               xmlns="http://www.w3.org/2000/svg">
            <path
              d="M7.06127 6.00012L11.7801 1.28108C12.0733 0.988138 12.0733 0.512831 11.7801 0.219892C11.4869 -0.0732974 11.0122 -0.0732974 10.719 0.219892L6.00012 4.93894L1.28102 0.219892C0.987847 -0.0732974 0.51306 -0.0732974 0.219883 0.219892C-0.0732943 0.512831 -0.0732943 0.988138 0.219883 1.28108L4.93898 6.00012L0.219883 10.7192C-0.0732943 11.0121 -0.0732943 11.4874 0.219883 11.7804C0.366471 11.9267 0.558587 12 0.750453 12C0.942319 12 1.13444 11.9267 1.28102 11.7801L6.00012 7.06106L10.719 11.7801C10.8656 11.9267 11.0577 12 11.2495 12C11.4414 12 11.6335 11.9267 11.7801 11.7801C12.0733 11.4872 12.0733 11.0119 11.7801 10.7189L7.06127 6.00012Z"
              fill="#A4A4A4"/>
          </svg>

        </div>
      </form>
    </div>

    <div class="adaptive-filter-wrapper">
      <div class="filter-wrapper">
        <button class="filter-btn flex-justify-center flex-align-center filter-toggler">
          <img src="{{ asset('/images/filter-results-button.svg') }}">
        </button>

        <div class="fw jc-space-between mb-20">
          <div class="input-wrapper w270">
            <label>Number of guests</label>
            <div class="filter-item" data-placeholder="Choose guest count" data-type="guest-count"
                 data-item-type="select">
              <div class="selected-item-wrapper">
                2 guests
              </div>

              <div class="dropdown-wrapper">
                <div class="content">
                  <div data-item-type="select" class="single-select-item select-item" data-value="1">1 guest</div>
                  <div data-item-type="select" class="single-select-item select-item" data-value="2">2 guests</div>
                  <div data-item-type="select" class="single-select-item select-item" data-value="3">3 guests</div>
                  <div data-item-type="select" class="single-select-item select-item" data-value="4">4 guests</div>
                  <div data-item-type="select" class="single-select-item select-item" data-value="5">5 guests</div>
                  <div data-item-type="select" class="single-select-item select-item" data-value="6">6 guests and
                    more
                  </div>
                </div>

                <div class="meta fw jc-space-between ai-center">
                  <button class="simple-link btn-reset-filter clear">Clear</button>
                  <button class="btn btn-save-filter filled green">Save</button>
                </div>
              </div>
            </div>
          </div>

          <div class="input-wrapper w270">
            <label>Category</label>
            <div class="filter-item" data-placeholder="Choose category" data-type="category">
              <div class="selected-item-wrapper">
                Choose category
              </div>
              <div class="dropdown-wrapper">
                <div class="content">
                  @foreach(get_categories() as $category)
                    <div class="select-item" data-item-type="checkbox" data-value="{{$category['id']}}">
                      <label class="control control--checkbox"><span>{{$category['title']}}</span>
                        <input
                          class="step-data-control"
                          data-control-type="checkbox"
                          data-control-required="true"
                          data-control-name="terms-agree"
                          type="checkbox"
                        >
                        <div class="control__indicator"></div>
                      </label>
                    </div>
                  @endforeach
                </div>
                <div class="meta fw jc-space-between ai-center">
                  <button class="simple-link btn-reset-filter clear">Clear</button>
                  <button class="btn btn-save-filter filled green">Save</button>
                </div>
              </div>
            </div>
          </div>

          <div class="input-wrapper w270">
            <label>Dates</label>
            <div class="filter-item" data-placeholder="Any date" data-type="dates">
              <div class="selected-item-wrapper">Any date</div>

              <div class="dropdown-wrapper">
                <div class="content">
                  <div
                    data-item-type="datepicker"
                    data-control-name="search-tour-dates"
                    class="select-item is-datepicker is-search-page-datepicker is-tourzy-datepicker is-range-datepicker has-no-disabled-dates">
                  </div>
                </div>
                <div class="meta fw jc-space-between ai-center">
                  <button class="btn-reset-filter simple-link clear">Clear</button>
                  <button class="btn-save-filter btn filled green">Save</button>
                </div>
              </div>
            </div>
          </div>

          <div class="input-wrapper w270">
            <label>Price</label>

            <div class="filter-item" data-type="price">
              <div id="price-range"></div>
            </div>
          </div>
        </div>
      </div>

      <div class="fw mb-20">
        <div class="sort-filter" data-active-option="visit_count" data-sort-by="DESC">
          Sort by: <span class="color-green filter-trigger">popular</span>
          <div class="sort-options">
            <ul>
              <li data-value="rating">rating</li>
              <li data-value="visit_count" class="active">popular</li>
              <li data-value="price" data-sort-by="ASC">price low to high</li>
              <li data-value="price" data-sort-by="DESC">price high to low</li>
            </ul>
          </div>
        </div>
      </div>
    </div>

    <div class="tours-container fw flex-wrap mb-24">

      @foreach($tours->items() as $tour)
        @include('components.tour', ['tour' => $tour])
      @endforeach

    </div>

    <div class="empty-search-result @if($tours->total()) hidden @endif">
      <h2>Oooops, we didn't find anything matching your search parameters 🙁</h2>
      <p class="underheader-text">Try changing the filter parameters and you will definitely get lucky!</p>

      <p class="cat-descr">You can try one of this categories:</p>
      <div class="slider-items-cover tour-category-slider-cover flex flex-align-stretch flex-justify-start flex-wrap"
           id="topCategories"></div>

      <img class="no-search-result-icon" src="{{asset('/images/icons/no-search-result.png')}}" alt="">
    </div>

    <div class="ball-loader-wrapper">
      <div class="ball-loader">
        <div class="ball-loader-ball ball1"></div>
        <div class="ball-loader-ball ball2"></div>
        <div class="ball-loader-ball ball3"></div>
      </div>
    </div>
  </div>
@endsection

@push('scripts')
  <script src="{{ mix('/js/search.js') }}"></script>
  <script>
    const datepickerBasicConfig = {
      minDate: new Date('{{$minDate}}'),
      maxDate: new Date('{{$maxDate}}'),
    };

    var tourPaginationData = {
      page: 1,
      more: {{Illuminate\Support\Js::from( $tours->currentPage() < $tours->lastPage())}}
    };

  </script>

@endpush
