@extends('layouts.app')

@section('content')
  <div id="spa">
    <my-tour-list-page />
  </div>
@endsection

@push('scripts')
  <script src="{{ mix('js/core-spa.js') }}"></script>
@endpush