<div id="book" class="popup fw w1000">
  <div class="popup-content datepicker-popup-content pr-45">
    <div data-control-name="book" class="is-datepicker is-book-datepicker is-tourzy-datepicker"></div>
  </div>
  <div class="popup-content pl-45">
    <p class="h2 mb-24">
      {{ $tour->name }}
      <span class="choosed-date small-header color-green"></span>
    </p>

    <div class="general-input-wrapper input-wrapper w100p">
      <label for="numberOfGuests">Number of guests</label>

      <select
        class="select2 step-data-control"
        name="numberOfGuests"

        data-placeholder="Choose number of guests"
        data-control-type="select"
        data-control-required="true"
        data-control-name="numberOfGuests"
      >
        <option></option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
      </select>

      <div class="validation-message"></div>
    </div>

    <div class="general-input-wrapper time-input input-wrapper mb-12">
      <label>Time</label>

      <div class="time-input-wrapper"></div>
    </div>

    <div class="book-summary-block">
      <div class="input-wrapper w100p mb-24">
        <label>Total</label>

        <div class="text-row">
          <p><span class="price">$113</span> x <span class="guests-count">2 guests</span></p>
          <p class="total-price-general">$226</p>
        </div>

        <div class="text-row">
          <p>Book tax (7$ per person)</p>
          <p class="total-tax">$14</p>
        </div>

        <div class="divider"></div>

        <div class="text-row bold">
          <p>Total (USD)</p>

          <p class="total-price">$240</p>
        </div>
      </div>

      <button class="btn submit-book-request filled orange text-center w280">
        Book
      </button>
    </div>
  </div>
</div>