<div id="pay" class="popup fw w1000">
  <div class="popup-content pr-45">
    <p class="h1 mb-24 hide-sm">Review and pay</p>

    <div class="hero-wrapper mb-24">
      <div class="popup-hero-img img-wrapper">
        @if($tour->user->avatar)
          <img class="user-photo"  src="{{ $tour->user->avatar->src }}">
        @else
          <img src="{{ asset('/images/icons/user-placeholder.png') }}">
        @endif
      </div>
      <p class="small-header">
        {{$tour->userfirst_name}} {{$tour->userlast_name}}
      </p>
    </div>

    <p class="h2 mb-16">Payment method</p>

    <div class="option-box mb-24">
      <div class="option-item option-item inactive" data-type="paypal">
        <img src="{{ asset('/images/popups/pay/paypal.svg') }}">
      </div>
      <div class="option-item" data-type="stripe">
        <img src="{{ asset('/images/popups/pay/stripe.svg') }}">
      </div>
    </div>

    <button class="btn submit-pay disabled filled orange ta-center w280">
      Book
    </button>
  </div>

  <div class="popup-content summary-content pl-45">
    <p class="h1 mb-24 show-sm">Review and pay</p>

    <div class="pay-summary-block mb-30">
      <div class="book-summary-block">
        <div class="input-wrapper w100p mb-0">
          <div class="text-row">
            <div>
              <p class="h2 mb-8">{{ $tour->name }}</p>
              <p class="small-header mb-16">by {{$tour->user->first_name}} {{$tour->user->last_name}}</p>
              <p>Tour duration {{ $tour->duration }} hour</p>
            </div>

            <div class="tour-image-wrapper">
              <img src="{{ $tour->cover_photo->src }}" alt="">
            </div>
          </div>

          <div class="divider"></div>

          <div class="text-row mb-24">
            <p>
              <span class="date display-inline-block mb-8">12 Sep 2019 12:00</span> <br>
              <span class="guests-count">2 guests</span>
            </p>

            <a href="#book" class="open-modal simple-link">edit</a>
          </div>

          <div class="text-row">
            <p>
              <span class="price">$113</span> x <span class="guests-count">2 guests</span>
            </p>

            <p class="total-price-general">
              $226
            </p>
          </div>

          <div class="text-row">
            <p>
              Book tax (7$ per person)
            </p>

            <p class="total-tax">
              $14
            </p>
          </div>

          <div class="text-row discount-row color-green hidden">
            <p>
              Discount
            </p>

            <p class="total-discount">
              $10
            </p>
          </div>

          <div class="divider"></div>

          <div class="text-row bold">
            <p>
              Total (USD)
            </p>

            <p class="total-price">
              $240
            </p>
          </div>
        </div>
      </div>
    </div>
    <p class="pl-24 сancellation-policy">
      Here you can find
      <a href="/refund" target="_blank" class="simple-link is-underline">Cancellation policy</a>
    </p>
  </div>
</div>