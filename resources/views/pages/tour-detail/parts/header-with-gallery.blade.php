<div class="header-part detail-tour-section">
  <div class="slider-wrapper">
    <ul>
      <li> <img src="{{ image_url($tour->cover_photo, 'medium') }}" alt=""></li>
      @foreach($tour->gallery_photos as $photo)
        <li><img src="{{ image_url($photo, 'medium')}}" alt=""></li>
      @endforeach
      @foreach($tour->gallery_photos as $photo)
        <li><img src="{{ image_url($photo, 'medium')}}" alt=""></li>
      @endforeach
    </ul>

    <div class="slider-controls-wrapper">
      <div class="slider-controls">
        <div class="control-item prev">
          <svg width="16" height="12" viewBox="0 0 16 12" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path
              d="M0.217894 5.47531L4.97923 0.71397C5.26973 0.423468 5.74087 0.423468 6.03137 0.71397C6.32193 1.00453 6.32193 1.47555 6.03137 1.76611L2.54006 5.25742H15.256C15.6669 5.25742 16 5.59053 16 6.00138C16 6.41216 15.6669 6.74534 15.256 6.74534H2.54006L6.03125 10.2366C6.32181 10.5272 6.32181 10.9982 6.03125 11.2888C5.88603 11.4339 5.69557 11.5067 5.50518 11.5067C5.31479 11.5067 5.12439 11.4339 4.97911 11.2888L0.217894 6.52744C-0.0726681 6.23688 -0.0726681 5.76587 0.217894 5.47531Z"
              fill="#25B29D"/>
          </svg>
        </div>

        <div class="control-item next">
          <svg width="16" height="12" viewBox="0 0 16 12" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path
              d="M15.7821 5.47531L11.0208 0.71397C10.7303 0.423468 10.2591 0.423468 9.96863 0.71397C9.67807 1.00453 9.67807 1.47555 9.96863 1.76611L13.4599 5.25742H0.743959C0.333115 5.25742 0 5.59053 0 6.00138C0 6.41216 0.333115 6.74534 0.743959 6.74534H13.4599L9.96875 10.2366C9.67819 10.5272 9.67819 10.9982 9.96875 11.2888C10.114 11.4339 10.3044 11.5067 10.4948 11.5067C10.6852 11.5067 10.8756 11.4339 11.0209 11.2888L15.7821 6.52744C16.0727 6.23688 16.0727 5.76587 15.7821 5.47531Z"
              fill="#25B29D"/>
          </svg>
        </div>
      </div>
    </div>
  </div>

  <div class="tour-info">
    <div class="box green">
      <div class="item">
        <img src="{{ asset('/images/tour-details/icons/icon-clock.svg') }}" alt="">
        <p>{{ $tour->duration }} hours</p>
      </div>

      <div class="item">
        <img src="{{ asset('/images/tour-details/icons/icon-point.svg') }}" alt="">
        <p>{{ $tour->city_location ?? 'NULL' }}</p>
      </div>

      <div class="item">
        <img src="{{ asset('/images/tour-details/icons/icon-add-user.svg') }}" alt="">
        <p>Up to {{ $tour->group_max_size }} person</p>
      </div>

      @if($tour->equip_required_type != 3)
        <div class="item">
          <img src="{{ asset('/images/tour-details/icons/icon-ok.svg') }}">
          <p>Equipment included</p>
        </div>
      @endif

      <div class="item">
        <img src="{{ asset('/images/tour-details/icons/icon-translate.svg') }}" alt="">
        <p>English</p>
      </div>

      <div class="item">
        @if($tour->category)
          <img src="/images/icons/category/{{ $tour->category->slug }}--green.svg" alt="{{$tour->category->title}}">
          <p>{{ $tour->category->title }}</p>
        @else
          <img src="/images/icons/category/other--green.svg" alt="{{$tour->custom_category_title}}">
          {{ $tour->custom_category_title }}
        @endif
      </div>
    </div>
    <div class="box">
      <div class="share-item item">
        <a href="#share-tour" class="block-link open-modal"></a>
        <img src="{{ asset('/images/tour-details/icons/icon-share.svg') }}" alt="">
        <p>Share with friends</p>
      </div>
    </div>
  </div>
</div>