<div class="header-part header-part-purchased detail-tour-section fw ai-start">
  <div class="img-wrapper w290 mr-24">
    <img src="{{ image_url($tour->cover_photo, 'medium') }}" alt="">
  </div>

  <div class="flex-1">
    <div class="fw ai-center jc-space-between mb-40 tour-title-wrapper">
      <h1 class="mb-0 tour-title">{{ $tour->name ?? 'no name provided' }}</h1>

      <div class="tour-info">
        <div class="box pr-0 pt-0 pb-0">
          <div class="share-item item">
            <a href="#share-tour" class="block-link open-modal"></a>
            <img src="{{ asset('/images/tour-details/icons/icon-share.svg') }}">
            <p>Share with friends</p>
          </div>
        </div>
      </div>
    </div>

    <div class="tour-info multi-box mb-40">
      <div class="box green">
        <div class="item">
          <img src="{{ asset('/images/tour-details/icons/icon-clock.svg') }}">
          <p>{{ $tour->duration }} hours</p>
        </div>

        <div class="item">
          <img src="{{ asset('/images/tour-details/icons/icon-point.svg') }}">
          <p>{{ $tour->city_location ?? 'NULL' }}</p>
        </div>

        <div class="item">
          <img src="{{ asset('/images/tour-details/icons/icon-add-user.svg') }}">
          <p>Up to {{ $tour->group_max_size }} person</p>
        </div>

        @if($tour->equip_required_type != 3)
          <div class="item">
            <img src="{{ asset('/images/tour-details/icons/icon-ok.svg') }}">
            <p>Equipment included</p>
          </div>
        @endif

        <div class="item">
          <img src="{{ asset('/images/tour-details/icons/icon-translate.svg') }}">
          <p>English</p>
        </div>

        <div class="item">
          @if($tour->category)
            <img src="/images/icons/category/{{ $tour->category->slug }}--green.svg" alt="{{$tour->category->title}}">
            <p>{{ $tour->category->title }}</p>
          @else
            <img src="/images/icons/category/other--green.svg" alt="{{$tour->custom_category_title}}">
            {{ $tour->custom_category_title }}
          @endif
        </div>
      </div>
    </div>
    <div class="hero-info fw ai-start jc-space-between hide-sm">
      <div class="fw ai-start">
        <div class="hero-img-wrapper mr-16">
          @if($tour->user->avatar)
            <img  src="{{ $tour->user->avatar->src }}">
          @endif
        </div>
        <div>
          <p class="small-header color-green mb-16">{{ $tour->user->first_name }} {{ $tour->user->last_name }}</p>
          <p class="mb-4">{{ $tour->user->phone_number }}</p>
          <p class="color-green"><a href="mailto:sam.cooper@gmail.com">{{ $tour->user->email }}</a></p>
        </div>
      </div>

      <div class="fw fd-column">
        <a href="#contact-host" class="open-modal btn unfilled orange">
          Contact host
        </a>
      </div>
    </div>
  </div>
</div>
