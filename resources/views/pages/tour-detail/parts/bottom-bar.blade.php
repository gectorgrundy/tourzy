<div class="bottom-bar">
  <div class="dashboard-container p-0 fw jc-space-between hide-xs">
    <div>
      <p class="h1 mb-12">
        {{ $tour->name }}
      </p>

      <div class="reviews-total-info">
        <div class="stars-wrapper">

          @for($i = 1; $i <=5; $i++)
            @if($i <= $tour->rating)
              <img src="{{asset('/images/tour-details/icons/icon-star.svg')}}">
            @else
              <img class="inactive" src="{{asset('/images/tour-details/icons/icon-star.svg')}}">
            @endif
          @endfor
        </div>

        <span class="color-grey">
            <span class="color-grey">({{$reviews->total() }} reviews)</span>
				</span>
      </div>
    </div>

    <div>
      <p class="mb-12 ta-right">
        <span class="h2 color-green bold">$<span
            class="tour-detail-price">{{ number_format($tour->price, 2, '.', ',') }}</span></span> per person
      </p>

      @if($tour->user->is_blocked)
        <p class="h2 color-red ta-center">Sorry, this tour is not available at the moment</p>
      @elseif(!$scheduleData->count())
        <a class="btn filled orange ta-center w280">No available dates</a>
      @else
        <a href="#book" class="btn open-modal filled orange ta-center w280">Check availability</a>
      @endif
    </div>
  </div>

  <div class="dashboard-container p-0 fw jc-space-between show-xs">
    <div class="fw ai-center jc-space-between mb-12">
      <p class="mb-0">
        <span class="h2 color-red bold">
          $<span class="tour-detail-price">{{ number_format($tour->price, 2, '.', ',') }}</span>
        </span>
        per person
      </p>

      <div class="mobile-bottom-bar-stars">
        <div class="reviews-total-info">
          <div class="stars-wrapper mini-stars">
            @for($i = 1; $i <=5; $i++)
              @if($i <= $tour->rating)
                <img src="{{asset('/images/tour-details/icons/icon-star.svg')}}">
              @else
                <img class="inactive" src="{{asset('/images/tour-details/icons/icon-star.svg')}}">
              @endif
            @endfor
          </div>

          <span class="color-green">{{ $tour->rating }}.0</span>
        </div>

        <span class="color-grey">({{$reviews->total() ?? 0}} reviews)</span>
      </div>
    </div>

    <div class="action-button-wrapper">
      @if($tour->user->is_blocked)
        <p class="h2 color-red ta-center">Sorry, this tour is not available at the moment</p>
      @elseif(!$scheduleData->count())
        <a class="btn filled orange ta-center w280">No available dates</a>
      @else
        <a href="#book" class="btn open-modal filled orange ta-center w280">Check availability</a>
      @endif
    </div>
  </div>
</div>
