<section class="detail-tour-section reviews-section" data-section-description="Book">
  <p class="h1 mb-24">Available dates</p>

  <div class="fw mb-24 calendar-month-column">
    <div data-control-name="detail-page-choose-date" class="is-datepicker show-avaialable-only this-month is-tourzy-datepicker mr-50"></div>
    <div data-control-name="detail-page-choose-date" data-plus-month="1" class="is-datepicker show-avaialable-only is-tourzy-datepicker mr-50"></div>
    <div data-control-name="detail-page-choose-date" data-plus-month="2" class="is-datepicker show-avaialable-only is-tourzy-datepicker"></div>
  </div>
</section>