@extends('layouts.app')

@section('content')
  <div class="dashboard-container detail-tour-container">
    @if($tour->gallery_photos->count())
      @include('pages.tour-detail.parts.header-with-gallery')
    @else
      @include('pages.tour-detail.parts.header')
    @endif

    @include('components.tour-detail.description', [
        'youtube_video_link' => $tour->youtube_video_link,
        'experience_description' => $tour->experience_description,
        'additional_requirements' => $tour->additional_requirements,
        'places_description' => $tour->places_description,
    ])

    @include('components.tour-detail.author', ['user' => $tour->user])

    @include('components.tour-detail.notice', [
        'equip_required_type' => $tour->equip_required_type,
        'equipment_what_to_bring' => $tour->equipment_what_to_bring,
    ])

    @include('components.tour-detail.meeting-place', [
        'city_meet' => $tour->city_meet,
        'direction_description' => $tour->direction_description,
        'address_meet' => $tour->address_meet,
        'latitude' => $tour->latitude,
        'longitude' => $tour->longitude,
    ])

    @include('components.tour-detail.reviews', [
        'rating' => $tour->rating,
        'canLeaveFeedback' => false,
    ])
  </div>

  @include('components.tour-detail.faq', ['notes' => $tour->notes])
  <div class="dashboard-container detail-tour-container mt-0 pt-80">

    @if(!$tour->user->is_blocked && $scheduleData->count())
      @include('pages.tour-detail.parts.available-dates')
    @endif
    {{--    {{ include('@Core/detail-tour-page/parts/related-tours.html.twig') }}--}}


    <div class="hidden">

      @include('pages.tour-detail.modals.book')
      @include('pages.tour-detail.modals.pay')
      @include('components.tour-detail.modals.contact')
      @include('components.tour-detail.modals.share')
    </div>

    @include('pages.tour-detail.parts.bottom-bar')

  </div>
@endsection

@push('scripts')
  @include('components.google-maps')
  <script src="{{mix('/js/tour-detail.js')}}"></script>
  <script>
    const USER_AGE = +` @auth {{intval(date("Y")) - intval(data_get(auth()->user(), 'year_of_birth', -1))}}  @elseauth {{-1}} @endauth `
    const MIN_AGE = +` {{ $tour->group_min_age }} `

    const tour = {
      id: +`{{ $tour->id }}`,
      price: +`{{ $tour->price }}`,
    }

    let sheduledDates = {{ Illuminate\Support\Js::from($scheduleData->toArray()) }}
      sheduledDates = sheduledDates.map(sheduledDate => ({
      ...sheduledDate,
      date: new Date(sheduledDate.date),
    })).filter(scheduleData => scheduleData.date instanceof Date && !isNaN(scheduleData.date)) || [];

    const availableDates = sheduledDates.map(sheduledDate => {
      sheduledDate.date.setHours(0);
      return sheduledDate.date;
    });

  </script>
@endpush