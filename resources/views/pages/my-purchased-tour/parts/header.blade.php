<div class="header-part header-part-purchased detail-tour-section fw ai-start">
  <div class="img-wrapper w290 mr-24">
    <img src="{{ image_url($tour->cover_photo, 'medium') }}" alt="">
  </div>
  <div class="flex-1">
    <div class="fw ai-center jc-space-between mb-40 tour-title-wrapper">
      <h1 class="mb-0 tour-title">{{ $booking->name ?? 'no name provided' }}</h1>

      <div class="tour-info">
        <div class="box pr-0 pt-0 pb-0">
          <div class="share-item item">
            <a href="#share-tour" class="block-link open-modal"></a>
            <img src="{{ asset('/images/tour-details/icons/icon-share.svg') }}">
            <p>Share with friends</p>
          </div>
        </div>
      </div>
    </div>

    <div class="tour-info multi-box mb-40">
      <div class="box green">
        <div class="item">
          <img src="{{ asset('/images/tour-details/icons/icon-clock.svg') }}">
          <p>{{ $booking->duration }} hours</p>
        </div>

        <div class="item">
          <img src="{{ asset('/images/tour-details/icons/icon-point.svg') }}">
          <p>{{ $tour->city_location ?? 'NULL' }}</p>
        </div>

        @if($booking->equip_required_type != 3)
          <div class="item">
            <img src="{{ asset('/images/tour-details/icons/icon-ok.svg') }}">
            <p>Equipment included</p>
          </div>
        @endif

        <div class="item">
          <img src="{{ asset('/images/icons/calendar.svg') }}">
          <p>{{ $booking->date->format('d.m.Y') }} {{ $booking->time . ':00' }}</p>
        </div>

        <div class="item">
          <img src="{{asset('/images/tour-details/icons/icon-add-user.svg')}}">
          <p>{{ $booking->guest_count }} persons</p>
        </div>

        <div class="item">
          <img src="{{ asset('/images/icons/coin.svg') }}">
          <p>Total (USD)&nbsp;
            <span class="color-red">
              ${{ number_format($booking->price * $booking->guest_count, 2, '.', ',') }}
            </span>
          </p>
        </div>

      </div>
    </div>

    <div class="hero-info fw ai-start jc-space-between hide-sm">
      <div class="fw ai-start">
        <div class="hero-img-wrapper mr-16">
          @if($tour->user->avatar)
            <img src="{{ $tour->user->avatar->src }}">
          @endif
        </div>
        <div>
          <p class="small-header color-green mb-16">{{ $tour->user->first_name }} {{ $tour->user->last_name }}</p>
          <p class="mb-4">{{ $tour->user->phone_number }}</p>
          <p class="color-green"><a href="mailto:{{ $tour->user->email }}">{{ $tour->user->email }}</a></p>
        </div>
      </div>
      @if($booking->status === \App\Models\Booking::STATUS_SUCCESS && $booking->activation_status === \App\Models\Booking::ACTIVATION_STATUS_PENDING)
        <div class="fw fd-column">
          @if($moneyBackData['percent'] > 0)
            <a href="#cancel" class="btn open-modal unfilled orange ta-center w280 mb-24">
              Cancel reservation
            </a>
          @else
            <button class="btn filled orange ta-center w280 mb-24 disabled">
              No Refund
            </button>
          @endif

          <a
            href="{{google_calendar_link($booking)}}"
            target="_blank"
            class="btn filled orange ta-center w280"
          >
            + Add to calendar
          </a>
        </div>
      @endif

      @if($booking->status === \App\Models\Booking::STATUS_SUCCESS && $booking->activation_status === \App\Models\Booking::ACTIVATION_STATUS_CANCELED)
        <button class="btn filled orange ta-center w280 mb-24 disabled">Cancellation pending</button>
      @endif
    </div>
  </div>
</div>
