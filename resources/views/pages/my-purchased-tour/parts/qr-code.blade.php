<section class="detail-tour-section">
  <p class="h1 mb-24">
    QR code
  </p>
  <div class="qr-code-wrapper fw">
    <div class="qr-code-img mr-24">
      <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')
                        ->size(230)->errorCorrection('H')
                        ->generate(route('my-booking-activate', ['token' => $booking->confirmation_token] ))) !!} ">
    </div>
    <div class="text-wrapper">
      <p class="h2 color-green mb-24">
        Show this QR code
      </p>
      <p class="small-header mb-40">
        <span class="font-weight-lighter">Confirmation code:</span>
        <span class="confirmation-code bold">{{ $booking->confirmation_token }}</span>
        <img class=" ml-4 copy-confirmation-code" src="{{asset('/images/icons/copy.svg')}}">
      </p>
      <a
        href="{{ route('my-booking-qr-code', ['booking' => $booking->id]) }}"
        download="tourzy-qr-{{ $booking->id }}.png"
        class="btn download-qr unfilled orange ta-center w280"
      >
        Download
      </a>
    </div>
  </div>
</section>
