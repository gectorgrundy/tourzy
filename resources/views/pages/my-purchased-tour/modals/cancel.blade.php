<div id="cancel" class="popup fw flex-wrap w1000">
  <div class="popup-content pr-45 pb-0">
    <p class="h1 mb-40">Cancel reservation</p>
    <p class="h2 mb-12">Reason why you cancel reservation?</p>
    <div class="general-input-wrapper w435 input-wrapper mb-8">
      <div class="step-data-control"
           data-control-type="radio"
           data-control-required="true"
           data-control-name="cancel-reason"
      >
        <label class="control control--radio">
          Travel issues
          <input type="radio" name="radio" value="Travel issues"/>
          <div class="control__indicator"></div>
        </label>

        <label class="control control--radio">
          Illness
          <input type="radio" name="radio" value="Illness"/>
          <div class="control__indicator"></div>
        </label>

        <label class="control control--radio">
          Family emergency
          <input type="radio" name="radio" value="Family emergency"/>
          <div class="control__indicator"></div>
        </label>

        <label class="control control--radio">
          Don’t feel like attending
          <input type="radio" name="radio" value="Don’t feel like attending"/>
          <div class="control__indicator"></div>
        </label>

        <label class="control control--radio">
          Too expensive
          <input type="radio" name="radio" value="Too expensive"/>
          <div class="control__indicator"></div>
        </label>

        <label class="control control--radio">
          Other
          <input type="radio" name="radio" value="Other"/>
          <div class="control__indicator"></div>
        </label>
      </div>
    </div>
    <div class="general-input-wrapper input-wrapper w435 mb-24 full-width">
      <label>Add comment <span class="light">(optional)</span></label>

      <textarea
        data-control-type="textarea"
        data-control-min-length="0"
        data-control-max-symbols="450"
        data-control-name="cancel-comment"
        class="step-data-control"
        name="cancel-comment"
        placeholder="Write there some additional information about cancellation"
      ></textarea>

      <div class="validation-message"></div>
    </div>
    <p class="h2 mb-12">Information about return</p>
    <div class="book-summary-block w380 mb-18">
      <div class="input-wrapper w100p mb-0">
        <div class="text-row">
          <p>
            <span class="price">${{ $booking->price }}</span> x
            <span class="guests-count">{{ $booking->guest_count }} guests</span>
          </p>
          <p class="total-price-general">{{ $booking->price * $booking->guest_count }}</p>
        </div>
        <div class="text-row">
          <p>Book tax (7$ per person)</p>
          <p class="total-tax">${{ $booking->guest_count * 7 }}</p>
        </div>
        <div class="divider"></div>
        <div class="text-row bold">
          <p>Total (USD)</p>
          <p class="total-price">${{ $booking->price * $booking->guest_count + $booking->guest_count * 7  }}</p>
        </div>
      </div>
    </div>
  </div>

  <div class="popup-content summary-content pl-45">
    <div class="pay-summary-block mb-30">
      <div class="book-summary-block">
        <div class="input-wrapper w100p mb-0">
          <div class="text-row">
            <div>
              <p class="h2 mb-8">Learn the photo</p>
              <p class="small-header mb-16">by Sam Cooper</p>
              <p>Tour duration {{ $booking->duration }} hour</p>
            </div>

            <div class="tour-image-wrapper">
              <img src="{{asset('/images/popups/pay/pay-image.png')}}">
            </div>
          </div>

          <div class="divider"></div>

          <div class="text-row mb-24">
            <p>
              <span class="date display-inline-block mb-8">{{ $booking->date->format('d M Y h:m') }}</span>
              <br>
              <span class="guests-count">{{ $booking->guest_count }} guests</span>
            </p>
          </div>

          <div class="text-row">
            <p>
              <span class="price">${{ $booking->price }}</span> x <span
                class="guests-count">{{ $booking->guest_count }} guests</span>
            </p>

            <p class="total-price-general">
              ${{ $booking->price * $booking->guest_count }}
            </p>
          </div>

          <div class="text-row">
            <p>
              Book tax (7$ per person)
            </p>

            <p class="total-tax">
              ${{ $booking->guest_count * 7 }}
            </p>
          </div>

          <div class="text-row discount-row color-green hidden">
            <p>
              Discount
            </p>

            <p class="total-discount">
              $10
            </p>
          </div>

          <div class="divider"></div>

          <div class="text-row bold">
            <p>
              Total (USD)
            </p>

            <p class="total-price">
              ${{ $booking->price * $booking->guest_count + $booking->guest_count * 7 }}
            </p>
          </div>

          <div class="divider"></div>

          <p class="ta-center color-green small-header">
            {{ $moneyBackData['percent'] }}% money back
          </p>
        </div>
      </div>
    </div>
    <p class="pl-24 сancellation-policy">
      Here you can find
      <a href="{{route('refund')}}" target="_blank" class="simple-link is-underline">Cancellation policy</a>
    </p>

    <br>

    <p class="ta-center">
      We’re sorry to hear that you need to cancel your trip; we want to have the chance to tour with you! If you cancel 2 days before your trip, you will receive a full refund to the card you originally paid with. You can cancel up to 24 hours in advance and receive a partial refund. Cancellations within 24 hours of the event and after the event will not be refunded unless there are extenuating circumstances.
      <br>
      <br>
      Refunds will post to the original payment method within 5-7 days.
    </p>
  </div>

  <div class="popup-content w100p">
    <div class="fw input-group input-group-multi-column">
      <div class="input-wrapper general-input-wrapper mr-16">
        <label>Bank name</label>

        <input
          data-control-type="input"
          data-control-name="bank-name"

          class="step-data-control"
          name="bank-name"
          placeholder="Bank name"
        >

        <div class="validation-message ta-left"></div>
      </div>

      <div class="input-wrapper general-input-wrapper">
        <label>Routing number</label>

        <input
          data-control-type="input"
          data-control-name="routing-number"

          class="step-data-control"
          name="routing-number"
          placeholder="Routing number"
        >

        <div class="validation-message ta-left"></div>
      </div>
    </div>

    <div class="fw input-group input-group-multi-column">
      <div class="input-wrapper general-input-wrapper mr-16">
        <label>Account number</label>

        <input
          data-control-type="input"
          data-control-name="account-number"

          class="step-data-control"
          name="account-number"
          placeholder="Account number"
        >

        <div class="validation-message ta-left"></div>
      </div>

      <div class="input-wrapper general-input-wrapper">
        <label>Account holder name</label>

        <input
          data-control-type="input"
          data-control-name="account-holder-name"

          class="step-data-control"
          name="account-holder-name"
          placeholder="Account holder name"
        >

        <div class="validation-message ta-left"></div>
      </div>
    </div>

    <input type="hidden" name="booking-id" value="{{$booking->id}}">
    <p class="small-header color-green mb-24">Money will return in next 5 days</p>
    <button class="btn submit-cancel filled orange ta-center w280">
      Cancel reservation
    </button>
  </div>
</div>