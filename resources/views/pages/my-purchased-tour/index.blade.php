@extends('layouts.app')

@section('content')
  <div class="dashboard-container detail-tour-container">

    @include('pages.my-purchased-tour.parts.header')

    @include('components.tour-detail.description', [
        'youtube_video_link' => $tour->youtube_video_link,
        'experience_description' => $tour->experience_description,
        'additional_requirements' => $tour->additional_requirements,
        'places_description' => $tour->places_description,
    ])

    @include('components.tour-detail.notice', [
        'equip_required_type' => $tour->equip_required_type,
        'equipment_what_to_bring' => $tour->equipment_what_to_bring,
    ])

    @include('components.tour-detail.meeting-place', [
       'city_meet' => $tour->city_meet,
       'direction_description' => $tour->direction_description,
       'address_meet' => $tour->address_meet,
        'latitude' => $tour->latitude,
        'longitude' => $tour->longitude,
    ])

    @if($booking->status === \App\Models\Booking::STATUS_SUCCESS && $booking->activation_status === \App\Models\Booking::ACTIVATION_STATUS_PENDING)
      @include('pages.my-purchased-tour.parts.qr-code')
    @endif

    @include('components.tour-detail.reviews', [
        'rating' => $tour->rating,
        'canLeaveFeedback' => $booking->activation_status === \App\Models\Booking::ACTIVATION_STATUS_SUCCESS && !$booking->current_user_review,
    ])

  </div>
  @include('components.tour-detail.faq', ['notes' => $tour->notes])

  <div class="hidden">
    @include('components.tour-detail.modals.share')
    @include('pages.my-purchased-tour.modals.cancel')
  </div>

@endsection
@push('scripts')
  @include('components.google-maps')
  <script src="{{mix('/js/tour-detail.js')}}"></script>
@endpush