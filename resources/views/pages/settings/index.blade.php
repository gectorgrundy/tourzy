@extends('layouts.app')

@section('content')

  <div id="spa">
    <user-settings-page />
  </div>
@endsection

@push('scripts')
  <script src="{{ mix('js/core-spa.js') }}"></script>
@endpush