@extends('layouts.app')

@section('content')
  <div class="dashboard-container payments-container detail-tour-container schedule-container">
    <h1>Payments</h1>

    <div class="fw ai-start main-payments-flexbox">
      <div class="data-table-wrapper table-wrapper hide-sm w800">
        <table class="data-table">
          <thead>
          <tr>
            <th>Payment ID</th>
            <th>Payment date</th>
            <th>Status</th>
            <th>Amount</th>
          </tr>
          </thead>
          <tbody>
          @foreach($payouts as $payout)
            <tr>
              <td>{{ $payout->id }}</td>
              <td>{{ $payout->created_at->format('d.m.Y H:i') }}</td>
              <td>
                @if($payout->type === \App\Models\Payout::TYPE_HOST_EARNING)
                  <span class="color-green">Earnings for "{{ $payout->booking->tour->name }}"</span>
                @else
                  @if($payout->status === \App\Models\Payout::STATUS_PENDING)
                    <span class="color-orange">Withdrawal under review</span>

                  @elseif($payout->status === \App\Models\Payout::STATUS_SUCCESS)
                    <span class="color-green">Withdrawal success</span>

                  @else
                    <span class="color-red">
                      Withdrawal declined
                      <span data-tippy-content="{{ $payout->decline_message }}" class="tippy ml-4">
                        <img src="{{ asset('/images/icons/info-icon-red.svg') }}">
                      </span>
                    </span>
                  @endif
                @endif
              </td>
              <td>${{ $payout->amount }}</td>
            </tr>
          @endforeach
          </tbody>
        </table>
      </div>

      <div class="payments-wrapper show-sm">
        <div class="input-wrapper w270 mb-24 show-xs">
          <div class="filter-item" data-placeholder="Any date" data-type="dates">
            <div class="selected-item-wrapper">
              Any date
            </div>

            <div class="dropdown-wrapper">
              <div class="content">
                <div
                  data-item-type="datepicker"
                  data-control-name="search-tour-dates"
                  class="select-item is-datepicker is-search-page-datepicker is-tourzy-datepicker is-range-datepicker has-no-disabled-dates">
                </div>
              </div>
              <div class="meta fw jc-space-between ai-center">
                <button class="btn-reset-filter simple-link clear">Clear</button>
                <button class="btn-save-filter btn filled green">Save</button>
              </div>
            </div>
          </div>
        </div>
        @foreach($payouts as $payout)
          <div class="payment-item">
            <p class="payment-title">
              @if($payout->type === \App\Models\Payout::TYPE_HOST_EARNING)
                <span class="color-green">Earnings for "{{ $payout->booking->tour->name }}"</span>
              @else
                @if($payout->status === \App\Models\Payout::STATUS_PENDING)
                  <span class="color-orange">Withdrawal under review</span>

                @elseif($payout->status === \App\Models\Payout::STATUS_SUCCESS)
                  <span class="color-green">Withdrawal success</span>

                @else
                  <span class="color-red">
                      Withdrawal declined
                      <span data-tippy-content="{{ $payout->decline_message }}" class="tippy ml-4">
                        <img src="{{ asset('/images/icons/info-icon-red.svg') }}">
                      </span>
                    </span>
                @endif
              @endif
            </p>

            <div class="fw ai-end jc-space-between">
              <div>
                <p class="payment-item-data">
                  <span class="payment-item-data-lable">ID: </span>
                  <span class="payment-item-data-value">{{ $payout->id }}</span>
                </p>
                <p class="payment-item-data">
                  <span class="payment-item-data-lable">Date: </span>
                  <span
                    class="payment-item-data-value">{{ $payout->created_at->format('d.m.Y H:i') }}</span>
                </p>
              </div>

              <p class="payment-amount">${{ $payout->amount }}</p>
            </div>
          </div>
        @endforeach
      </div>

      <div class="balance-wrapper">
        <div class="balance-box">
          <p class="h2 color-white mb-18">Account balance</p>
          <p class="balance color-white h1 mb-18">${{ $availableAmount }}</p>
        </div>

        <div class="open-moda-wrapper">
          <a href="#withdraw" class="open-modal btn filled orange w280 ta-center bolder">
            Withdraw
          </a>
        </div>
      </div>
    </div>
  </div>
  <div class="hidden">
    @include('pages.my-payments.modals.withdrawal')
  </div>
@endsection

@push('scripts')
  <script src="{{mix('/js/payments.js')}}"></script>

@endpush