<div id="withdraw" class="popup w700">
  <div class="popup-content">
    <h1 class="mb-24">Withdrawal of funds</h1>

    <div class="input-wrapper general-input-wrapper full-width">
      <label>Sum of withdrawal</label>

      <input
        data-control-type="input"
        data-control-name="withdraw-sum"

        class="step-data-control"
        name="withdraw-sum"
        data-no-value-msg="Please, enter the title of your tour"
        placeholder="Sum must be less than your account balance">

      <div class="validation-message ta-left"></div>
    </div>

    <div class="fw input-group input-group-multi-column">
      <div class="input-wrapper general-input-wrapper mr-16">
        <label>Bank name</label>

        <input
          data-control-type="input"
          data-control-name="bank-name"

          class="step-data-control"
          name="bank-name"
          placeholder="Bank name">

        <div class="validation-message ta-left"></div>
      </div>

      <div class="input-wrapper general-input-wrapper">
        <label>Routing number</label>

        <input
          data-control-type="input"
          data-control-name="routing-number"

          class="step-data-control"
          name="routing-number"
          placeholder="Routing number">

        <div class="validation-message ta-left"></div>
      </div>
    </div>

    <div class="fw input-group input-group-multi-column">
      <div class="input-wrapper general-input-wrapper mr-16">
        <label>Account number</label>

        <input
          data-control-type="input"
          data-control-name="card-number"

          class="step-data-control"
          name="account-number"
          placeholder="Account number">

        <div class="validation-message ta-left"></div>
      </div>

      <div class="input-wrapper general-input-wrapper">
        <label>Account holder name</label>

        <input
          data-control-type="input"
          data-control-name="card-number"

          class="step-data-control"
          name="account-holder-name"
          placeholder="Account holder name">

        <div class="validation-message ta-left"></div>
      </div>
    </div>

    <button class="submit-withdrawal-request btn filled orange w380 mt-2">
      Submit
    </button>
  </div>
</div>