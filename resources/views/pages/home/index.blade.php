@extends('layouts.app')

@section('content')

  @include('pages.home.parts.banner')

  @if($popularTours->count())
    @include('pages.home.parts.popular-tours')
  @endif

  @include('pages.home.parts.tour-category')

  @if($restTours->count())
    @include('pages.home.parts.rest-tours')
  @endif

@endsection