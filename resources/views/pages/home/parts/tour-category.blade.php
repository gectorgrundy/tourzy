<div class="container third-block">
  <div class="block-main-title">Choose what you like!</div>
  <div class="slider-cover">
    <div class="prev-item flex flex-align-center flex-justify-center flex-wrap">
      <img src="{{  asset('/images/triangle.svg') }}" alt="prev">
    </div>
    <div class="slider-items-cover tour-category-slider-cover flex flex-align-stretch flex-justify-start flex-wrap" id="topCategories">
      @foreach($categories as $category)
        <div class="tour-category-item-cover">
          <a class="tour-category-item" href="/search?category={{$category['id']}}">
            <div class="tour-category-item-title">{{$category['title']}}</div>
            <div class="tour-category-item-goto-button flex flex-align-center flex-justify-center flex-wrap"></div>
          </a>
        </div>
      @endforeach
    </div>
    <div class="next-item flex flex-align-center flex-justify-center flex-wrap">
      <img src="{{  asset('/images/triangle.svg') }}" alt="prev">
    </div>
  </div>
</div>