<div class="container second-block">
  <div class="block-main-title">Popular in {{ current_city() }}</div>
  <div class="slider-cover">
    <div class="slider-items-cover popular-slider-cover flex flex-align-stretch flex-justify-start flex-wrap">
      @foreach($popularTours as $tour)
        <div class="tour-item-cover">
          @include('components.tour', ['tour' => $tour, 'full' => true])
        </div>
      @endforeach

    </div>
  </div>
  <div class="popular-show-more flex flex-align-center flex-justify-center flex-wrap">
    <a href="{{ route('search') }}" class="button button-show-more">Show more</a>
  </div>
</div>
