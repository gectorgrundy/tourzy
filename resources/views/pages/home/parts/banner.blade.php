<div class="full-size-block flex flex-align-center flex-justify-center">
  <div class="container flex flex-align-center flex-justify-center flex-wrap">
    <div class="search-form">
      <div class="block-title">
        Unique tours <span>hosted</span><br><span>by</span> locals
      </div>
      <form class="form-search" action="{{ route('search') }}" method="get">
        <div class="form-search-cover flex flex-align-stretch flex-justify-center">
          <div class="input-search-cover flex flex-align-stretch flex-justify-center">
            <input name="search" class="search-input home-search-input" type="text" required placeholder=" {{ current_city() }}">
            <input id="submit-search" type="submit" class="hidden">
            <label class="form-submit-label button button-explore" for="submit-search">Explore</label>
          </div>
        </div>
      </form>
    </div>
  </div>
  <div class="arrow-scroll" data-scroll data-target=".second-block">
    <span>Scroll down</span>
    <img src="{{  asset('/images/arrow-down-sign-to-navigate.svg') }}" alt="arrow">
  </div>
</div>
