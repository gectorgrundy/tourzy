<div class="container four-block">
  <div class="block-main-title">All tours in {{ current_city() }}</div>

  <div class="slider-cover">
    <div class="prev-item flex flex-align-center flex-justify-center flex-wrap">
      <img src="{{  asset('/images/triangle.svg') }}" alt="prev">
    </div>
    <div class="slider-items-cover all-slider-cover flex flex-align-stretch flex-justify-start flex-wrap">
      @foreach($restTours as $tour)
        <div class="tour-item-cover">
          @include('components.tour', ['tour' => $tour, 'full'=> true])
        </div>
      @endforeach
    </div>
    <div class="next-item flex flex-align-center flex-justify-center flex-wrap">
      <img src="{{  asset('/images/triangle.svg') }}" alt="prev">
    </div>
  </div>

  <div class="all-show-more flex flex-align-center flex-justify-center flex-wrap">
    <a href="{{ route('search') }}" class="button button-show-more">Show more</a>
  </div>
</div>
