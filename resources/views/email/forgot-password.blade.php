<p>Hello, {{ $user->first_name }} {{$user->last_name}}!</p>
<p>To verify your email, please follow the link below:{{ route('registration_confirmation', ['token' => $user->confirmation_token]) }}</p>

<p>Regards,Tourzy team</p>
