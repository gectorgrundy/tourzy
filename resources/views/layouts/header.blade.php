@php
  $isHomePage = Request::routeIs('home');
@endphp
<div class="header-cover @if(!$isHomePage) active-header @endif">
  <div class="container">
    <div class="header d-flex align-items-center justify-content-between flex-wrap">
      <div class="logo-side">
        <a href="/">
          <img src="{{  asset('/images/logo.png') }}" alt="tourzy">
        </a>
        @if(!$isHomePage)
          <div class="header-search-form position-relative">
            <form action="{{ route('search') }}" method="GET">
              <input type="text" name="search" placeholder="{{current_city()}}" required value="{{Request::get('search')}}">

              <a href="{{ route('search') }}" class="header-search-clear"><i class="feather icon-x"></i></a>
            </form>
          </div>
        @endif
      </div>

      <div class="hide-md menu flex flex-wrap flex-justify-center flex-align-center">
        @guest
          <div class="menu-item flex flex-align-stretch flex-justify-center flex-wrap">
            <a href="#login" data-action data-target="login"
               class="flex flex-align-center flex-justify-center flex-wrap">
              Log in
            </a>
          </div>
          <div class="menu-item button-cover flex flex-align-stretch flex-justify-center flex-wrap">
            <a href="#sign" data-action data-target="sign"
               class="button-wrapper flex flex-align-center flex-justify-center flex-wrap">
              <span class="button button-sign">Sign up</span>
            </a>
          </div>
        @endguest

        @auth
          @if(Auth::user()->hasFinishedTour)
            <div class="menu-item flex flex-align-stretch flex-justify-center flex-wrap">
              <a href="{{route('my-tour-list')}}" class="flex flex-align-center flex-justify-center flex-wrap">
                My Hosted Tour
              </a>
            </div>

            <div class="menu-item flex flex-align-stretch flex-justify-center flex-wrap">
              <a href="{{route('my-payments')}}" class="flex flex-align-center flex-justify-center flex-wrap">
                My Payments
              </a>
            </div>

            <div class="menu-item flex flex-align-stretch flex-justify-center flex-wrap">
              <a href="{{route('my-schedule')}}" class="flex flex-align-center flex-justify-center flex-wrap">
                My Schedule
              </a>
            </div>

          @else
            <div class="menu-item flex flex-align-stretch flex-justify-center flex-wrap">
              <a href="{{route('host')}}" class="flex flex-align-center flex-justify-center flex-wrap">
                Host
              </a>
            </div>
            <div class="menu-item flex flex-align-stretch flex-justify-center flex-wrap">
              <a href="{{route('my-bookings')}}">My tours</a>
            </div>
          @endif
          @if(Auth::user()->hasRole('admin'))
            <div class="menu-item flex flex-align-stretch flex-justify-center flex-wrap">
              <a href="{{route('admin.home')}}" class="flex flex-align-center flex-justify-center flex-wrap">Admin</a>
            </div>
          @endif

          <div class="menu-item hero-wrapper-item flex flex-align-stretch flex-justify-center flex-wrap">
            <span class="hero-wrapper flex flex-align-center flex-justify-center flex-wrap">
              @if(Auth::user()->avatar)
                <img class="user-photo " src="{{ image_url(Auth::user()->avatar, 'small') }}" alt="{{Auth::user()->avatar->alt}}">
              @else
                <img class="user-photo no-avatar" src="{{ asset('/images/default-avatar.svg') }}" alt="No Avatar">
              @endif
            </span>
            <div class="menu-item-dropdown">
              <ul>
                <li><a href="{{route('my-bookings')}}">My tours</a></li>
                <li><a href="{{ route('settings') }}">Settings</a></li>
                <li><a href="{{ route('logout') }}">Log out</a></li>
              </ul>
            </div>
          </div>
        @endauth
      </div>
    </div>
  </div>
</div>