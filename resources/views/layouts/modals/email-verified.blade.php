<div
  class="message-success message-success-register flex flex-align-stretch flex-justify-between flex-wrap"
  data-item="email-verified"
>
  <div class="message-success-cover">
    <div class="message-success-title">Email Verified Successfully</div>
    <div class="message-success-content">
      Your email address was successfully verified
    </div>
  </div>
  <div class="message-success-close" data-close data-close-target="email-verified"></div>
</div>