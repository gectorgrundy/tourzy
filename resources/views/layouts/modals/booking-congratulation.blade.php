@php $booking = \App\Models\Booking::with('user')->find(Session::get('booking-congratulation')) @endphp
<div class="message-success message-success-register flex flex-align-stretch flex-justify-between flex-wrap" data-item="booking-congratulation">
  <div class="message-success-cover">
    <div class="message-success-title">Congratulation</div>
    <div class="message-success-content">
      You successfully buy tour "{{$booking->name}}" by {{$booking->user->first_name}} {{$booking->user->last_name}}
      that will be {{$booking->date->format('F d')}} at {{ $booking->time . ':00' }}. All information about your tour nd cancellation you can find in your profile "My tours"
    </div>

    <div class="button-cover submit-cover forgot-password-submit">
      <a class="button button-explore" href="{{route('my-booking', ['booking' => $booking->id])}}">
        Review the tour details
      </a>
    </div>
  </div>
  <div class="message-success-close" data-close data-close-target="booking-congratulation"></div>
</div>