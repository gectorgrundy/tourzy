<div class="form-cover flex flex-align-stretch flex-justify-between flex-wrap" data-item="reset-password">
  <div class="form-left-side">
    <div class="form-left-side-title">Set New</div>
    <div class="form-left-side-text-before">If you remember your password, just</div>
    <a data-close data-close-target="reset-password" data-action data-target="login" class="button button-form">log in</a>
  </div>
  <div class="form-right-side">
    <div class="form-right-side-title">Set New Password</div>

    <div class="form-warning">
      <img src="{{ asset('/images/icons/danger.svg') }}">
      <p>It seems that this e-mail is not registered within our platform</p>
    </div>

    <form method="post" id="reset-password-form">
      <input type="hidden" name="token" value="{{Session::get('reset-password-token')}}">
      <input type="hidden" name="email" value="{{Session::get('reset-password-email')}}">
      <div class="input-cover">
        <label for="new-password">Password</label>
        <input
          type="password"
          data-validate="_validateLength"
          min="8"
          name="password"
          id="new-password"
          required="required"
          placeholder="At least 8 symbols"
        >
        <span class="show-password" data-toggle-text="hide pass">show pass</span>
        <div class="warning">Password should contain at least 8 characters</div>
      </div>
      <div class="button-cover submit-cover forgot-password-submit">
        <button type="submit" class="button button-explore button-form-submit btn">Submit</button>
      </div>
    </form>
  </div>
  <div class="form-close" data-close data-close-target="reset-password"></div>
</div>