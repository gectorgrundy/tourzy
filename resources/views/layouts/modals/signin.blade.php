<div class="form-cover flex flex-align-stretch flex-justify-between flex-wrap" data-item="sign">
  <div class="form-left-side">
    <div class="form-left-side-title">Sign up</div>
    <div class="form-left-side-text">Start your journey with us</div>
    <div class="form-left-side-text-before">Or if you have already account just</div>
    <a data-close data-close-target="sign" data-action data-target="login" class="button button-form">Log in</a>
  </div>
  <div class="form-right-side">
    <div class="form-right-side-title">Sign up to Tourzy</div>

    <div class="form-warning">
      <img src="{{asset('images/icons/danger.svg')}}" alt="danger">
      <p>It seems that this e-mail is already registered within our platform</p>
    </div>

    <form method="post" id="sign_form">
      <div class="input-cover">
        <label for="sign-email">E-mail</label>
        <input type="email" data-validate="_validateEmail" id="sign-email" placeholder="Enter email" name="email" required="required">
        <div class="warning">wrong email address</div>
      </div>
      <div class="input-cover">
        <label for="first-name">First name</label>
        <input type="text" data-validate="_validateLength" min="1" name="first_name" id="first-name"
               required="required" placeholder="Enter your first name">
        <div class="warning">Please, fill this field</div>
      </div>
      <div class="input-cover">
        <label for="last-name">Last name</label>
        <input type="text" data-validate="_validateLength" min="1" name="last_name" id="last-name" required="required"
               placeholder="Enter your last name">
        <div class="warning">Please, fill this field</div>
      </div>
      <div class="input-cover">
        <label for="sign-password">Password</label>
        <input type="password" data-validate="_validateLength" min="8" name="password" id="sign-password"
               required="required" placeholder="At least 8 symbols">
        <span class="show-password" data-toggle-text="hide pass">show pass</span>
        <div class="warning">Password should contain at least 8 characters</div>
      </div>
      <div class="input-cover birthday-date flex flex-align-stretch flex-justify-between flex-wrap">
        <div class="like-label">
          Birthday <span>(To sign up, you need to be at least 18. Other people won’t see your birthday.)</span>
        </div>

        <div class="like-wrapper">
          <div class="like-select like-select-day" data-like-select="1" data-like-select-target="birthday">
            <div class="like-select-placeholder">day</div>
            <div class="like-select-options-cover">

              @for($i = 1; $i <= 31; $i++)
                <div class="like-select-options">{{ $i < 10 ? 0 : '' }}{{ $i }}</div>
              @endfor
            </div>
          </div>

          <div class="like-select like-select-month" data-like-select="2" data-like-select-target="birthday">
            <div class="like-select-placeholder">month</div>
            <div class="like-select-options-cover">
              @for($i = 1; $i <= 12; $i++)
                <div class="like-select-options">{{ $i < 10 ? 0 : '' }}{{ $i }}</div>
              @endfor
            </div>
          </div>

          <div class="like-select like-select-year" data-like-select="3" data-like-select-target="birthday">
            <div class="like-select-placeholder">year</div>
            <div class="like-select-options-cover">
              @php $currentYear = \Carbon\Carbon::now()->year @endphp

              @for($i = 1933; $i <= $currentYear; $i++)
                <div class="like-select-options">{{ $i }}</div>
              @endfor

            </div>
          </div>
        </div>

        <input
          type="hidden"
          data-validate="_validateLength"
          min="10"
          class="birthday-date-input"
          data-like-select-item="birthday"
          data-like-select-pattern="{1}-{2}-{3}"
          name="birthday"
          required="required"
        >
        <div class="warning">Please, choose your date of birth</div>
      </div>
      <div class="agree input-cover">
        <input type="checkbox" id="sign-agree" data-validate="_validateChecked" class="hidden" name="agree">
        <label for="sign-agree" class="label-agree flex flex-align-center flex-justify-start flex-wrap">
          I agree with <a href="/policy" target="_blank">User Agreement</a>
        </label>
        <div class="warning">Please, check this mark</div>
      </div>
      <div class="button-cover submit-cover text-center">
        <button type="submit" class="button button-explore button-form-submit btn">Create account</button>
      </div>
    </form>
  </div>
  <div class="form-close" data-close data-close-target="sign"></div>
</div>
