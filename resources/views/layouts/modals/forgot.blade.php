<div class="form-cover flex flex-align-stretch flex-justify-between flex-wrap" data-item="forgot">
  <div class="form-left-side">
    <div class="form-left-side-title">Forgot pass</div>
    <div class="form-left-side-text">Don’t worry, we will send you reset link</div>
    <div class="form-left-side-text-before">If you remember your password, just</div>
    <a data-close data-close-target="forgot" data-action data-target="login" class="button button-form">log in</a>
  </div>
  <div class="form-right-side">
    <div class="form-right-side-title">Reset password</div>

    <div class="form-warning">
      <img src="{{ asset('/images/icons/danger.svg') }}">
      <p>It seems that this e-mail is not registered within our platform</p>
    </div>

    <form method="post" id="forgot_form">
      <div class="input-cover forgot-password-text">
        Enter the email address associated with your account, and we’ll email you a link to reset your password.
      </div>
      <div class="input-cover">
        <label for="forgot-email">E-mail</label>
        <input type="email" data-validate="_validateEmail" name="email" placeholder="Enter email" required="required">
      </div>
      <div class="button-cover submit-cover forgot-password-submit text-center">
        <button type="submit" class="button button-explore button-form-submit btn">Send reset link</button>
      </div>
      <p class="mt-3 forgot-password-text forgot-password-text-success hidden">
        Reset password link was sent to your e-mail address. Please, check your inbox
      </p>
    </form>
  </div>
  <div class="form-close" data-close data-close-target="forgot"></div>
</div>