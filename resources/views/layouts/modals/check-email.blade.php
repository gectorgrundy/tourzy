<div class="message-success message-success-register flex flex-align-stretch flex-justify-between flex-wrap" data-item="check-email">
  <div class="message-success-cover">
    <div class="message-success-title">Check mail</div>
    <div class="message-success-content">
      Click on the link in the email we sent you. A verified email address allows you to receive information about your trip.
    </div>
  </div>
  <div class="message-success-close" data-close data-close-target="check-email"></div>
</div>