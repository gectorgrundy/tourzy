<div class="form-cover flex flex-align-stretch flex-justify-between flex-wrap" data-item="login">
  <div class="form-left-side">
    <div class="form-left-side-title">Log in</div>
    <div class="form-left-side-text">Welcome back, my friend!</div>
    <div class="form-left-side-text-before">Or if you don’t have account yet just</div>
    <a data-close data-close-target="login" data-action data-target="sign" class="button button-form">Sign up</a>
  </div>
  <div class="form-right-side">
    <div class="form-right-side-title">Log in to Tourzy</div>

    <div class="form-warning">
      <img src="{{asset('/images/icons/danger.svg')}}" alt="danger">
      <p>Your email or password is incorrect, please try again.</p>
    </div>

    <form method="post" id="login_form">
      @csrf
      <div class="input-cover">
        <label for="login-email">E-mail</label>
        <input type="text" id="login-email" data-validate="_validateEmail" placeholder="Enter email" name="email" required="required">
        <div class="warning">wrong email address</div>
      </div>
      <div class="input-cover">
        <label for="login-password">Password</label>
        <input type="password" name="password" data-validate="_validateLength" min="5" id="login-password" required="required" placeholder="Enter password">
        <span class="show-password" data-toggle-text="hide pass">show pass</span>
        <div class="warning">password to short</div>
      </div>
      <div class="button-cover forgot-password-cover">
        <a class="forgot-password" data-close data-close-target="login" data-action data-target="forgot">Forgot password?</a>
      </div>
      <div class="button-cover submit-cover text-center">
        <button type="submit" class="button button-explore button-form-submit btn">Log in</button>
      </div>
    </form>
  </div>
  <div class="form-close" data-close data-close-target="login"></div>
</div>