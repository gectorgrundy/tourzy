@php
  $isTourVerificationPage = Route::currentRouteName() === 'tour-verification';
  $isTourInstructionPage = Route::currentRouteName() === 'tour-instruction';
@endphp

@if(!$isTourVerificationPage && !$isTourInstructionPage)
  <div class="footer-cover">
    <div class="container">
      <div class="footer flex flex-align-center flex-justify-between flex-wrap">
        <div class="footer-top flex flex-align-start flex-justify-between flex-wrap">
          <div class="logo-side">
            <a href="/">
              <img src="{{  asset('/images/logo.png') }}" alt="tourzy">
            </a>
          </div>
          <div class="footer-top-menu">
            @auth
              @if(Auth::user()->hasFinishedTour)
                <div class="footer-top-menu-item"><a href="{{ route('my-tour-list') }}">My Hosted Tour</a></div>
                <div class="footer-top-menu-item"><a href="{{ route('my-payments') }}">My Payments</a></div>
                <div class="footer-top-menu-item"><a href="{{ route('my-schedule') }}">My Schedule</a></div>
              @else
                <a href="{{ route('host') }}">Become a host</a>
              @endif
            @endauth

            @guest
              <div class="footer-top-menu-item">
                <a href="#2" data-action data-target="login">Log in</a>
              </div>
              <div class="footer-top-menu-item">
                <a href="#3" data-action data-target="sign">Sign up</a>
              </div>
            @endguest
          </div>
          <div class="footer-top-contact-us">
            <div class="footer-top-title">Contact us</div>
            <a href="mailto:support@tourzy.net">support@tourzy.net</a>
          </div>
          <div class="footer-top-follow-us">
            <div class="footer-top-title">Follow us</div>
            <div class="footer-top-text">yes, we are social</div>
            <div class="footer-top-social flex flex-align-center flex-justify-start flex-wrap">
              <a target="_blank" href="https://www.facebook.com/TourzyNet/" class="social social-facebook"></a>
              <a target="_blank" href="https://www.instagram.com/tourzynet/?hl=en" class="social social-instagram"></a>
              <a target="_blank" href="https://twitter.com/tourzynet?lang=en" class="social social-twitter"></a>
            </div>
          </div>
        </div>
        <div class="footer-bottom flex flex-align-center flex-justify-between flex-wrap">
          <div class="footer-bottom-copyrights">Tourzy, 2019</div>

          <div style="color: #A4A4A4 !important; font-size: 14px;">
            Created by
            <a
              style="color: #fc4a1a !important"
              rel="nofollow"
              href="https://wildwebart.com/"
              target="_blank">Wild Web Art</a>
          </div>

          <div class="footer-bottom-side-menu flex flex-align-center flex-justify-between flex-wrap">
            <div class="footer-bottom-side-menu-item">
              <a href="{{route('policy')}}">Privacy Policy, Terms, and Agreements</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endif