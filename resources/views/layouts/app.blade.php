<!DOCTYPE html>
<html lang="ru">
<head>
  <title>Tourzy</title>
  <meta charset="utf-8"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
  <meta content='width=device-width' name='viewport'/>
  <link rel="shortcut icon" href="{{ asset('/images/favicon.ico') }}">
  <link href="{{ mix('/css/core-style.css') }}" rel="stylesheet">
  <meta name="csrf-token" content="{{ csrf_token() }}">

</head>

<body class="@if(!Request::routeIs('home')) is-dashboard-page @endif">

@include('layouts.header')


<div class="main-container">
  @yield('content')
</div>

@include('layouts.footer')

@guest
  @include('layouts.modals.login')
  @include('layouts.modals.signin')
  @include('layouts.modals.forgot')

  @if(Session::exists('reset-password-token'))
    @include('layouts.modals.reset-password')
  @endif
@endguest

@include('layouts.modals.email-verified')
@include('layouts.modals.check-email')

@if(Session::exists('booking-congratulation'))
  @include('layouts.modals.booking-congratulation')
@endif

<div class="overlay" data-close data-close-target="login|sign|forgot|check-email|email-verified|booking-congratulation|reset-password"></div>

<div class="message message-cover flex flex-justify-center flex-align-center flex-wrap">
  <div class="message-cover-content-container">
    <div class="message-cover-content"></div>
  </div>
</div>
<script src="{{ mix('/js/vendor.js') }}"></script>
<script src="{{ mix('/js/app.js') }}"></script>
@stack('scripts')

<script>
  function gPlacesReady() {
    let event = new CustomEvent('google-maps-ready', {detail: {}});
    document.dispatchEvent(event);
  }

  @if(Session::exists('need-login'))
    $(function () {
      $("a[data-target='login']").click()
    });
  @endif

  @if(Session::exists('check-email'))
    @php Session::remove('check-email') @endphp
    let checkEmail = true
  @endif

  @if(Session::exists('email-verified'))
    @php Session::remove('email-verified') @endphp
    let showEmailVerifiedModal = true
  @endif
  @if(Session::exists('reset-password-token'))
    @php Session::remove('reset-password-token') @endphp
    let showResetPasswordModal = true
  @endif
  @if(Session::exists('need-login'))
    @php Session::remove('need-login') @endphp
    let showLoginModal = true
  @endif
  @if(Session::exists('booking-congratulation'))
    @php Session::remove('booking-congratulation') @endphp
    let showBookingCongratulationModal = true
  @endif

  let notificationToShow = {{ Illuminate\Support\Js::from(Session::get('notification')) }}

</script>
</body>
</html>