@extends('admin.layout.app')

@section('content')
  <div class="row">
    <div class="col-md-12 grid-margin">
      <div class="d-flex justify-content-between flex-wrap">
        <div class="d-flex align-items-end flex-wrap">
          <div class="row breadcrumbs">
            <div class="d-flex col-md-12">
              <a href="/">
                <i class="mdi mdi-home text-muted hover-cursor"></i>
              </a>
              <p class="text-muted mb-0 hover-cursor">&nbsp;/&nbsp;<a href="{{route('admin.home')}}">Dashboard</a></p>
              <p class="text-muted mb-0 hover-cursor">&nbsp;/&nbsp;<a href="{{route('admin.users')}}">Users List</a></p>
              <p class="text-primary mb-0">&nbsp;/&nbsp;User - {{ $user->first_name }}</p>
            </div>
          </div>
          <div class="mr-md-3 mr-xl-5">
            <h2>User - {{ $user->first_name }} {{ $user->last_name }}</h2>
          </div>
        </div>
        <div class="d-flex justify-content-between align-items-end flex-wrap"></div>
      </div>
    </div>
  </div>

  <div id="spa" >
    <user-single-page :user-id="{{ $user->id }}" />
  </div>
@endsection

@push('scripts')
  <script src="{{ mix('/js/admin-spa.js') }}"></script>

@endpush