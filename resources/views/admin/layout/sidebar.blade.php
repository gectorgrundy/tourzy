@php
  $isHomePage = Route::currentRouteName() === 'admin.home';
@endphp

<nav class="sidebar sidebar-offcanvas" id="sidebar">
  <ul class="nav">
    <li class="nav-item {{ $isHomePage ? 'active' : '' }}">
      <a class="nav-link" href="{{route('admin.home')}}">
        <i class="mdi mdi-home menu-icon"></i>
        <span class="menu-title">Dashboard</span>
      </a>
    </li>
    <li class="nav-item {{ Route::currentRouteName() === 'admin.users' ? 'active' : '' }}">
      <a class="nav-link" data-toggle="collapse" href="#user" aria-expanded="false" aria-controls="user">
        <i class="mdi mdi-account menu-icon"></i>
        <span class="menu-title">Users Pages</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse" id="user">
        <ul class="nav flex-user sub-menu">
          <li class="nav-item">
            <a class="nav-link" href="{{route('admin.users')}}">Users List</a>
          </li>
        </ul>
      </div>
    </li>
    <li class="nav-item {{ Route::currentRouteName() === 'admin.tours' ? 'active' : '' }}">
      <a class="nav-link" data-toggle="collapse" href="#tour" aria-expanded="false" aria-controls="tour">
        <i class="mdi mdi-run menu-icon"></i>
        <span class="menu-title">Tours Pages</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse" id="tour">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item">
            <a class="nav-link" href="{{route('admin.tours')}}">Tours List</a>
          </li>
        </ul>
      </div>
    </li>
    <li class="nav-item {{ Route::currentRouteName() === 'admin.payouts' ? 'active' : '' }}">
      <a class="nav-link" href="{{route('admin.payouts')}}">
        <i class="mdi mdi-cash-multiple menu-icon"></i>
        <span class="menu-title">Payout List</span>
      </a>
    </li>
  </ul>
</nav>