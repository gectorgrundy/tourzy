<!doctype html>
<html lang="ru">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Tourzy Admin</title>
  <link rel="stylesheet" href="{{ asset('/static/admin/vendors/mdi/css/materialdesignicons.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/static/admin/vendors/base/vendor.bundle.base.css') }}">
  <link rel="stylesheet" href="{{ asset('/static/admin/vendors/datatables.net-bs4/dataTables.bootstrap4.css') }}">

  <link href="{{ mix('css/admin-style.css') }}" rel="stylesheet">

  <link rel="shortcut icon" href="{{ asset('/img/favicon.ico') }}?v=2">
</head>
<body>
<div class="container-scroller">
  @include('admin.layout.header')
  <div class="container-fluid page-body-wrapper">
    @include('admin.layout.sidebar')
    <div class="main-panel">
      <div class="content-wrapper">
        @yield('content')
      </div>
      <footer class="footer">
        <div class="d-sm-flex justify-content-center justify-content-sm-between">
          <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2020 <a href="/" target="_blank">Tourzy</a>. All rights reserved.</span>
        </div>
      </footer>
    </div>
  </div>
</div>
<script src="{{ asset('/static/admin/vendors/base/vendor.bundle.base.js') }}"></script>
<script src="{{ asset('/static/admin/vendors/chart.js/Chart.min.js') }}"></script>
<script src="{{ asset('/static/admin/vendors/datatables.net/jquery.dataTables.js') }}"></script>
<script src="{{ asset('/static/admin/vendors/datatables.net-bs4/dataTables.bootstrap4.js') }}"></script>
<script src="{{ asset('/static/admin/js/off-canvas.js') }}"></script>
<script src="{{ asset('/static/admin/js/hoverable-collapse.js') }}"></script>
<script src="{{ asset('/static/admin/js/template.js') }}"></script>
<script src="{{ asset('/static/admin/js/dashboard.js') }}"></script>
<script src="{{ asset('/static/admin/js/data-table.js') }}"></script>
<script src="{{ asset('/static/admin/js/jquery.dataTables.js') }}"></script>
<script src="{{ asset('/static/admin/js/dataTables.bootstrap4.js') }}"></script>


@stack('scripts')
</body>
</html>