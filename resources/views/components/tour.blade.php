<div class="tour-box @if(isset($full)) full-width @endif">
  <a href="{{ route('tour-detail', ['tour' => $tour]) }}" class="block-link"></a>
  <div class="img-wrapper">
    <div class="tag">{{ data_get($tour->category, 'title', $tour->custom_category_title) }}</div>
    @if($tour->cover_photo)
      <img src="{{ image_url($tour->cover_photo, 'small') }}" alt="">
    @endif
  </div>

  <div class="text-wrapper">
    <p class="title">{{ $tour->name }}</p>

    <div class="show-sm">
      <div class="reviews-mob-view fw ai-center jc-space-between">
        <div class="reviews-count">
          <img src="{{ asset('/images/tour-details/icons/icon-star.svg') }}">
          <span class="color-green">{{ $tour->rating }} ({{$tour->reviews_count ?? 0}} <span class="reviews-none"> reviews</span>)</span>
        </div>

        <span class="time">{{ $tour->duration }} hours</span>
      </div>
    </div>

    <div class="reviews-total-info small hide-sm">
      <div class="stars-wrapper">
        @for($i = 1; $i <=5; $i++)
          @if($i <= $tour->rating)
            <img src="{{asset('/images/tour-details/icons/icon-star.svg')}}">
          @else
            <img class="inactive" src="{{asset('/images/tour-details/icons/icon-star.svg')}}">
          @endif
        @endfor
      </div>

      <span class="color-grey">({{$tour->review_count ?? 0}} <span class="reviews-none"> reviews</span>)</span>
    </div>

    <p class="country">{{ $tour->city_location }}</p>

    <div class="fw jc-space-between ai-center">
      <p class="price">
        <span class="dollar-sign">$</span><span class="number">{{ number_format($tour->price, 2, '.', ',') }}</span>
        per person
      </p>

      <p class="time hide-sm">
        {{ $tour->duration }} hours
      </p>
    </div>
  </div>
</div>