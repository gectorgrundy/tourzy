<section class="detail-tour-section host-information-section">
  <p class="h1 mb-24">Host information</p>

  <div class="hero-info fw ai-center jc-space-between">
    <div class="fw ai-center">
      <div class="hero-img-wrapper round mr-16">
        @if($tour->user->avatar)
          <img  src="{{ $tour->user->avatar->src }}">
        @endif
      </div>

      <div>
        <p class="small-header color-green mb-0">{{ $tour->user->first_name }} {{ $tour->userlast_name }}</p>
      </div>
    </div>

    <div class="fw fd-column contact-btn-wrapper">
      <a href="#contact-host" class="open-modal btn unfilled orange">
        Contact host
      </a>
    </div>
  </div>
</section>