<div class="has-background-grey">
  <div class="dashboard-container detail-tour-container mt-0 pb-40">
    <section class="detail-tour-section faq-tour mb-0" data-section-description="Ask">
      <p class="h1 mb-24">FAQ</p>

      @if($tour->notes)
        <div class="question-row mb-24">
          <p class="title">Notes</p>
          <p class="answer">
            {{ $tour->notes}}
          </p>
        </div>
      @endif

      <div class="question-row mb-24">
        <p class="title">What makes Tourzy different than other travel guides?</p>
        <p class="answer">
          Tourzy is completely unique when compared to all other types of guided travel; we’ve completely flipped the guided tour industry upside-down. We connect locals with tourists, so when you’re visiting somewhere new, you can get a personalized tour to everywhere you want to go from a real local – not an agency. You’ll be able to pick amongst dozens of tours anywhere you go, so you can visit the main attractions, the hidden gems, or both! It’s totally up to you. If you’re in an area with lots to do, sign up to be a Tourzy guide and start making money!
        </p>
      </div>

      <div class="question-row mb-24">
        <p class="title">What types of activities can Tourzy guides lead me on?</p>
        <p class="answer">
          Tourzy guides lead tourists on all different types of activities. There are classes and workshops everywhere you visit – you’ll find classes on making wine, cheese, and much more. You can also find food and drink tours, history tours, tours on the arts and entertainment, sports and outdoors tours, and far more. Check out our extensive list of available tours and find one that works for you and your entire group – there’s tons of possibilities.
        </p>
      </div>

      <div class="question-row mb-24">
        <p class="title">Where are Tourzy guides from?</p>
        <p class="answer">
          All of the guides on Tourzy are local to the areas where they do their tours; that’s what makes us so unique. And don’t think that Tourzy is just a place for you to do tours as a tourists; you can sign up to be a guide in your local area and start making money with Tourzy today!
        </p>
      </div>

      <div class="question-row">
        <p class="title">How much does it cost to tour with Tourzy?</p>
        <p class="answer">
          All of our tours have varying costs, depending on the activity. If you search the location you’re visiting, you’ll be able to pick from a variety of categories, and then you can find a tour that works according to your budget and what you’d like to see and visit. We’re so excited to help you see the world!
        </p>
      </div>
    </section>
  </div>
</div>