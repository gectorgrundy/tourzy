<section class="detail-tour-section full-tour-description tour-description">
  <div>
    <div class="notice">
      <div class="img-wrapper">
        <svg width="60" height="60" viewBox="0 0 60 60" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M29.7039 11.3828C27.6236 11.3828 26 12.4991 26 14.478C26 20.5158 26.7102 29.1921 26.7102 35.2301C26.7104 36.803 28.0805 37.4625 29.704 37.4625C30.9217 37.4625 32.6468 36.803 32.6468 35.2301C32.6468 29.1923 33.3571 20.516 33.3571 14.478C33.3571 12.4993 31.6828 11.3828 29.7039 11.3828Z" fill="#25B29D"/><path d="M29.907 40.9648C27.6746 40.9648 26 42.7407 26 44.8718C26 46.9521 27.6744 48.7788 29.907 48.7788C31.9873 48.7788 33.7633 46.9521 33.7633 44.8718C33.7633 42.7407 31.9871 40.9648 29.907 40.9648Z" fill="#25B29D"/></svg>
      </div>

      <p>
        @if($equip_required_type === \App\Models\Tour::EQUIP_TYPE_I_WILL_PROVIDE_EQUIPMENT)
          This host will provide all needed equipment
        @endif
        @if($equip_required_type === \App\Models\Tour::EQUIP_TYPE_DOES_NOT_EQUIPMENT)
            This tour does not require any kind of equipment
        @endif
        @if($equip_required_type === \App\Models\Tour::EQUIP_TYPE_REQUIRED)
            This host doesn't provide additional equipment.
            Please, pay attention to the following Host`s instruction or the list of things that you should bring with you on this tour.

            {!! $equipment_what_to_bring !!}
        @endif
      </p>
    </div>
  </div>
</section>