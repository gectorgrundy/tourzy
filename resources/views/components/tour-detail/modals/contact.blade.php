<div id="contact-host" class="popup w700">
  <div class="popup-content">
    <h1 class="mb-24">Contact host</h1>
    <div class="input-wrapper general-input-wrapper full-width">
      <label>Write a message</label>

      <textarea
        data-control-type="textarea"
        data-control-min-length="0"
        data-control-max-symbols="450"
        placeholder="Write there everything you are interested in tour"
      ></textarea>

      <div class="validation-message"></div>
    </div>

    <input type="hidden" name="tour-id" value="{{$tour->id}}">
    <button class="submit-step btn filled orange w380 d-block my-0 mx-auto">
      Send
    </button>
  </div>
</div>
