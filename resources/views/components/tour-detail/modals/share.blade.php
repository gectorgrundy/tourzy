<div id="share-tour" class="popup w540">
  <div class="popup-content">
    <h1 class="mb-0">Share with friends</h1>
    <h2 class="mb-24 color-green">{{ $tour->name }}</h2>

    <div class="share-buttons-wrapper">
      <a href="http://www.facebook.com/sharer.php?u={{ route('tour-detail', ['tour' => $tour->id]) }}" target="_blank" class="share-button">
        <img src="{{ asset('/images/share-icons/facebook.svg') }}">
        <span>Facebook</span>
      </a>

      <a href="https://twitter.com/share?url={{ route('tour-detail', ['tour' => $tour->id]) }}&amp;text={{ $tour->name }}&amp;hashtags=#tourzy" target="_blank" class="share-button">
        <img src="{{ asset('/images/share-icons/twitter.svg') }}">
        <span>Twitter</span>
      </a>

      <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url={{ route('tour-detail', ['tour' => $tour->id]) }}" target="_blank" class="share-button">
        <img src="{{ asset('/images/share-icons/linkedin.svg') }}">
        <span>LinkedIn</span>
      </a>

      <a href="http://reddit.com/submit?url={{ route('tour-detail', ['tour' => $tour->id]) }}&amp;title={{ $tour->name }}" target="_blank" class="share-button">
        <img src="{{ asset('/images/share-icons/reddit.svg') }}">
        <span>Reddit</span>
      </a>

      <a href="mailto:?Subject={{ $tour->name }}&amp;Body=I%20saw%20this%20and%20thought%20of%20you!%20 {{ route('tour-detail', ['tour' => $tour->id]) }}" target="_blank" class="share-button">
        <img src="{{ asset('/images/share-icons/email.svg') }}">
        <span>Email</span>
      </a>
    </div>
    <img class="share-popup-image" src="{{ asset('/images/popups/share-popup/share-popup-image.png') }}">
  </div>
</div>