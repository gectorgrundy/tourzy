<section class="detail-tour-section tour-description" data-section-description="Explore">
  <p class="h1 mb-24">Tour description</p>

  @if($youtube_video_link)

    <div class="fw video-box">
      <div class="videoWrapper">
        <div class="iframeWrapper">
          <iframe
            width="560"
            height="315"
            src="{{preg_replace(
                "/\s*[a-zA-Z\/\/:\.]*youtu(be.com\/watch\?v=|.be\/)([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i",
                "https://www.youtube.com/embed/$2",
                $youtube_video_link
            )}}"
            frameborder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowfullscreen
          ></iframe>
        </div>
      </div>

      <div class="textWrapper">
        {!! $experience_description !!}
      </div>
    </div>

    <br>
  @endif

  @if($experience_description)
    <div>
      <div class="textWrapper w900">

        @if(!$youtube_video_link)
          {!! $experience_description !!}
        @endif

        <h2 class="color-green">Places to visit</h2>
        <div class="place-description">
          {!! $places_description !!}
        </div>
      </div>
    </div>
  @endif

  @if($additional_requirements)
    <div>
      <div class="textWrapper w900">
        <h2 class="color-green">Additional requirements</h2>
        <div class="place-description">
          {!! $additional_requirements !!}
        </div>
      </div>
    </div>
  @endif
</section>