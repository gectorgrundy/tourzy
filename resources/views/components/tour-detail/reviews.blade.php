<section class="detail-tour-section reviews-section mb-0" data-section-description="Read">
  <div class="fw ai-center mb-24">
    <p class="h1 m-0">Reviews</p>

    @if($canLeaveFeedback)
      <button class="btn filled orange ta-center w280 ml-16" id="show_review_form_btn">Leave a feedback</button>
    @endif
  </div>

  <div class="fw mb-24">
    <div class="reviews-total-info">
      <div class="stars-wrapper">
        @for($i = 1; $i <=5; $i++)
          @if($i <= $tour->rating)
            <img src="{{asset('/images/tour-details/icons/icon-star.svg')}}">
          @else
            <img class="inactive" src="{{asset('/images/tour-details/icons/icon-star.svg')}}">
          @endif
        @endfor
      </div>

      <span class="color-grey">({{ $reviews->total() }} reviews)</span>
    </div>
  </div>

  @auth
    @if($canLeaveFeedback)
      <form action="{{ route('review-create', ['booking' => $booking->id])  }}" method="post" class="review-form" style="display: none;">
        <input type="hidden" name="rating">
        @csrf
        <div class="fw ai-center">
          <div class="img-wrapper">
            @if($tour->user->avatar)
              <img src="{{ $tour->user->avatar->src }}">
            @else
              <img src="{{asset('/images/user-placeholder.png')}}">
            @endif
          </div>

          <div class="text-wrapper">
            <div class="hero-name">{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}</div>
          </div>
        </div>
        <div class="fw mt-12">
          <div class="text-wrapper mr-8">
            Rate the tour:
          </div>

          <div class="stars-wrapper stars-control">
            <img class="inactive" src="{{asset('/images/tour-details/icons/icon-star.svg')}}" data-value="1">
            <img class="inactive" src="{{asset('/images/tour-details/icons/icon-star.svg')}}" data-value="2">
            <img class="inactive" src="{{asset('/images/tour-details/icons/icon-star.svg')}}" data-value="3">
            <img class="inactive" src="{{asset('/images/tour-details/icons/icon-star.svg')}}" data-value="4">
            <img class="inactive" src="{{asset('/images/tour-details/icons/icon-star.svg')}}" data-value="5">
          </div>
        </div>

        <div class="general-input-wrapper mt-12">

        <textarea
          data-control-type="textarea"
          data-control-min-length="100"
          data-control-max-symbols="1200"
          data-control-name="review"
          class="step-data-control "
          name="review"
          placeholder="Write a few words about your experience"
        ></textarea>
        </div>

        <div class="fw mt-24">
          <button type="submit" class="btn disabled filled orange ta-center w280" id="submit_review_btn">
            Publish my feedback
          </button>

          <button type="button" class="simple-link orange ml-6" id="hide_review_form_btn">
            Cancel
          </button>
        </div>
      </form>
    @endif
  @endauth

  <div class="fw reviews-detail-row mb-24">

    @if($reviews->total())
      @foreach($reviews->items() as $review)
        <div class="review-detail">
          <div class="review-details">
            <div class="fw">
              <div class="img-wrapper">
                @if($review->user->avatar)
                  <img src="{{ $review->user->avatar->src }}">

                @else
                  <img src="{{ asset('/images/default-avatar.svg') }}">

                @endif

              </div>
              <div class="text-wrapper">
                <div class="hero-name">{{ $review->user->first_name }} {{ $review->user->last_name }}</div>
                <div class="hero-stars">

                  @for($i = 1; $i <=5; $i++)
                    @if($i <= $review->rating)
                      <img src="{{asset('/images/tour-details/icons/icon-star.svg')}}">
                    @else
                      <img class="inactive" src="{{asset('/images/tour-details/icons/icon-star.svg')}}">
                    @endif
                  @endfor
                </div>
              </div>
            </div>

            <div class="date">
              {{ $review->created_at->format('d M Y')}}
            </div>
          </div>

          <div class="review-content">
            @if(strlen($review->text) > 200)
              <p class="less-text">
                {{ substr($review->text, 0, 200) }} ...
              </p>
              <p class="full-text" style="display: none;">
                {{ $review->text}}
              </p>
              <a class="show-more" href="#">show more</a>
              <a class="show-less" style="display: none;" href="#">show less</a>

            @else
              <p>{{ $review->text }}</p>
            @endif
          </div>
        </div>
      @endforeach
    @else
      No reviews left yet
    @endif
  </div>


  @if($reviews->total() > 3)
    <div class="ta-center">
      <button class="btn unfilled orange w280" id="load-reviews">Show more</button>
    </div>
  @endif
</section>

@push('scripts')
  <script>
    let reviewPaginationData = {
      tourId: {{ $tour->id }},
      page: 1,
      more: {{Illuminate\Support\Js::from( $reviews->total() > 3)}}
    }

    $(function () {
      if (Boolean((new URLSearchParams(window.location.search).get('feedback')))) {
        $('html, body').animate({
          scrollTop: $(".reviews-section").offset().top - 100
        }, 1000);
      }

      let reviewForm = $('.review-form');
      let showFormBtn = $('#show_review_form_btn');
      let hideFormBtn = $('#hide_review_form_btn');
      let submitReviewBtn = $('#submit_review_btn');

      showFormBtn.click(function () {
        showFormBtn.hide();
        reviewForm.show();
      });

      hideFormBtn.click(function () {
        reviewForm.hide();
        showFormBtn.show();
      });

      reviewForm.find('.stars-wrapper img')
        .mouseenter(function () {
          let rating = reviewForm.find(":input[name='rating']").val();
          let onStar = parseInt($(this).data('value'));

          if (!rating) {
            $(this).parent().children('img').each(function (e) {
              if (e < onStar) {
                $(this).removeClass('inactive');
              } else {
                $(this).addClass('inactive');
              }
            });
          }
        })
        .mouseleave(function () {
          let rating = reviewForm.find(":input[name='rating']").val();
          if (!rating) {
            $(this).parent().children('img').each(function (e) {
              $(this).addClass('inactive');
            });
          }
        });


      reviewForm.find('.stars-wrapper img').on('click', function () {
        let onStar = parseInt($(this).data('value'));
        reviewForm.find(":input[name='rating']").val(onStar).trigger("change");

        $(this).parent().children('img').each(function (e) {
          if (e < onStar) {
            $(this).removeClass('inactive');
          } else {
            $(this).addClass('inactive');
          }
        });
      });

      reviewForm.on('change', function (event) {
        let rating = $(this).find(":input[name='rating']").val();
        let review = $(this).find(":input[name='review']").val();

        if (rating && review.length) {
          return submitReviewBtn.removeClass('disabled')
        }

        return submitReviewBtn.addClass('disabled')

      })
    });

  </script>
@endpush

