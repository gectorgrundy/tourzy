<section class="detail-tour-section meeting-place" data-section-description="Meet">
  <p class="h1 mb-24">
    Meeting place
    <span class="small-header color-green">
			{{ $city_meet }}, <span class="address-meet">{{ $address_meet }}</span>
		</span>
  </p>

  @if($direction_description)
    <div class="fw">
      <div class="mapWrapper flex-1 mr-24">
        <div class="iframeWrapper">
          <div style="height: 250px" id="map"></div>
        </div>
      </div>

      <div class="div w280">
        <p>{!! $direction_description !!}</p>
      </div>
    </div>
  @else
    <div class="mapWrapper">
      <div class="iframeWrapper">
        <div style="height: 250px" id="map" data-lat="{{$latitude}}" data-lng="{{$longitude}}"></div>
      </div>
    </div>
  @endif
</section>