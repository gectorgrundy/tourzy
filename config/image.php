<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Image Driver
    |--------------------------------------------------------------------------
    |
    | Intervention Image supports "GD Library" and "Imagick" to process images
    | internally. You may choose one of them according to your PHP
    | configuration. By default PHP's "GD Library" implementation is used.
    |
    | Supported: "gd", "imagick"
    |
    */

    'driver' => 'gd',

    'sizes' => [
        'small' => '270x250',
        'medium' => '310x450',
        'big' => '1230x820',
    ],
    'default' => [
        'tour' => '/images/demo/1.jpg',
        'avatar' => '/images/default-avatar.svg'
    ]
];
