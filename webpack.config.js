let path = require('path');

module.exports = {
  resolve: {
    extensions: ['.js', '.vue', '.json', '.scss'],
    alias: {
      '@validations': path.resolve('./resources/assets/common/utils/validations/validations.js'),

      '@core': path.resolve('./resources/assets/core/js'),
      '@core-api': path.resolve('./resources/assets/core/js/api'),
      '@core-scss': path.resolve('./resources/assets/core/scss'),
      '@core-img': path.resolve('./resources/assets/core/images'),
      '@core-fonts': path.resolve('./resources/assets/core/fonts'),

      '@admin': path.resolve('./resources/assets/admin/js'),
      '@admin-api': path.resolve('./resources/assets/admin/js/api'),
      '@admin-css': path.resolve('./resources/assets/admin/scss'),
      '@admin-img': path.resolve('./resources/assets/admin/images'),
      '@admin-fonts': path.resolve('./resources/assets/admin/fonts'),

      '@common': path.resolve('./resources/assets/common'),
    }
  },
}