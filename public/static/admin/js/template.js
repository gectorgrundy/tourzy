(function($) {
  'use strict';
  $(function() {
    var body = $('body');
    var contentWrapper = $('.content-wrapper');
    var scroller = $('.container-scroller');
    var footer = $('.footer');
    var sidebar = $('.sidebar');

    //Add active class to nav-link based on url dynamically
    //Active class can be hard coded directly in html file also as required

    function filterUrl(url) {
      url = url.split('/');
      var newUrl = [];
      if (url.length > 0) {
        for (var i = 0; i < url.length; i++) {
          if (url[i] !== "") {
            newUrl.push(url[i]);
          }
        }
      }
      return '/' + newUrl.join('/');
    }

    function addActiveClass() {
      var origin = this.origin;
      var element = $(this);
      var currentElementUrl = filterUrl(this.pathname);
      if (current === "") {
        //for root url
        if (element.attr('href').indexOf("index.html") !== -1) {
          element.parents('.nav-item').last().addClass('active');
          if (element.parents('.sub-menu').length) {
            element.closest('.collapse').addClass('show');
            element.addClass('active');
          }
        }
      } else {
        //for other url
        if (origin === location.origin && this.hash.length === 0) {
          if (currentElementUrl === current) {
            element.parents('.nav-item').addClass('active');
            if (element.parents('.sub-menu').length) {
              element.closest('.collapse').addClass('show');
              element.addClass('active');
            }
            if (element.parents('.submenu-item').length) {
              element.addClass('active');
            }
          }
        }
      }
    }

    var current = filterUrl(location.pathname.replace(/^\/|\/$/g, ''));

    $('.nav li a', sidebar).each(addActiveClass);

    //Close other submenu in sidebar on opening any

    sidebar.on('show.bs.collapse', '.collapse', function() {
      sidebar.find('.collapse.show').collapse('hide');
    });


    //Change sidebar

    $('[data-toggle="minimize"]').on("click", function() {
      body.toggleClass('sidebar-icon-only');
    });

    //checkbox and radios
    $(".form-check label,.form-radio label").append('<i class="input-helper"></i>');


  });
})(jQuery);


function confirmBlock() {
  var closest = $(this).closest( ".form-group" );
  var checkbox = closest.find('[type="checkbox"]');
  var textarea = closest.find('textarea');
  var button = $(this);
  var confirm = closest.find('.confirm-block-status');
  if (checkbox) {
    var checked = $(checkbox[0]).attr('checked');
    if (checked == 'checked') {
      changeStatus(button,checkbox);
    } else {
      if (confirm.length > 0) {
        confirm.addClass('active');
        var buttonConfirm = confirm.find('.btn');
        if (buttonConfirm) {
          buttonConfirm.click(_initConfirm.bind(false,button,checkbox,textarea,confirm))
        }
      }
    }
  }


  textarea.on('input', _validateInput.bind(textarea) );

  function _validateInput() {
    var value = this.val();
    if (value.length > 10) {
      this.removeClass('error');
    } else {
      this.addClass('error');
    }
  }

  function changeStatus(button,checkbox) {
    var changeHTML = button.attr('data-change-html');
    var currentHTML = button.html();
    button.addClass('btn-info');
    button.removeClass('btn-primary');
    var element = $(checkbox[0]);
    var checked = element.attr('checked');
    if (checked == 'checked') {
      element.removeAttr('checked');
    } else {
      element.attr('checked','checked');
    }
    button.attr('data-change-html',currentHTML);
    button.html(changeHTML);
  }

  function _initConfirm(button,checkbox,textarea,confirm) {
    textarea.attr('name', 'statusConfirmText');
    var value = textarea.val();
    if (value.length > 10) {
      var changeHTML = button.attr('data-change-html');
      var currentHTML = button.html();
      var element = $(checkbox[0]);
      var checked = element.attr('checked');
      if (checked == 'checked') {
        element.removeAttr('checked');
      } else {
        element.attr('checked','checked');
      }
      button.attr('data-change-html',currentHTML);
      button.html(changeHTML);
      button.removeClass('btn-info');
      button.addClass('btn-primary');
      textarea.removeClass('error');
      confirm.removeClass('active');
      window.user_form.submit();
    } else {
      textarea.addClass('error');
    }
  }
}

function submitForm() {
  if (this.hasOwnProperty('action') && this.hasOwnProperty('modal') ) {
    if (this.modal.prop("tagName") === 'FORM') {
      this.modal.attr('action', this.action);
      this.modal.submit();
    }
  }
}

(function ($elements) {
  var scope = this;
  scope._validate = function(callback,params) {
    scope._checkValidate.call(params);
    if (Object.keys(params.validation).length === 0) {
      if (params.hasOwnProperty('success')) {
        params.success.call(params);
      }
      callback.call(params);
    }
  };

  scope._checkValidate = function () {
    if ( !this.validationElements || this.validationElements.length === 0) return;
    for (var i = 0; i < this.validationElements.length; i++) {
      var validationFunction = this.validationElements[i].getAttribute('data-validation-function');
      if (validationFunction) {
        var name = this.validationElements[i].getAttribute('data-target-validate');
        scope[validationFunction].call(this.validationElements[i],this, name);
      }
    }
  }

  scope._validateTextArea = function (params , name) {
    if ( this == document || !params || !name) return;
    var minValue = this.getAttribute('min');
    var minLength = (minValue) ? minValue : 0;
    if (this.value.length <= minLength){
      params.validation[name] = false;
      scope._addError.call(this);
    } else {
      delete params.validation[name];
      scope._removeError.call(this);
    }
  }

  scope._addError = function() {
    if ( this == document ) return;
    this.classList.add('error');
    this.classList.remove('success');
    this.parentNode.classList.add('error');
    this.parentNode.classList.remove('success');
  };

  scope._removeError = function() {
    if ( this == document ) return;
    this.classList.remove('error');
    this.classList.add('success');
    this.parentNode.classList.remove('error');
    this.parentNode.classList.add('success');
  };

  scope._addListener = function(method, name, params) {
    if ( !method && !name || !params ) return false;
    this.addEventListener('input', scope[method].bind(this, params , name));
    this.removeAttribute('required');
  };

  scope._setValidateName = function() {
    for (var i = 0; i < this.validationElements.length; i++) {
      var $element = $(this.validationElements[i]);
      var name = $element.data('target-validate');
      $element.attr('name', name);
    }
  };

  $elements.each(function (i,e) {
      var params = {};
      params.validation = {};
      params.validationElements = [];
      var modal = $(e).data('modal');
      if (modal) {
        var modalElement = $('[data-modal-target="'+modal+'"]');
        if (modalElement.length > 0) {
          params.modal = modalElement;
          var mode = $(e).data('mode');
          switch (mode) {
            case 'fade':
              params.mode = 'fade';
            break;
            default:
              params.mode = 'show';
          }

          var validate = $(e).data('validate');
          if (validate) {

            validate = validate.split('|');
            for (var a = 0; a < validate.length; a++) {
              var name = validate[a];
              $('[data-target-validate="'+name+'"]', params.modal).each(function (i,e) {
                var validationFunction = $(e).data('validation-function');
                if (validationFunction) {
                  if (scope.hasOwnProperty(validationFunction) && typeof scope[validationFunction] === 'function') {

                    scope._addListener.call(e, validationFunction, name, params);
                    params.validation[name] = false;
                    params.validationElements.push(e);
                  }
                }
              });
            }
          }

          var success = $(e).data('success');
          if (success && window.hasOwnProperty(success)) {
            params.success = window[success];
          }

          var action = $(e).data('action');
          if (action) {
            params.action = action;
          }

          var before = $(e).data('before');
          if (before && window.hasOwnProperty(before)) {
            params.before = window[before];
          }

          var afterClose = $(e).data('after-close');
          if (afterClose && window.hasOwnProperty(afterClose)) {
            params.afterClose = window[afterClose];
          }

        }
      }
      $(e).on('click', function (e) {
        params.event = e;
        params.target = this;
        if (typeof params.before === 'function') {
          params.before.call(params);
        }
        scope._setValidateName.call(params);
        e.preventDefault();
        showModal.call(params);
      });

  });

  function showModal() {
    var params = this;
    if (this.modal) {
      switch (this.mode) {
        case 'fade':
          this.modal.fadeIn();
          break;
        default:
          this.modal.show();
      }

      this.modal.addClass('active');

      console.log(this.modal.find('.overlay'));

      this.modal.find('.overlay').each(function (i, e) {
        $(e).fadeIn();
        $(e).on('click', function () {
          hideModal.call(params);
        });
      });

      this.modal.find('[data-close]').on('click', function (e) {
        e.stopPropagation();
        if (typeof params.afterClose === 'function') {
          params.afterClose.call(params);
        }
        hideModal.call(params);
      });

      this.modal.find('[data-confirm]').on('click', function () {
        if (scope.hasOwnProperty('_validate') && typeof scope['_validate'] === 'function') {
          scope['_validate'](hideModal,params);
        } else {
          hideModal.call(params);
        }
      });
    }
  }

  function hideModal() {
    if (this.modal) {
      var $this = this;
      switch (this.mode) {
        case 'fade':
          this.modal.fadeOut();
        break;
        default:
          this.modal.hide();
      }

      var $timer = setTimeout(function () {
        $this.modal.removeClass('active');
        clearTimeout($timer);
      }, 300);

      this.modal.find('.overlay').fadeOut();
    }
  }

}).call({},jQuery('[data-modal]'));