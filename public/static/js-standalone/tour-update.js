const syncStepViewBySidebar = (scrollToEl = true) => {
    const currentStep = $(".steps-sidebar .sidebar-point.active").index() + 1;
    $(".step-wrapper").toArray().slice(0, currentStep).forEach((stepWrapper, index) => {
        $(stepWrapper).removeClass('active').removeClass('prev');
        $(stepWrapper).addClass( currentStep === (index + 1) ? 'active' : 'prev' );
    });
};

const stepDataSubmitSuccess = () => {
    showNotification('Successful');
}

const dataSendAdditionalParams = () => {
    return {
        update: true,
        id: currentTour.id,
    }
}

$(function() {
    if (typeof currentTour !== 'undefined' && typeof currentTour.step !== 'undefined') {
        $(`.steps-sidebar .sidebar-point:nth-child(${ currentTour.step })`).addClass("active").prevAll().addClass("done");
        syncStepViewBySidebar();
    }

    dataSumbit.init({
        activeStepOnly: false,
        dataSumbitUrl: '/api/tour/step/save',
        successSubmitCallback: stepDataSubmitSuccess,
        dataSendAdditionalParams: dataSendAdditionalParams,
    });

    $('button#save-changes').click(function () {
        dataSumbit.handleSubmitDataClick()
    });

    $(".age-control-checkbox").change(function() {
        if ( $(this).is(':checked') ) {
            $(this)
                .closest(".step-wrapper").find('[data-control-name="groupMinAge"]')
                .val(21).trigger('change')
                .prop('disabled', true);
        } else {
            $(this)
                .closest(".step-wrapper").find('[data-control-name="groupMinAge"]')
                .prop('disabled', false);
        }
    });

    if (typeof currentTour !== 'undefined' && typeof currentTour.step !== 'undefined') {
        if ( currentTour.hasAlcohol ) {
            $(".age-control-checkbox").prop('checked', true).change();
        }
    }

    $(".steps-sidebar .sidebar-point").click(function() {
        if ( $(this).hasClass('active') || $(this).hasClass('done') ) {
            $("html, body").animate({
                scrollTop: $('.step-wrapper').eq( $(this).index() ) ? ($('.step-wrapper').eq( $(this).index() ).offset().top - 100) : 0
            }, 500);
        }

    });

    $('[data-step-type="equipment"] .category-box').click(function() {
        const $eqNote = $(this).closest('[data-step-type="equipment"]').find('.equipment-note');
        if ( $(this).attr('data-value') === '3' ) {
            $eqNote.removeClass('hidden');
        } else {
            $eqNote.addClass('hidden');
        }
    });
});