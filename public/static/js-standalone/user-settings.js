$(function() {
	$(".photo-wrapper").click((e) => {
		$('[name="user-photo-input"]').click();
	});

	$('[name="user-photo-input"]').change(function() {
        const input = this.files;

        if (input && input[0]) {

	    	if ( input[0].size / 1024 / 1024 > 5 ) {
	    		showNotification("Please, upload photos not larger than 5 Mb", "Error", "fail")
	    		return;
	    	}

	    	if ( !['png', 'jpg', 'jpeg'].includes(input[0].type.split("/")[1]) ) {
	    		showNotification("Please, upload photos only of .png / .jpeg / .jpg type", "Error", "fail")
	    		return;
	    	}


	    	const output = $(this).closest(".photo-settings").find('img');

	    	$(this).closest(".photo-settings").find('.photo-wrapper').removeClass('no-photo');

	        var reader = new FileReader();

	        reader.onload = function(e) {
	            $(output).attr('src', e.target.result);
	        }

	        _saveUserPhoto( input[0] );

	        reader.readAsDataURL(input[0]);


	    }
    });

    const _saveUserPhoto = ( photo ) => {
    	const ajaxData = new FormData();
        
	    ajaxData.delete("avatar");
	    ajaxData.append("avatar", photo);
	    
	    $.ajax({
	      method: 'POST',
	      url: saveUserPhotoApiRoute,
	      data: ajaxData,
	      dataType: 'json',
	      cache: false,
	      contentType: false,
	      processData: false,
	    });
    }

	function capitalizeFirstLetter(string) {
	    return string.charAt(0).toUpperCase() + string.slice(1);
	}

	$settingsFormWrapper = $('.settings-form-wrapper');
	if ( $settingsFormWrapper && $settingsFormWrapper.length > 0 ) {
		$settingsFormWrapper.find('input[type="radio"]').change(function () {
			$.post(
                saveUserSettingsApiRoute,
                {
                	'_gender': $(this).val(),
                },
                () => {
                	showNotification(`You changed your gender to ${ $(this).val() }`, 'Nice', 'success');
                }
            );
		});


		$settingsFormWrapper.find('.row .row-control .simple-link, .row .hide-not-active').click(function() {
			$(this).closest('.row').toggleClass('active');
		});

		$settingsFormWrapper.find('.row .submit-step').click(function () {
			// $(this).closest(".input-field")
			if ( $(this).hasClass('loading') ) {
				return;
			}

			const stepDataControls = dataSumbit.getStepFormData( $(this).closest('.row') );
			let stepData = dataSumbit.validateAndGetStepData(stepDataControls);

			console.log( stepData );

			if ( stepData.filter(step => step.name === "_password" ).length === 2 ) {
				const passwordFields = stepData.filter(step => step.name === "_password" );
				if ( passwordFields[0].value !== passwordFields[1].value ) {
					showNotification(`Repeate password and New password do not match, please check it and try again`, 'Error', 'fail');
					return;
				}
			}

			const invalidFields = stepData.filter(data => data.isValid === false);
			if ( invalidFields.length > 0 ) {
				invalidFields.filter((thing, index, self) =>
				  index === self.findIndex((t) => (
				    t.name === thing.name
				  ))
				).forEach((field) => {
					showNotification(`${capitalizeFirstLetter(field.viewName)} is invalid`, 'Error', 'fail');
				});
			} else {
				$(this).addClass('loading');

				const apiData = {};

				stepData.forEach((field) => {
					apiData[field.name] = field.value;
				});

				if ( apiData["bithday-day"] && apiData["bithday-month"] && apiData["bithday-year"] ) {
					apiData['_birthday'] = apiData["bithday-day"] + '/' + apiData["bithday-month"] + '/' + apiData["bithday-year"];
				}

				$.post(
	                // url: `/api/user/settings/`, 
	                saveUserSettingsApiRoute,
	                apiData,
	                (response) => {
	                	$(this).removeClass('loading');
	                    if (response.success) {
	                    	$(this).closest('.row').toggleClass('active');
	                    	if ( apiData["bithday-day"] && apiData["bithday-month"] && apiData["bithday-year"] ) {
								apiData['_birthday'] = apiData["bithday-day"] + '/' + apiData["bithday-month"] + '/' + apiData["bithday-year"];
								stepData = [{
									viewName: 'Birthday',
									value: apiData["bithday-day"] + '/' + apiData["bithday-month"] + '/' + apiData["bithday-year"],
									name: 'bithday-day',
								}];
							}

				
							if ( apiData["_old_password"] && apiData["_password"] ) {
								stepData = [{
									viewName: 'Password',
									value: apiData["_password"].replace(/[^*]/g, '*'),
									name: '_password',
								}];
							}


	                        stepData.forEach((field) => {
	                        	showNotification(`You changed your ${field.viewName} to ${field.value}`, 'Nice', 'success');

	                        	$(`[data-control-name="${field.name}"]`).val('');

	                        	$(`[data-control-name="${field.name}"]`)
	                        		.closest('.column-value').find('.value')
	                        		.text( field.value );
	                        });
	                    } else {
	                    	if ( apiData['_old_password'] ) {
	                    		showNotification(`Old password is not correct. Please try again`, 'Error', 'fail');
	                    	}
	                    }
	                }
	            );
			}
		});
	}
});
