const dataSumbit = (function () {
  let activeStepOnly = true;
  let querySelector = '.step-wrapper';
  let activeElementSelector = '.active';
  let submitDataInitControl = '.submit-step';
  let successSubmitCallback = null;
  let dataSendAdditionalParams = null;
  let dataSumbitUrl = null;
  
  const sendPhoto = (photo) => {
    let photoType = null;

    switch ( !!photo.name.match(/[^0-9]+/) ? photo.name.match(/[^0-9]+/)[0] : photo.name) {
      case 'cover-photo':
        photoType = 'coverPhoto';
        break;
      
      case 'verification-photo':
        photoType = 'verificationPhoto';
        break;
      
      default:
        photoType = 'galleryPhoto';
        break;
    }
    
    const ajaxData = new FormData();
    
    ajaxData.delete(photoType);
    ajaxData.append(photoType, photo.value);
    
    ajaxData.delete("i");
    ajaxData.append("i", photo.name.split("-").pop());
    
    
    let additionalParams = {};
    if (dataSendAdditionalParams) {
      additionalParams = dataSendAdditionalParams();
      
      Object.keys(additionalParams).forEach((key) => {
        ajaxData.delete(key);
        ajaxData.append(key, additionalParams[key]);
      });
    }
    
    if (photo.type) {
      ajaxData.delete("type");
      ajaxData.append("type", photo.type);
    }
    
    
    $.ajax({
      method: 'POST',
      url: `/api/tour/${photoType.replace("Photo", "")}_photo_upload/`,
      data: ajaxData,
      dataType: 'json',
      cache: false,
      contentType: false,
      processData: false,
      async: false,
    });
  }
  
  const submitDataAllSteps = (data, btn) => {
    if ($(btn).hasClass("loading")) {
      return;
    }
    
    $(btn).addClass("loading");
    
    let currentStep = $(btn).closest(querySelector).data('step');
    let additionalParams = {};
    if (dataSendAdditionalParams) {
      additionalParams = dataSendAdditionalParams();
    }
    
    const photoToSend = [];
    let apiData = {};
    data.forEach((dataParam) => {
      if (dataParam.isValid && dataParam.value) {
        if (typeof dataParam.value.name === "string") {
          photoToSend.push({
            name: dataParam.name,
            value: dataParam.value,
          });
        } else {
          apiData[dataParam.name] = dataParam.value;
        }
      }
    });
    
    photoToSend.forEach(sendPhoto);
    
    $.post(dataSumbitUrl, {
      ...apiData,
      ...additionalParams,
      step: currentStep,
    })
      .done(function (response) {
        $(btn).removeClass("loading");
        
        if (response.success) {
          successSubmitCallback();
        }
      });
      
    
    // const steps = getSteps();
    //
    // console.log(steps)
    //
    // setTimeout(() => {
    //     photoToSend.forEach(sendPhoto);
    //
    //     steps.forEach((step, stepIndex) => {
    //         $.post(
    //             dataSumbitUrl,
    //             {
    //                 ...apiData,
    //                 ...additionalParams,
    //                 step: $(step).index(),
    //             },
    //             (response) => {
    //                 $(btn).removeClass("loading");
    //
    //                 if (response.success && stepIndex === (steps.length - 1)) {
    //                     successSubmitCallback();
    //                 }
    //             }
    //         );
    //     });
    // }, 1);
  };
  
  const submitData = (data, btn) => {
    if ($(btn).hasClass("loading")) {
      return;
    }
    
    
    $(btn).addClass("loading");
    
    let additionalParams = {};
    if (dataSendAdditionalParams) {
      additionalParams = dataSendAdditionalParams();
    }
    
    const photoToSend = [];
    let apiData = {};
    data.forEach((dataParam) => {
      if (dataParam.isValid && dataParam.value) {
        if (typeof dataParam.value.name === "string") {
          photoToSend.push({
            name: dataParam.name,
            value: dataParam.value,
          });
        } else {
          apiData[dataParam.name] = dataParam.value;
        }
      }
    });
    
    setTimeout(() => {
      photoToSend.forEach(sendPhoto);
      
      $.post(
        dataSumbitUrl,
        {
          ...apiData,
          ...additionalParams,
        },
        (response) => {
          $(btn).removeClass("loading");
          
          if (response.success) {
            successSubmitCallback();
          }
        }
      );
    }, 1);
  };
  
  const validateStepDataControl = (stepDataControl) => {
    const controlType = $(stepDataControl).attr("data-control-type");
    const controlName = $(stepDataControl).attr("data-control-name");
    const controlViewName = $(stepDataControl).attr("data-view-name");
    
    const stepControlData = {
      name: controlName,
      isValid: true,
      value: null,
      error: null,
    }
    
    if (controlViewName) {
      stepControlData.viewName = controlViewName;
    }
    
    let value = null;
    let isRequired = null;
    
    switch (controlType) {
      case 'text':
        let maxLength = $(stepDataControl).attr("data-control-max-length");
        let minLength = $(stepDataControl).attr("data-control-min-length");
        let dataControlPattern = $(stepDataControl).attr("data-control-pattern");
        
        maxLength ? maxLength = +maxLength : null;
        minLength ? minLength = +minLength : null;
        
        value = $(stepDataControl).val().trim();
        
        if (value.length <= 0 && minLength !== 0) {
          stepControlData.isValid = false;
          stepControlData.error = $(stepDataControl).attr("data-no-value-msg") || 'Please, fill this field';
        }
        
        if (stepControlData.isValid && maxLength > 0 && value.length > maxLength) {
          stepControlData.isValid = false;
          stepControlData.error = `This field should be less than ${maxLength} symbols`;
        }
        
        if (stepControlData.isValid && minLength && value.length < minLength) {
          stepControlData.isValid = false;
          stepControlData.error = `This field should be bigger than ${minLength} symbols`;
        }
        
        if (stepControlData.isValid && value.length > 0 && dataControlPattern && value.match(new RegExp(dataControlPattern)) === null) {
          stepControlData.isValid = false;
          stepControlData.error = $(stepDataControl).attr("data-control-pattern-msg") || `Provided value does not match required pattern`;
        }
        
        const textControlType = $(stepDataControl).attr("type");
        
        switch (textControlType) {
          case 'number':
            const minValue = $(stepDataControl).attr("min") !== undefined ? +$(stepDataControl).attr("min") : null;
            const maxValue = $(stepDataControl).attr("max") !== undefined ? +$(stepDataControl).attr("max") : null;
            
            const valueInt = +value;
            
            if (stepControlData.isValid && value.length > 0 && valueInt < minValue) {
              stepControlData.isValid = false;
              stepControlData.error = `Please provide value bigger then ${minValue}`;
            }
            
            if (stepControlData.isValid && value.length > 0 && valueInt > maxValue) {
              stepControlData.isValid = false;
              stepControlData.error = `Please provide value smaller then ${maxValue}`;
            }
            
            break;
        }
        
        break;
      
      case 'options-select-box-input':
        let maxSymbolsLength = $(stepDataControl).attr("data-control-max-length");
        
        maxSymbolsLength ? maxSymbolsLength = +maxSymbolsLength : null;
        
        value = $(stepDataControl).val().trim();
        
        const isOthers = $(stepDataControl).closest(".category-input-wrapper").hasClass('show');
        
        if (value.length <= 0) {
          stepControlData.isValid = false;
          
          const msg = isOthers ? $(stepDataControl).attr("data-no-value-other") : $(stepDataControl).attr("data-no-value-msg")
          stepControlData.error = msg || 'Please, fill this field';
        }
        
        if (stepControlData.isValid && maxSymbolsLength > 0 && value.length > maxSymbolsLength) {
          stepControlData.isValid = false;
          stepControlData.error = `This field should be less than ${maxSymbolsLength} symbols`;
        }
        
        console.log(value, stepControlData.isValid, maxSymbolsLength > 0, value.length > maxSymbolsLength);
        break;
      
      
      case 'quill-editor':
        const quillEditor = $(stepDataControl).data('quill');
        value = quillEditor.root.innerHTML;
        
        const textContent = quillEditor.getText().trim();
        
        if (textContent.length === 0) {
          stepControlData.isValid = false;
          stepControlData.error = `Please, fill this field`;
        }
        
        if (stepControlData.isValid) {
          const symbolsCount = textContent.length;
          
          if (symbolsCount < 100) {
            stepControlData.isValid = false;
            stepControlData.error = $(stepDataControl).attr("data-too-small-msg") || `This field should contain at least 100 symbols`;
          } else {
            let maxSymbolsCount = $(stepDataControl).attr("data-max-symbols");
            maxSymbolsCount ? maxSymbolsCount = +maxSymbolsCount : null;
            if (symbolsCount > maxSymbolsCount) {
              stepControlData.isValid = false;
              stepControlData.error = `This field should contain less then ${maxSymbolsCount} symbols`;
            }
          }
        }
        
        break;
      
      case 'image':
        value = $(stepDataControl).closest(".photo-upload-box").data("file");
        
        isRequired = $(stepDataControl).attr("data-control-required") === "true";
        
        if (isRequired && !value && !$(stepDataControl).closest(".photo-upload-box").find(".result-img").attr("src")) {
          stepControlData.isValid = false;
          stepControlData.error = $(stepDataControl).attr("data-no-value-msg") || 'Please, fill this field';
        }
        
        let type = $(stepDataControl).attr("data-type");
        
        if (type) {
          stepControlData.type = type;
        }
        
        break;
      
      case 'select':
        const $selectedOption = $(stepDataControl).find("option:selected");
        if ($selectedOption && $selectedOption.length > 0) {
          value = $selectedOption.attr("value");
        }
        
        isRequired = $(stepDataControl).attr("data-control-required") === "true";
        if (isRequired && !value) {
          stepControlData.isValid = false;
          stepControlData.error = $(stepDataControl).attr("data-no-value-msg") || 'Please, fill this field';
        }
        
        break;
      
      case 'radio':
        const $checkedInput = $(stepDataControl).find("input:checked");
        
        if ($checkedInput && $checkedInput.length > 0) {
          value = $checkedInput.attr("value");
        }
        
        isRequired = $(stepDataControl).attr("data-control-required") === "true";
        if (isRequired && !value) {
          stepControlData.isValid = false;
          stepControlData.error = `Please, fill this field`;
        }
        
        break;
      
      case 'textarea':
        let textareaMaxSymbolsCount = $(stepDataControl).attr("data-control-max-symbols");
        textareaMaxSymbolsCount ? textareaMaxSymbolsCount = +textareaMaxSymbolsCount : null;
        
        let textareaMinSymbolsCount = $(stepDataControl).attr("data-control-min-length");
        textareaMinSymbolsCount ? textareaMinSymbolsCount = +textareaMinSymbolsCount : null;
        
        value = $(stepDataControl).val().trim();
        
        if (textareaMinSymbolsCount && value.length < textareaMinSymbolsCount) {
          stepControlData.isValid = false;
          stepControlData.error = `This field should contain at least ${textareaMinSymbolsCount} symbols`;
        }
        
        if (stepControlData.isValid && textareaMaxSymbolsCount) {
          const symbolsCount = value.length;
          if (symbolsCount > textareaMaxSymbolsCount) {
            stepControlData.isValid = false;
            stepControlData.error = `This should contain less than ${textareaMaxSymbolsCount} symbols.`;
          }
        }
        
        break;
      
      case 'checkbox':
        isRequired = $(stepDataControl).attr("data-control-required") === "true";
        value = $(stepDataControl).is(":checked");
        
        if (!value && isRequired) {
          stepControlData.isValid = false;
          stepControlData.error = `Please, fill this field`;
        }
        
        break;
      
      default:
        break;
    }
    
    if (stepControlData.isValid) {
      stepControlData.value = value;
    }
    
    return stepControlData;
  }
  
  $(".general-input-wrapper input, .general-input-wrapper textarea, .general-input-wrapper checkbox, .general-input-wrapper checkbox, .general-input-wrapper select").on("change input", (e) => {
    if ($(e.target).closest(".general-input-wrapper").hasClass("has-error")) {
      showAllStepsControlErrors(
        [validateStepDataControl($(e.target).closest(".general-input-wrapper").find(".step-data-control").get())],
        false
      );
    }
  });
  
  const validateAndGetStepData = (stepDataControls) => {
    return stepDataControls.map(validateStepDataControl);
  }
  
  const resetActiveStepControlErrors = () => {
    getActiveStep().find(".general-input-wrapper").removeClass("has-error");
  };
  
  const resetAllStepsControlErrors = () => {
    const steps = getSteps();
    steps.forEach((step) => {
      $(step).find(".general-input-wrapper").removeClass("has-error");
    });
  };
  
  const showActiveStepControlErrors = (stepData, scrollToError = true) => {
    const $activeStep = getActiveStep();
    
    stepData
      .filter(stepControlData => stepControlData.isValid === false)
      .forEach(stepControlData => {
        $control = $activeStep.find(`[data-control-name="${stepControlData.name}"]`);
        $control
          .closest(".general-input-wrapper").addClass("has-error")
          .find(".validation-message").text(stepControlData.error);
      });
    
    stepData
      .filter(stepControlData => stepControlData.isValid === true)
      .forEach(stepControlData => {
        $control = $activeStep.find(`[data-control-name="${stepControlData.name}"]`);
        $control
          .closest(".general-input-wrapper").removeClass("has-error")
          .find(".validation-message").text('');
      });
    
    if (scrollToError) {
      try {
        $("html, body").animate({
          scrollTop: $('.general-input-wrapper.has-error').eq(0).offset().top - 100,
        }, 500);
      } catch (e) {
      }
    }
  };
  
  const showAllStepsControlErrors = (stepData, scrollToError = true) => {
    stepData
      .filter(stepControlData => stepControlData.isValid === false)
      .forEach(stepControlData => {
        $control = $(`[data-control-name="${stepControlData.name}"]`);
        $control
          .closest(".general-input-wrapper").addClass("has-error")
          .find(".validation-message").text(stepControlData.error);
      });
    
    stepData
      .filter(stepControlData => stepControlData.isValid === true)
      .forEach(stepControlData => {
        $control = $(`[data-control-name="${stepControlData.name}"]`);
        $control
          .closest(".general-input-wrapper").removeClass("has-error")
          .find(".validation-message").text('');
      });
    
    if (scrollToError) {
      try {
        $("html, body").animate({
          scrollTop: $('.general-input-wrapper.has-error').eq(0).offset().top - 100,
        }, 500);
      } catch (e) {
      }
    }
  };
  
  const getStepFormData = ($step) => {
    return $step.find(".step-data-control").toArray();
  };
  
  const getActiveStep = () => $(`${querySelector}${activeElementSelector}`);
  const getActiveStepFormData = () => getStepFormData(getActiveStep()) || null;
  
  const getActiveAndPrevStepFormData = () => {
    const res = [];
    
    const $activeStep = getActiveStep();
    
    $activeStep
      .prevAll().toArray()
      .filter(e => $(e).hasClass("step-wrapper"))
      .map(step => getStepFormData($(step)))
      .forEach(formDataArray => {
        res.push(...formDataArray || [])
      });
    
    res.push(...getStepFormData(getActiveStep()) || []);
    
    return res;
  };
  
  const getSteps = () => {
    const steps = $(`${querySelector}${activeElementSelector}`).toArray();
    steps.push(...$(`${querySelector}${activeElementSelector}`).prevAll().toArray().filter(e => $(e).is(querySelector)));
    
    return steps;
  };
  
  const getAllStepsFormData = () => {
    const steps = getSteps();
    
    let formData = [];
    steps.forEach((step) => {
      formData.push(...getStepFormData($(step)));
    });
    
    return formData;
  };
  
  document.addEventListener("revalidate_step", () => {
    const stepDataControls = getActiveAndPrevStepFormData().filter(e => $(e).closest(".general-input-wrapper").hasClass("has-error"));
    if (stepDataControls) {
      const stepData = validateAndGetStepData(stepDataControls);
      showAllStepsControlErrors(stepData, false);
    }
  });
  
  const handleSubmitDataClick = (btn) => {
    const stepDataControls = activeStepOnly ? getActiveStepFormData() : getAllStepsFormData();
    
    if (stepDataControls) {
      const stepData = validateAndGetStepData(stepDataControls);
      
      activeStepOnly ? resetActiveStepControlErrors() : resetAllStepsControlErrors();
      if (stepData.filter(stepControlData => stepControlData.isValid === false).length === 0) {
        activeStepOnly ? submitData(stepData, btn) : submitDataAllSteps(stepData, btn);
      } else {
        activeStepOnly ? showActiveStepControlErrors(stepData) : showAllStepsControlErrors(stepData);
      }
    } else {
      // @TODO: add handler for not valid data step data controls
    }
  }
  
  function init(options) {
    if (options) {
      
      console.log(options);
      
      if (options.activeStepOnly !== undefined) {
        activeStepOnly = options.activeStepOnly;
      }
      
      if (options.querySelector) {
        querySelector = options.querySelector;
      }
      
      if (options.activeElementSelector) {
        activeElementSelector = options.activeElementSelector;
      }
      
      if (options.submitDataInitControl) {
        submitDataInitControl = options.submitDataInitControl;
      }
      
      if (options.successSubmitCallback) {
        successSubmitCallback = options.successSubmitCallback;
      }
      
      if (options.dataSendAdditionalParams) {
        dataSendAdditionalParams = options.dataSendAdditionalParams;
      }
      
      if (options.photoSendAdditionalParams) {
        photoSendAdditionalParams = options.photoSendAdditionalParams;
      }
      
      if (options.dataSumbitUrl) {
        dataSumbitUrl = options.dataSumbitUrl;
      }
    }
    
    if (!options.preventDefaultDataSubmit) {
      $(querySelector + ' ' + submitDataInitControl).click(function () {
        handleSubmitDataClick(this);
      });
    }
  }
  
  return {
    sendPhoto,
    submitData,
    validateAndGetStepData,
    resetActiveStepControlErrors,
    showActiveStepControlErrors,
    getStepFormData,
    getActiveStep,
    getActiveStepFormData,
    handleSubmitDataClick,
    
    init,
  };
}());
