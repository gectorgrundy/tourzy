const paginationData = {
  page: 1,
  length: 9,
};
let maxPages = null;

const pagniationStep = 9;

let isLoadingSearchTours = false;

const renderTours = (tours, append = false) => {
  if (tours) {
    const $toursContainer = $(".tours-container");
    const oldHtml = $toursContainer.html();

    if (tours.length === 0 && !append) {
      $toursContainer.html('');
      $(".empty-search-result").removeClass('hidden');
      $(".empty-search-result").closest(".dashboard-container").addClass('show-empty-search-result');
    } else {
      $(".empty-search-result").addClass('hidden');
      $(".empty-search-result").closest(".dashboard-container").removeClass('show-empty-search-result');
      $toursContainer.html(`
				${append ? oldHtml : ''}
				${
        tours.map((tour) => {

          var rating = '';
          [1, 2, 3, 4, 5].forEach(i => {
            rating += `<img class="${tour[0].rating >= i ? '' : 'inactive'}" src="/static/img/tour-details/icons/icon-star.svg">`
          })

          return `
							<div class="tour-box">
								<a href="${pageUrls.detailTourPageUrl.replace('__ID__', tour[0].id)}" class="block-link"></a>
								<div class="img-wrapper">
									<div class="tag">${tour[0].customCategoryTitle ? tour[0].customCategoryTitle : tour.category_title}</div>
									<img src="${tour.cover_photo_src}" alt="">
								</div>

								<div class="text-wrapper">
									<p class="title">${tour[0].name || ''}</p>

									<div class="show-sm">
										<div class="reviews-mob-view fw ai-center jc-space-between">
											<div class="reviews-count">
												<img src="/static/img/tour-details/icons/icon-star.svg" alt="">
												<span class="color-green">${ tour[0].rating } (${tour.review_count || 0}<span class="reviews-none"> reviews</span>)</span>
											</div>

											<span class="time">
												${tour[0].duration} hours
											</span>
										</div>
									</div>

									<div class="reviews-total-info small hide-sm">
										<div class="stars-wrapper">
										   ${rating}
										</div>

										<span class="color-grey">
											(${tour.review_count || 0} reviews)
										</span>
									</div>

									<p class="country">${tour[0].cityLocation}</p>

									<div class="fw jc-space-between ai-center">
										<p class="price">
											<span class="dollar-sign">$</span><span class="number">${ formatAmount(tour[0].price) }</span>
											per person
										</p>

										<p class="time hide-sm">
											${tour[0].duration} hours
										</p>
									</div>
								</div>
							</div>
						`;
        }).join("")
      }
			`);
    }

    _setToursVars();
    _hideLoader();
  }
};

const _showLoader = () => {
  isLoadingSearchTours = true;
  $(".ball-loader-wrapper").addClass('active');
};

const _hideLoader = () => {
  setTimeout(() => {
    isLoadingSearchTours = false;
  }, 1000);
  $(".ball-loader-wrapper").removeClass('active');
};

const _incrementAndGetPaginationData = () => {
  paginationData.page++;
  // paginationData.length += pagniationStep;

  return paginationData;
};

const getNewPosts = (isNewPage = false) => {
  _showLoader();

  let apiData = {};

  console.log(searchTourFilters);

  Object.keys(searchTourFilters).forEach((key) => {
    let value = null;

    if (!searchTourFilters[key]) {
      return;
    }

    if (Array.isArray(searchTourFilters[key]) && searchTourFilters[key].length > 1) {
      value = searchTourFilters[key].map((e) => e.value);
    } else {
      value = searchTourFilters[key][0].value;
    }

    if (value !== null) {
      apiData[key] = value;
    }
  });

  if ($('input[name="search-tour"]').val().length > 0) {
    apiData.search = $('input[name="search-tour"]').val();
  }


  if (apiData.dates) {
    apiData.dateFrom = apiData.dates[0];
    apiData.dateTo = apiData.dates[1];

    delete apiData['dates'];
  }

  if (apiData.price) {
    apiData.priceFrom = apiData.price.from;
    apiData.priceTo = apiData.price.to;

    delete apiData['price'];
  } else {
    const mobFilterPriceFrom = $('.mobile-filter-wrapper input[name="price-from"]').val();
    const mobFilterPriceTo = $('.mobile-filter-wrapper input[name="price-to"]').val();

    if (mobFilterPriceFrom !== '' && !Number.isNaN(+mobFilterPriceFrom)) {
      apiData.priceFrom = +mobFilterPriceFrom;
    }

    if (mobFilterPriceTo !== '' && !Number.isNaN(+mobFilterPriceTo)) {
      apiData.priceTo = +mobFilterPriceTo;
    }
  }

  if (isNewPage) {
    apiData = {
      ...apiData,
      ..._incrementAndGetPaginationData(),
    }
  }

  $.get(
    apiUrls.searchTour,
    apiData,
    (response) => {
      let tours = null;

      maxPages = response.data.count_pages;

      try {
        tours = response.data.data;
      } catch {

      }

      if (tours) {
        renderTours(tours, isNewPage);
      } else {
        _hideLoader();
      }
    }
  );
}

let toursContainerHeight = $(".tours-container").outerHeight();
let toursContainerOffsetTop = $(".tours-container").offset().top;

const _setToursVars = () => {
  toursContainerHeight = $(".tours-container").outerHeight();
  toursContainerOffsetTop = $(".tours-container").offset().top;
}


let lastScroll = null;

$(window).scroll(function() {
	const scrolledTop = $(window).scrollTop();
  if( !isLoadingSearchTours && (lastScroll !== null && scrolledTop > lastScroll) && (scrolledTop + $(window).height()) > toursContainerOffsetTop + (toursContainerHeight / 100 * 75) ) {
  	if ( maxPages === null || paginationData.page < maxPages ) {
  		if ( $(".empty-search-result").hasClass('hidden') ) {
  			getNewPosts(true);
  		}
  	}
  }
  lastScroll = scrolledTop;
});


$(".filter-toggler").click(() => {
  $(".mobile-filter-wrapper").toggleClass('active');
});

$(".btn-reset-filters").click(() => {
  $(".btn-reset-filter").toArray().forEach((resetBtn) => {
    const $filterItem = $(resetBtn).closest('.filter-item');
    resetFilterValue($filterItem);

    const filterType = $filterItem.attr('data-type');
    searchTourFilters[filterType] = null;

    const placeholder = $filterItem.attr('data-placeholder');
    $filterItem.find('.selected-item-wrapper').text(placeholder);

    $filterItem.removeClass('active');

  });

  $('.mobile-filter-wrapper input[name="price-from"]').val('');
  $('.mobile-filter-wrapper input[name="price-to"]').val('');

  getNewPosts();
});

$(".btn-save-filters").click(() => {
  getNewPosts();
});


$(function() {
  $(".search-form-row .clear-icon").click((e) => {
    $(e.target).closest(".search-form-row").find('input').val('');
  });
});
