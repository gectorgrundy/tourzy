const formatDate = (date) => {
  var year = date.getFullYear();
  var month = date.getMonth() + 1;
  var day = date.getDate();
  var hours = date.getHours();
  var minutes = date.getMinutes();
  var seconds = date.getSeconds();
  
  return year + "-" + month + "-" + day + " " + hours + ":" + minutes + ":" + seconds;
}

$(function () {
  const bookParams = {
    date: null,
    guestCount: null,
  };
  let bookingId = null
  
  const updateBookingPriceSummary = () => {
    if (bookParams.guestCount !== null && tour.price) {
      const $bookSummaryBlock = $('.book-summary-block');
      
      const discount = bookParams.discountAmount || 0;
      
      $bookSummaryBlock.find('.price').text(`$${formatAmount(tour.price)}`);
      $bookSummaryBlock.find('.guests-count').text(`${bookParams.guestCount} guest${bookParams.guestCount > 1 ? 's' : ''}`);
      $bookSummaryBlock.find('.total-price-general').text(`$${formatAmount(bookParams.guestCount * tour.price)}`);
      $bookSummaryBlock.find('.total-tax').text(`$${bookParams.guestCount * 7}`);
      $bookSummaryBlock.find('.total-price').text(`$${formatAmount(bookParams.guestCount * (7 + tour.price) - discount)}`);
      
      if (discount !== 0) {
        $bookSummaryBlock
          .find('.discount-row').removeClass('hidden')
          .find('.total-discount').text(`$${discount}`);
      } else {
        $bookSummaryBlock
          .find('.discount-row').addClass('hidden');
      }
    }
  };
  
  document.addEventListener('datepicker_data_select', (e) => {
    if (e.detail.type === 'book') {
      bookParams.date = e.detail.date;
      $(".choosed-date").text(bookParams.date.toString().split(" ").slice(1, 4).join(" "));
      renderAvailableSlots();
    }
  });
  
  $('select[name="numberOfGuests"]').change((e) => {
    const choosedOption = $(this).find('option:selected').val();
    bookParams.guestCount = choosedOption;
    
    updateBookingPriceSummary();
    renderAvailableSlots();
  });
  
  const renderSlots = (slots) => {
    const $slotContainer = $('.time-input-wrapper');
    $slotContainer.empty();
    
    slots.forEach((slot) => {
      $slotContainer.append(`
				<div class="time-option ${slot.isDisabled ? 'disabled' : ''}" data-time-option="${slot.time}">
	        		<p class="time">${slot.time}</p>
	        		<p class="slots"><span class="available">${slot.available}</span> slot${slot.available > 1 ? 's' : ''} left</p>
	        	</div>
			`);
    });
    
    $slotContainer.find('.time-option').click(function () {

      let available = parseInt($(this).find('.available').text())
      let numberOfGuests = $("select[name='numberOfGuests']");

      numberOfGuests.find('option').each(function () {
        $(this).val() > available ? $(this).attr("disabled", "disabled") : $(this).removeAttr('disabled')
      })

      if (!numberOfGuests.val()) {
        numberOfGuests.val(available).change()

        return $slotContainer.find(".time-option[data-time-option='" + $(this).find('.time').text() +"']").click()
      }

      $(this)
        .addClass('active')
        .siblings().removeClass('active');
      
      bookParams.time = $(this).find('.time').text();
      
      $('.book-summary-block').addClass('active');


    });
    
    $slotContainer.closest('.time-input').addClass('active');
  };
  const renderAvailableSlots = () => {
    if (bookParams.date !== null && bookParams.guestCount !== null) {
      if (typeof sheduledDates !== 'undefined') {
        let sheduledDate = sheduledDates.find(sheduledDate => sheduledDate.date.getTime() === bookParams.date.getTime());
        
        sheduledDate = sheduledDate.scheduledTimeSlots || [];
        sheduledDate.sort((a, b) => a.time - b.time);

        let slotsData = sheduledDate.map(sheduledTimeSlot => ({
          time: ('0' + sheduledTimeSlot.time).slice(-2) + ':00',
          available: sheduledTimeSlot.availableSlots > 0 ? sheduledTimeSlot.availableSlots : 0,
        }));
        
        slotsData = slotsData.map((slot) => {
          return {
            ...slot,
            isDisabled: !slot.available,
          }
        });
        
        // console.log(slotsData);
        
        renderSlots(slotsData);
      }
    }
  };
  
  $('.submit-book-request').click(function () {
    if ( $('[data-target="login"]').length > 0 ) {
      $.magnificPopup.close();
      $('[data-target="login"]').click();
    }

    if (USER_AGE !== -1 && USER_AGE < MIN_AGE) {
      showNotification(`Sorry, your age does not meet the minimum age requirements of the tour`, 'Error', 'fail');
      return;
    }
    
    if ($(this).hasClass('loading')) {
      return;
    }
    
    if (bookParams.date) {
      const dateArray = (bookParams.date).toString().split(" ");
      $(".pay-summary-block .date.display-inline-block").text(`${dateArray[2]} ${dateArray[1]} ${dateArray[3]} ${bookParams.time}`)
    }
    
    $(this).addClass('loading');

    $.post(
      routes.bookingDataSave,
      {
        ...bookParams,
        time: bookParams.time,
        date: bookParams.date.getFullYear() + "-" + ("0" + (bookParams.date.getMonth() + 1)).slice(-2) + "-" + ("0" + bookParams.date.getDate()).slice(-2),
        id: tour.id,
      },
      (response) => {
        bookingId = response.data.booking_id
        $(this).removeClass('loading');
        
        // $.magnificPopup.close();
        $.magnificPopup.open({
          items: {
            src: '#pay'
          },
        });
      }
    );
  });
  
  
  const $payPopup = $("#pay");
  
  $payPopup.find('.option-box .option-item').click(() => {
    $payPopup.find('.submit-pay').removeClass('disabled');
    bookParams.payMethod = $payPopup.find('.option-box .option-item.active').attr('data-type');
  });
  
  $payPopup.find('.discount-wrapper input[data-control-name="hasPromocode"]').change(function () {
    const $promocodeInputWrapper = $payPopup
      .find('.discount-wrapper input[data-control-name="promocodeValue"]')
      .closest('.opacity-transition');
    if ($(this).is(':checked')) {
      $promocodeInputWrapper
        .removeClass('opacity0')
        .addClass('opacity100');
    } else {
      $promocodeInputWrapper
        .removeClass('opacity100')
        .addClass('opacity0');
    }
  });
  
  let promocodeCheckRequest = null;
  const checkPromocode = () => {
    const promocodeValue = $payPopup.find('[data-control-name="promocodeValue"]').val();
    
    if (promocodeValue && promocodeValue.trim().length > 0) {
      $payPopup.find('.submit-pay').addClass('disabled').addClass('loading');
      
      $.post(
        'https://jsonplaceholder.typicode.com/posts',
        {
          ...bookParams,
          tourId: tour.id,
          promocodeValue,
        },
        (response) => {
          response = {
            isValid: true,
            discountAmount: Math.floor(Math.random() * 100) + 1,
          }
          
          if (bookParams.payMethod) {
            $payPopup.find('.submit-pay').removeClass('disabled')
          }
          
          $payPopup.find('.submit-pay').removeClass('loading');
          
          if (response && response.isValid && response.discountAmount) {
            bookParams.discountAmount = response.discountAmount;
            bookParams.promocodeValue = promocodeValue;
            
            updateBookingPriceSummary();
          }
        }
      );
    }
  };
  
  let promocodeTypingTimer = null;
  const donePromocodeTypingInterval = 2000;
  
  $payPopup.find('[data-control-name="promocodeValue"]').on('keyup paste', () => {
    clearTimeout(promocodeTypingTimer);
    promocodeTypingTimer = setTimeout(checkPromocode, donePromocodeTypingInterval);
  });
  
  $payPopup.find('.submit-pay').click(() => {
    const serialize = (obj) => {
      var str = [];
      for (var p in obj)
        if (obj.hasOwnProperty(p)) {
          str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        }
      return str.join("&");
    };
    
    // const bookDate = bookParams.date;
    // bookDate.setHours(bookParams.time.split(":")[0]);
    // bookDate.setMinutes(bookParams.time.split(":")[1]);
    
    const payParams = {
      payMethod: bookParams.payMethod,
      bookingId: bookingId,
    }
    
    const redirectUrl = `${window.location.protocol}//${window.location.host}${routes.payment}?${serialize(payParams)}`;
    location.href = redirectUrl;
  });
});
