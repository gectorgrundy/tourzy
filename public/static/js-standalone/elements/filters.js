const searchTourFilters = [];

$('.filter-item').each((i, filterItem) => {
	const $filterItem = $(filterItem);


});

$('.filter-item .selected-item-wrapper').click(function() {
	$(this)
		.closest('.filter-item').toggleClass('active')
		.closest(".input-wrapper")
		.siblings().find(".filter-item").removeClass('active');
});

$(".filter-item").on("click", function(event){
    event.stopPropagation();
});

$("body").on("click", function(event){
    $(".filter-item").removeClass('active');
});


$(".filter-item .single-select-item").click(function() {
	$(this).addClass('active').siblings().removeClass('active');

	const filterValue = $(this).attr('data-value');

	const $filterItem = $(this).closest('.filter-item');
	const filterType = $filterItem.attr('data-type');

	if (filterType && filterValue) {
		searchTourFilters[filterType] = [{
			value: filterValue,
			text: $(this).text(),
		}];
	}

	// $filterItem.find('.selected-item-wrapper').text( searchTourFilters[filterType].map(e => e.text).join(", ") );
	// $filterItem.removeClass('active');

	// getNewPosts();
});

$(".filter-item .multiple-select-item").click(function() {
	$(this).toggleClass('active');

	const filterValue = $(this).attr('data-value');

	const $filterItem = $(this).closest('.filter-item');
	const filterType = $filterItem.attr('data-type');

	if (filterType && filterValue) {
		if ( $(this).hasClass('active') ) {
			if ( searchTourFilters[filterType] ) {
				searchTourFilters[filterType].push({
					value: filterValue,
					text: $(this).text(),
				});
			} else {
				searchTourFilters[filterType] = [{
					value: filterValue,
					text: $(this).text(),
				}];
			}
		}  else {
			searchTourFilters[filterType] = searchTourFilters[filterType].filter(e => e.value !== filterValue);
		}
	}

	console.log( searchTourFilters );
});

const getFilterValue = ($filterItem) => {
	const filtersValue = [];

	$filterItem.find('.select-item').toArray().forEach((selectItem) => {
		const $selectItem = $(selectItem);
		const selectItemType = $selectItem.attr('data-item-type');

		let value = null;
		switch (selectItemType) {
			case 'checkbox':
				const $checkbox = $selectItem.find('input[type="checkbox"]');

				if ( $checkbox.is(':checked') ) {
					value = {
						text: $selectItem.find('label > span').text(),
						value: $selectItem.attr('data-value')
					};
				}

				break;

			case 'datepicker':
				value = searchTourFilters[ $(selectItem).closest(".filter-item").attr("data-type") ];
				break;

			case 'select':
				value = $selectItem.is('.active') ? {
					value: $selectItem.attr('data-value'),
					text: $selectItem.text(),
				} : null;
				break;
		}

		if ( value !== null ) {
			filtersValue.push( value );
		}
	});

	return filtersValue;
};


const resetFilterValue = ($filterItem) => {
	$filterItem.find('.select-item').toArray().forEach((selectItem) => {
		const $selectItem = $(selectItem);
		const selectItemType = $selectItem.attr('data-item-type');

		searchTourFilters[selectItemType] = null;

		let value = null;
		switch (selectItemType) {
			case 'checkbox':
				const $checkbox = $selectItem.find('input[type="checkbox"]').prop('checked', false);
				break;

			case 'datepicker':
				$selectItem.datepicker().data('datepicker').clear();
				break;
		}
	});
};

$(".filter-item .btn-reset-filter").click(function() {
	const $filterItem = $(this).closest('.filter-item');
	const filterType = $filterItem.attr('data-type');

	resetFilterValue($filterItem);
	
	searchTourFilters[filterType] = null;

	const placeholder = $filterItem.attr('data-placeholder');
	$filterItem.find('.selected-item-wrapper').text(placeholder);

	$filterItem.removeClass('active');

	console.log( {...searchTourFilters} );

	getNewPosts();
});

function formatDate(date) {
  var monthNames = [
    "January", "February", "March",
    "April", "May", "June", "July",
    "August", "September", "October",
    "November", "December"
  ];

  var day = date.getDate();
  var monthIndex = date.getMonth();
  var year = date.getFullYear();

  return day + ' ' + monthNames[monthIndex] + ' ' + year;
}

document.addEventListener('datepicker_data_select', (e) => {
	if (e.detail.type === 'search-tour-dates' && e.detail.date && e.detail.date.length === 2) {
		searchTourFilters.dates = {
			value: e.detail.date.map((date) => {
				return date.getFullYear() + "/" + ("0"+(date.getMonth()+1)).slice(-2) + "/" + ("0" + date.getDate()).slice(-2)
			}),
			text: `${formatDate(new Date(e.detail.date[0]))} - ${formatDate(new Date(e.detail.date[1]))}`
		};
	}
});

$(".filter-item .btn-save-filter").click(function() {
	const $filterItem = $(this).closest('.filter-item');

	const filterType = $filterItem.attr('data-type');
	const filtersValue = getFilterValue($filterItem);

	if ( !filtersValue || filtersValue.length === 0 || filtersValue[0] === undefined ) {
		$filterItem.find('.btn-reset-filter').click();
	} else {
		$filterItem.find('.selected-item-wrapper').text( filtersValue.map(e => e.text).join(", ") );

		searchTourFilters[filterType] = filtersValue;

		$filterItem.removeClass('active');

		getNewPosts();
	}
});


let priceChangedTimer = null;
const priceChanged = () => {
	getNewPosts();
}

const priceRangeFrom = $("#price-range").attr('data-from');
const priceRangeTo = $("#price-range").attr('data-to');

$("#price-range").ionRangeSlider({
	type: "double",
	grid: true,
	min: priceRangeFrom || 0,
	max: priceRangeTo || 300,
	from: priceRangeFrom || 0,
	to: priceRangeTo || 300,
	prefix: "",
	onChange: (data) => {
        searchTourFilters.price = [
        	{
        		value: {
	        		from: data.from_pretty,
	        		to: data.to_pretty,
	        	}
        	}
        ];

        clearTimeout( priceChangedTimer );
        priceChangedTimer = setTimeout(priceChanged, 1500);
    }
});



let categoryToChoose = null;
if ( getParameterByName('category') ) {
	categoryToChoose = getParameterByName('category');
}
document.addEventListener('categories_created', (e) => {
	if ( categoryToChoose ) {
		$('.filter-item[data-type="category"]').find(`[data-value="${ categoryToChoose }"] input[type="checkbox"]`).prop('checked', true);
		$('.filter-item[data-type="category"]').find(`.btn-save-filter`).click();
	}
});


$(function() {
	const sortFilter$ = $(".sort-filter");

	if ( sortFilter$.length > 0 ) {
		searchTourFilters[ 'sortParam' ] = sortFilter$.attr('data-active-option');
		searchTourFilters[ 'sortBy' ] = sortFilter$.attr('data-sort-by');

		sortFilter$.find('.filter-trigger').click(function() {
			$(this).closest('.sort-filter').find('.sort-options').addClass('active');
		});

		sortFilter$.find('.sort-options li').click(function() {
			$(this).closest('.sort-options').removeClass('active');
			$(this).addClass('active').siblings().removeClass('active');

			$(this).closest('.sort-filter').attr( 'data-active-option', $(this).attr('data-value') );
			$(this).closest('.sort-filter').attr( 'data-sort-by', ($(this).attr('data-sort-by') || 'DESC') );
			$(this).closest('.sort-filter').find('.filter-trigger').text( $(this).text() );

			searchTourFilters[ 'sortParam' ] = [ { value: sortFilter$.attr('data-active-option') } ];
			searchTourFilters[ 'sortBy' ] = [{ value: sortFilter$.attr('data-sort-by') }];

			getNewPosts();
		});
	}
});
