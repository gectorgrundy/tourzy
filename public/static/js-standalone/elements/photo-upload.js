$(function() {
	function saveAndShowUploadedImg(input, output) {
	    if (input && input[0]) {

	    	if ( input[0].size / 1024 / 1024 > 5 ) {
	    		$(output).closest('.input-wrapper').addClass('has-error').find('.validation-message').text('Please, upload photos not larger than 5 Mb');
	    		return;
	    	}

	    	if ( !['png', 'jpg', 'jpeg'].includes(input[0].type.split("/")[1]) ) {
	    		$(output).closest('.input-wrapper').addClass('has-error').find('.validation-message').text('Please, upload photos only of .png / .jpeg / .jpg type');
	    		return;
	    	}

	    	$(output).closest(".photo-upload-box").data("file", input[0]);

	        var reader = new FileReader();

	        reader.onload = function(e) {
	            $(output).attr('src', e.target.result);
	            $(output).closest(".photo-upload-box").addClass("has-photo");
	        }

	        reader.readAsDataURL(input[0]);

	        document.dispatchEvent( new CustomEvent( 'revalidate_step' ) );
	    }
	}

	function restoreImage(imgPath, output) {
	    if (imgPath && imgPath.split().length > 0) {
	    	$(output).attr('src', imgPath);
	        $(output).closest(".photo-upload-box").addClass("has-photo");
	    }
	}

	$('input.photo-upload[type="file"]').each((i, el) => {
	    const isBig = $(el).hasClass('is-big');
	    const isSmallHeight = $(el).hasClass('is-small-height');
	    
	    $(el).wrap(`<div class="photo-upload-box ${ isBig ? 'big' : '' } ${ isSmallHeight ? 'is-small-height' : '' }"></div>`);
	    $(el).closest(".photo-upload-box").append(`
	        <div class="remove-photo">
	            <svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
	            <path d="M7.06127 6.00012L11.7801 1.28108C12.0733 0.988138 12.0733 0.512831 11.7801 0.219892C11.4869 -0.0732973 11.0122 -0.0732973 10.719 0.219892L6.00012 4.93894L1.28102 0.219892C0.987847 -0.0732973 0.51306 -0.0732973 0.219883 0.219892C-0.0732943 0.512831 -0.0732943 0.988138 0.219883 1.28108L4.93898 6.00012L0.219883 10.7192C-0.0732943 11.0121 -0.0732943 11.4874 0.219883 11.7804C0.366471 11.9267 0.558587 12 0.750453 12C0.942319 12 1.13444 11.9267 1.28102 11.7801L6.00012 7.06106L10.719 11.7801C10.8656 11.9267 11.0577 12 11.2495 12C11.4414 12 11.6335 11.9267 11.7801 11.7801C12.0733 11.4872 12.0733 11.0119 11.7801 10.7189L7.06127 6.00012Z" fill="#484848"/>
	            </svg>
	        </div>
	        <img class="result-img" />
	        <div class="img-wrapper"><img src="/static/img/submit-tour/upload-photo.svg"></div>
	        <div class="text-part">
	            <p>Drag file to upload or</p>
	            <button class="btn unfilled green">Choose file</button>
	        </div>
	    `);


	    if ( "uploadedImages" in window || typeof uploadedImages !== 'undefined' ) {
	    	const controlName = $(el).attr('data-control-name');
	    	const imgType = $(el).attr('data-type');

	    	const uploadedImage = uploadedImages.find((img) => {
	    		let isMatch = true;

	    		if ( img.name !== controlName ) {
	    			isMatch = false;
	    		}

	    		if ( imgType && +img.photo_type !== +imgType ) {
	    			isMatch = false;
	    		}

	    		return isMatch;
	    	});

	    	if ( uploadedImage ) {
	    		restoreImage(
			    	`/static/uploads/${uploadedImage.hash}`,
			    	$(el).closest(".photo-upload-box").find(".result-img")
			    );
	    	}
		}
	});

	$(".photo-upload-box button").click(function() {
	    $photoUploadBox = $(this).closest(".photo-upload-box");
	    $photoUploadBox.find('input').click();
	});

	$(".photo-upload-box .remove-photo").click(function() {
	    $photoUploadBox = $(this).closest(".photo-upload-box");
	    $photoUploadBox.find('input[type="file"].photo-upload').val(null);	
	    $photoUploadBox.removeClass("has-photo").find(".result-img").attr("src", "");
	});

	[ 'drag', 'dragstart', 'dragend', 'dragover', 'dragenter', 'dragleave', 'drop' ].forEach( function( event )
	{
	    document.addEventListener( event, function( e )
	    {
	        // preventing the unwanted behaviours
	        e.preventDefault();
	        e.stopPropagation();
	    });
	});

	$(".photo-upload-box").each((i, uploadBox) => {
	    [ 'dragover', 'dragenter' ].forEach( function( event )
	    {
	        uploadBox.addEventListener( event, function()
	        {
	            uploadBox.classList.add( 'is-dragover' );
	        });
	    });

	    [ 'dragleave', 'dragend', 'drop' ].forEach( function( event )
	    {
	        uploadBox.addEventListener( event, function()
	        {
	            uploadBox.classList.remove( 'is-dragover' );
	        });
	    });

	    uploadBox.addEventListener( 'drop', function( e )
	    {
	        let droppedFiles = e.dataTransfer.files; // the files that were dropped
	        saveAndShowUploadedImg(droppedFiles, $(uploadBox).find(".result-img"));
	    }); 

	    $(uploadBox).find('input[type="file"].photo-upload').change(function() {
	        saveAndShowUploadedImg(this.files, $(uploadBox).find(".result-img"));
	    });
	});
});














