$(document).on('click','a.show-more', {} ,function(e){
  e.preventDefault();

  let content = $(this).parent('.review-content');
  content.find('.less-text').hide()
  content.find('a.show-more').hide()
  content.find('.full-text').show()
  content.find('a.show-less').show()
})
$(document).on('click','a.show-less', {} ,function(e){
  e.preventDefault();
  let content = $(this).parent('.review-content');

  content.find('.less-text').show()
  content.find('a.show-more').show()
  content.find('.full-text').hide()
  content.find('a.show-less').hide()
})

$('#load-tours').on('click', function (e) {
  e.preventDefault();
  let $this = $(this);
  reviewPaginationData.page += 1

  $.get(routes.reviewList, reviewPaginationData).always(function (response) {
    reviewPaginationData.filteredCount += response.data.length

    renderReviews(response.data)
    if (reviewPaginationData.filteredCount >= reviewPaginationData.totalCount) {
      $this.hide()
    }
  })
});

function renderReviews(reviews) {
  const $reviewsContainer = $(".reviews-detail-row");
  const oldHtml = $reviewsContainer.html();
  $reviewsContainer.empty().append(`${oldHtml} ${ reviews.map((review) => renderReview(review)).join("")}`);
}

function renderReview(review) {
  var rating = '';
  [1, 2, 3, 4, 5].forEach(i => {
    rating += `<img class="${review.rating >= i ? '' : 'inactive'}" src="/static/img/tour-details/icons/icon-star.svg">`
  })
  var avatar = review.avatarSrc ? `<img src='/static/uploads/${review.avatarSrc}'>` : '<img src="/static/img/default-avatar.svg">'
  var text = review.text.length < 200 ? `<p>${review.text}</p>` : `
            <p class="less-text">${review.text.slice(0, 200)} ...</p>
            <p class="full-text" style="display: none;">${review.text}</p>
            <a class="show-more" href="#">show more</a>
            <a class="show-less" style="display: none;" href="#">show less</a>`

  var date = new Date(review.created.date).toLocaleDateString('en-GB', {
    day: 'numeric', month: 'short', year: 'numeric'
  }).split(' ').join(' ');

  return `
    <div class="review-detail">
        <div class="review-details">
            <div class="fw">
                <div class="img-wrapper">
                    ${avatar}
                </div>
                <div class="text-wrapper">
                    <div class="hero-name">${review.userFirstName} ${review.userLastName}</div>
                    <div class="hero-stars">${rating}</div>
                </div>
            </div>

            <div class="date">
                ${date}
            </div>
        </div>
        <div class="review-content">${text}</div>
    </div>`;
}