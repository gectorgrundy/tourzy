$(function() {

  $(".quill-editor").each((i, e) => {
      const maxSymbols = $(e).attr('data-max-symbols') || 450;

      var quill = new Quill('.quill-editor[data-control-name="' + $(e).attr('data-control-name') + '"]', {
        modules: {
          toolbar: [
            ['bold', 'italic', 'underline', { 'list': 'ordered'}, { 'list': 'bullet' }],
          ]
        },
        formats: [
            'bold',
            'italic',
            'underline', 
            'list',
        ],
        placeholder: $(e).attr('data-placeholder'),
        theme: 'snow'
      });

      $(e)
        .closest(".general-input-wrapper")
        .find(".ql-container")
        .after(`<div class="quill-counter"><span class="current">0</span> / <span class="max-words">${maxSymbols}</span></div>`);

      quill.on('text-change', function(e) {
        const content = quill.getText().trim();
        const symbolsCount = content.length ? content.length : 0;

        $(quill.root)
          .closest(".general-input-wrapper")
          .find(".quill-counter .current")
          .text(symbolsCount);

        const maxLength = +$(quill.root)
          .closest(".general-input-wrapper")
          .find(".quill-counter .max-words")
          .text();

        if (quill.getLength() > maxLength) {
          quill.deleteText(maxLength, quill.getLength());
        }

        document.dispatchEvent( new CustomEvent( 'revalidate_step' ) );
      });

      if (currentTour[$(e).attr('data-control-name')]) {
        quill.clipboard.dangerouslyPasteHTML( currentTour[$(e).attr('data-control-name')] );
      }

      $(e).data('quill', quill);
    });

});

