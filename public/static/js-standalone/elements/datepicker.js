const datePickerChoosedValues = [];

$(function() {
    $('.is-datepicker').each((i, datepicker) => {
        const $datepicker = $(datepicker);

        let confObject = {
            language: 'en',
            onRenderCell: function (date, cellType) {
                if (cellType == 'day') {
                    let isDisabled = false;

                    if ( typeof availableDates !== 'undefined' ) {
                        isDisabled = !availableDates.find((availableDate) => availableDate.getTime() === date.getTime());
                    }

                    let html = date.getDate();

                    if ( typeof datepickerCounts !== 'undefined' && typeof datepickerCounts[this.controlName] !== 'undefined' && datepickerCounts[this.controlName].filter(scheduledDate => scheduledDate.date.getTime() === date.getTime()).length > 0 ) {
                        html += `<span class="count">${ datepickerCounts[this.controlName].find(scheduledDate => scheduledDate.date.getTime() === date.getTime()).count }</span>`;
                    }

                    return {
                        disabled: isDisabled,
                        html,
                    }
                }
            },
            onSelect: (formattedDate, date, inst) => {
                const type = $(inst.el).attr('data-control-name');
                datePickerChoosedValues[type] = new Date(date);

                let event = new CustomEvent('datepicker_data_select', { detail: { date, type } });
                document.dispatchEvent(event);
            }
        };

        if ( $datepicker.hasClass('is-range-datepicker') ) {
            confObject.range = true;
            confObject.toggleSelected = false;
        }

        if ( $datepicker.hasClass('can-multiple-dates') ) {
            confObject.multipleDates = 999;
        }

        if ( $datepicker.hasClass('has-no-disabled-dates') ) {
            confObject.onRenderCell = function() {
                return {
                    disabled: false,
                }
            }
        }


        if ( typeof datepickerBasicConfig !== 'undefined' ) {
            confObject = {
                ...confObject,
                ...datepickerBasicConfig,
            }
        } else {
            confObject = {
                ...confObject,
                minDate: new Date(),
            }
        }

        let moveNext = false;

        if ( $datepicker.hasClass("show-avaialable-only") && $datepicker.hasClass("next-month") ) {
            if ( typeof availableDates !== 'undefined' ) {
                if ( availableDates.filter( availableDate => availableDate.getMonth() === (new Date()).getMonth() + 1 ).length > 0 ) {
                    moveNext = true;
                } else {
                    $datepicker.hide();    
                }
            } else {
                $datepicker.hide();                
            }
        }

        $datepicker.datepicker(confObject);

        if ( moveNext ) {
            $datepicker.find( '.datepicker--nav-action[data-action="next"]' ).click();  
        }
    });
});















