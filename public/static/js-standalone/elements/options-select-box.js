$(function() {
	$(".categories-wrapper .category-box").click(function() {
	    $(this).addClass('active').siblings().removeClass('active');

	    const $categoryInputWrapper = $(this).closest(".general-input-wrapper").find(".category-input-wrapper");
	    $categoryInputWrapper.find("input").val( $(this).attr("data-value") );

	    if ( $categoryInputWrapper.find("select") ) {
	    	if ( $categoryInputWrapper.find("select").find(`option[value="${$(this).attr("data-value")}"]`).length > 0 ) {
	    		$categoryInputWrapper.find("select").find(`option[value="${$(this).attr("data-value")}"]`).attr('selected', true);
	    	} else {
	    		console.log("dddd");
	    		$categoryInputWrapper.find("select").find(`option`).removeAttr('selected');
	    	}

	    	$categoryInputWrapper.find("select").trigger('change');
	    }

	    const dataType = $(this).attr("data-type");
	    if ( dataType === "other" ) {
	        $categoryInputWrapper.addClass("show");
	    } else {
	        $categoryInputWrapper.removeClass("show");
	    }

	    document.dispatchEvent( new CustomEvent( 'revalidate_step' ) );
	});
});




