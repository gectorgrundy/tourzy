$(function () {
    $('.select2').each((i, el) => {
        $(el).select2({
            minimumResultsForSearch: Infinity
        });
    });

    let lastCityPredictions = [];

    $('.selectCity').each((i, el) => {
        $.fn.select2.amd.define('select2/data/googleAutocompleteAdapter', ['select2/data/array', 'select2/utils'],
            function (ArrayAdapter, Utils) {
                function GoogleAutocompleteDataAdapter ($element, options) {
                    GoogleAutocompleteDataAdapter.__super__.constructor.call(this, $element, options);
                }

                Utils.Extend(GoogleAutocompleteDataAdapter, ArrayAdapter);

                GoogleAutocompleteDataAdapter.prototype.query = function (params, callback) {
                    var returnSuggestions = function(predictions, status)
                    {
                        var data = {results: []};
                        if (status != google.maps.places.PlacesServiceStatus.OK) {
                            callback(data);
                        }
                        if ( predictions ) {
                            lastCityPredictions = predictions;
                            for(var i = 0; i< predictions.length; i++)
                            {
                                data.results.push({id:predictions[i].description, text: predictions[i].description});
                            }
                        }
                        data.results.push({id:' ', text: 'Powered by Google', disabled: true});
                        callback(data);
                    };

                    if(params.term && params.term != '')
                    {
                        var service = new google.maps.places.AutocompleteService();

                        let countries = ['usa', 'ae'];

                        if ( $('.selectCountry').length > 0 && $('.selectCountry').val().trim().length > 0 ) {
                            countries = $('.selectCountry').val();
                        }

                        service.getPlacePredictions({
                            input: params.term,
                            types: ['(cities)'],
                            componentRestrictions: {
                                country: countries
                            },
                        }, returnSuggestions);
                    }
                    else
                    {
                        var data = {results: []};
                        data.results.push({id:' ', text: 'Powered by Google', disabled: true});
                        callback(data);
                    }
                };
                return GoogleAutocompleteDataAdapter;
            }
        );

        function formatRepo (repo) {
          if (repo.loading) {
            return repo.text;
          }

          var markup = "<div class='select2-result-repository clearfix'>" +
              "<div class='select2-result-title'>" + repo.text + "</div>";
          return markup;
        }

        function formatRepoSelection (repo) {
            return repo.text;
        }

        var googleAutocompleteAdapter = $.fn.select2.amd.require('select2/data/googleAutocompleteAdapter');
            
        $(el).select2({
            width: '100%',
            dataAdapter: googleAutocompleteAdapter,
            placeholder: 'Choose the city',
            escapeMarkup: function (markup) { return markup; },
            minimumInputLength: 2,
            templateResult: formatRepo,
            templateSelection: formatRepoSelection
        });

        $(el).change( (e) => {
            const selectedCity = lastCityPredictions.find( place => place.place_id === e.target.value );
            console.log( selectedCity );
        } );

        const controlName = $(el).attr('data-control-name');
        if (controlName && controlName.trim().length > 0) {
            let value = false;

            if ("controlsDataContainer" in window || typeof controlsDataContainer !== 'undefined') {
                value = controlsDataContainer[controlName];
            }

            if (value && value.trim().length > 0) {
                var $newOption = $("<option selected='selected'></option>").val(value).text(value)
                $(el).append($newOption).trigger('change');
            }
        }
    });


    $('.selectCountry').each((i, el) => {

        $(el).html(
            '<option></option>' +
            [
                { name: "USA", countryCode: "usa" },
                { name: "United Arab Emirates", countryCode: "ae" },
            ].map( countryData => `
                <option value="${ countryData.countryCode }">${ countryData.name }</option>
            ` ).join('')
        )

        $(el).select2({
            placeholder: 'Choose the country',
        });

        const controlName = $(el).attr('data-control-name');
        if (controlName && controlName.trim().length > 0) {
            let value = false;

            if ("controlsDataContainer" in window || typeof controlsDataContainer !== 'undefined') {
                value = controlsDataContainer[controlName];
            }

            if (value && value.trim().length > 0) {
                var $newOption = $("<option selected='selected'></option>").val(value).text(value)
                $(el).append($newOption).trigger('change');
            }
        }
    });
});
