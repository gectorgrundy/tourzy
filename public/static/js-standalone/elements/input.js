$(function() {
    $('input[type="password"]').toArray().forEach((input) => {
        $inputWrapper = $(input).closest('.general-input-wrapper');

        if ( $inputWrapper && $inputWrapper.length > 0 ) {
            $inputWrapper.addClass('has-hint');
            $inputWrapper.find('.input-hint').click(function () {
                $(this).toggleClass('active');

                if ( $(this).hasClass('active') ) {
                    $(this).closest('.general-input-wrapper').find('input').attr('type', 'text');
                } else {
                    $(this).closest('.general-input-wrapper').find('input').attr('type', 'password');
                }
            });
        }
    });

    $('input.input-control[data-control-max-length]').toArray().forEach((input) => {
        $( input ).wrap( '<div class="input-control-wrapper"></div>' );

        if ( $( input ).is('[data-control-no-counter]') ) {
            return;
        }

        $( input ).closest( '.input-control-wrapper' ).append( `
            <div class="input-counter">
                <span class="current">${ $( input ).val().length }</span>/<span class="max">${ $( input ).attr('data-control-max-length') }</span>
            </div>
        ` );

        $( input ).on('input propertychange', function() {
            const textLength = $( this ).val().length;
            $( this ).closest( '.input-control-wrapper' ).find( '.input-counter .current' ).text( textLength );
        } );
    });

    $('input[data-control-max-length]').toArray().forEach(input => {
        $(input).on('keydown', function() {
            const maxLength = +$(input).attr('data-control-max-length');
            if ( !Number.isNaN( maxLength ) && maxLength <= input.value.length ) {
                return false;
            }
        });
    })
});