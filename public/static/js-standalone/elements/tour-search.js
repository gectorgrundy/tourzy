document.addEventListener("google-maps-ready", function() {
	$('[name="search-tour"], input[data-control-name="city"]').toArray().forEach( input => {
		var autocomplete = new google.maps.places.Autocomplete(input, { types: ['(cities)'], componentRestrictions: {country: ['usa', 'ae']} });
	} )
});
