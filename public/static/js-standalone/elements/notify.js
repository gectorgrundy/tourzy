function showNotification(message, title = false, additionalClass = 'success') { // additionalClass = 'fail'
    const options = {
        text: message,
        icon: false,
        addClass: additionalClass,
        // delay: 90000,
        confirm : {
            confirm: true,
            buttons: [
                // additionalBtn,
                {
                    text: 'Dissmiss',
                    addClass: 'btn btn-link ' + additionalClass,
                    click: function (notice) {
                        notice.remove();
                    }
                },
                null
            ]
        },
        animate : {
            animate  : true,
            in_class : 'slideInDown',
            out_class: 'slideOutUp'
        },
        modules: {
            Buttons : {
                closer : true,
                closerHover: false,
                sticker: false
            },
        }

    };

    if ( title ) {
        options.title = title;
    }

    PNotify.notice( options );
}
