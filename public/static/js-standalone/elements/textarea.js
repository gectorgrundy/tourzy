; (function ($) {
    $.fn.characterCounter = function (limit) {
        return this.filter("textarea, input:text").each(function () {
            var $this = $(this),
            checkCharacters = function (event) {
                if ($this.val().length > limit) {

                    $this.val($this.val().substring(0, limit));

                    event.preventDefault();
                    event.stopPropagation();
                }

                let content = $this.val();
                let words = content === '' ? 0 : content;
                $this.closest(".textarea-wrapper").find('.word-count').text( words && words.length ? (words.length) : 0 );
            };

            $this.keyup(function (event) {

                // Keys "enumeration"
                var keys = {
                    BACKSPACE: 8,
                    TAB: 9,
                    LEFT: 37,
                    UP: 38,
                    RIGHT: 39,
                    DOWN: 40
                };

                // which normalizes keycode and charcode.
                switch (event.which) {

                    case keys.UP:
                    case keys.DOWN:
                    case keys.LEFT:
                    case keys.RIGHT:
                    case keys.TAB:
                        break;
                    default:
                        checkCharacters(event);
                        break;
                }
            });

            // Handle cut/paste.
            $this.bind("paste cut", function (event) {
                setTimeout(function () { checkCharacters(event); event = null; }, 150);
            });
        });
    };
} (jQuery));

$(function() {
	$("textarea").each((i, e) => {
        $(e).wrap('<div class="textarea-wrapper"></div>');

        const maxSymbols = $(e).attr('data-control-max-symbols');
        if (maxSymbols) {
            let content = $(e).val().trim();
            let words = content === '' ? 0 : content;

            $(e).closest(".textarea-wrapper").append(`<div class="counter"><span class="word-count">${words.length ? words.length : 0}</span> / ${maxSymbols}</div>`);
        }

        // $(e).bind('input propertychange', function () {
        // });

        const maxLength = +$(e).attr('data-control-max-symbols');
        if ( !Number.isNaN( maxLength ) ) {
            $(e).characterCounter(maxLength);
        }
    });
});