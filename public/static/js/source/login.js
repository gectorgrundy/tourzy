var validateLogin = new Validate('data-validate','#login_form');
$( "#login_form" ).submit(function( event ) {
    event.preventDefault();
    validateLogin.checkValidate();
    if (0 === Object.keys(validateLogin.errors).length) {
        $( "#login_form" ).find('.btn').addClass( 'loading' );

        var data = {};
        $(this).find('input[name]').each(function (index, element) {
            data[element.name] = element.value;
        })

        if ( $( "#login_form" ).closest(".form-cover").find(".form-warning").length > 0 ) {
            $( "#login_form" ).closest(".form-cover").find(".form-warning").removeClass("show");
        }


        $.post($(this).attr('action'), data, function (result) {
            if(result.success){
                document.location.reload();
            } else {
                if ( $( "#login_form" ).closest(".form-cover").find(".form-warning").length > 0 ) {
                    $( "#login_form" ).closest(".form-cover").find(".form-warning").addClass("show");
                } else {
                    showMessage(result.message);
                }
            }

            $( "#login_form" ).find('.btn').removeClass( 'loading' );
        });
    }
});

