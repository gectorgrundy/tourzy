jQuery( document ).ready(function($) {
    var topCategories = jQuery('#topCategories');
    if (topCategories.length > 0)
    {
        $.get(window.category_list, function (response) {
            console.log( response, window.category_list );


            response.forEach(function (category) {
                $('#topCategories').append(`
                <div class="tour-category-item-cover">
                    <a class="tour-category-item" href="${ pageUrls.searchPage + '?category=' + category.id}">
                         <div class="tour-category-item-title">${category.title}</div>
                         <div class="tour-category-item-goto-button flex flex-align-center flex-justify-center flex-wrap"></div>
                    </a>
                </div>
            `)
            });

            if (window['category-slider']) {
                window['category-slider'].rebuild();
            } else if (optionsCategorySlider) {
                window['category-slider'] = tns(optionsCategorySlider);
            }
        });
    }
});
