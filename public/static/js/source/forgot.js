var validateForgot = new Validate('data-validate','#forgot_form');
$( "#forgot_form" ).submit(function( event ) {
    event.preventDefault();
    validateForgot.checkValidate();
    if (0 === Object.keys(validateForgot.errors).length) {
        $( "#forgot_form" ).find('.btn').addClass( 'loading' );

        var data = {};
        var email = '';
        let input = document.querySelector('.message-success-forgot input[name="_email"]');

        $(this).find('input[name]').each(function (index, element) {
            if (element.name == '_email') email = element.value;
            data[element.name] = element.value;
        });

        if ( $( "#forgot_form" ).closest(".form-cover").find(".form-warning").length > 0 ) {
            $( "#forgot_form" ).closest(".form-cover").find(".form-warning").removeClass("show");
        }

        $.post($(this).attr('action'), data, function (result) {
            if (result.status == true) {
                setValue(email,input);

                $( "#forgot_form" ).find('.btn').hide();
                $( "#forgot_form" ).closest(".form-right-side").find('.forgot-password-text-success').removeClass('hidden');

                $(this).find('input[name]').each(function (index, element) {
                    element.value = '';
                })
            }
            if (result.status == false) {
                if ( $( "#forgot_form" ).closest(".form-cover").find(".form-warning").length > 0 ) {
                    $( "#forgot_form" ).closest(".form-cover").find(".form-warning").addClass("show");
                } else {
                    showMessage(result.message);
                }
            }

            $( "#forgot_form" ).find('.btn').removeClass( 'loading' );
        });
    }
});

var validateForgotMessage = new Validate('data-validate','.reset-password');
$( ".reset-password" ).submit(function( event ) {
    event.preventDefault();
    validateForgotMessage.checkValidate();
    if (0 === Object.keys(validateForgotMessage.errors).length) {
        var data = {};
        $(this).find('input[name]').each(function (index, element) {
            data[element.name] = element.value;
        });
        $.post($(this).attr('action'), data, function (result) {
            if (result.status == true) removeActive('success-forgot');
            if (result.status == false) showMessage(result.message);
        });
    }
});

