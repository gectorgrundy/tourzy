var validateSign = new Validate('data-validate','#sign_form');
$("#sign_form").submit(function( event ) {
    event.preventDefault();
    validateSign.checkValidate();
    if (0 === Object.keys(validateSign.errors).length) {
        $( "#sign_form" ).find('.btn').addClass( 'loading' );

        var data = {};
        $(this).find('input[name]').each(function (index, element) {
            data[element.name] = element.value;
        })

        $.post($(this).attr('action'), data, (result) => {
            if (result.status == true) {
                $(".message-success-register input[type='email']").val( data._email );

                addActive('success-register');
                removeActive('sign');
            }
            if (result.status == false) {
                if ( $( "#sign_form" ).closest(".form-cover").find(".form-warning").length > 0 ) {
                    $( "#sign_form" ).closest(".form-cover").find(".form-warning").addClass("show");
                } else {
                    showMessage(result.message);
                }
            }

            $( "#sign_form" ).find('.btn').removeClass( 'loading' );
        });
    }
});
