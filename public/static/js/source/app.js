function fallbackCopyTextToClipboard(text) {
    var textArea = document.createElement("textarea");
    textArea.value = text;

    // Avoid scrolling to bottom
    textArea.style.top = "0";
    textArea.style.left = "0";
    textArea.style.position = "fixed";

    document.body.appendChild(textArea);
    textArea.focus();
    textArea.select();

    try {
        var successful = document.execCommand('copy');
        var msg = successful ? 'successful' : 'unsuccessful';
        console.log('Fallback: Copying text command was ' + msg);
    } catch (err) {
        console.error('Fallback: Oops, unable to copy', err);
    }

    document.body.removeChild(textArea);
}

function copyTextToClipboard(text) {
    if (!navigator.clipboard) {
        fallbackCopyTextToClipboard(text);
        return;
    }
    navigator.clipboard.writeText(text).then(function () {
        console.log('Async: Copying to clipboard was successful!');
    }, function (err) {
        console.error('Async: Could not copy text: ', err);
    });
}

const formatAmount = num => {
    let res = num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    if ( res.split(".").length === 1 ) {
        res = `${res}.00`;
    }

    return res;
};

function scrollTo($element) {
    if (!$element || $element && $element.length === 0) return;
    jQuery('html, body').animate({
        scrollTop: $element.offset().top
    }, 2000);
}

jQuery("[data-scroll]").click(function (){
    const selector = jQuery(this).attr("data-target");
    if (selector) scrollTo(jQuery(selector));
});

const optionsSlider = {
    "items": 4,
    "mouseDrag": false,
    "nav": false,
    "slideBy": 1,
    "fixedWidth" : 319,
    "responsive": {
        "400": {
            "items": 1,
            "slideBy": "page",
        },
        "767": {
            "items": 2,
            "slideBy": "page",
        },
        "1024": {
            "items": 3,
            "slideBy": "page",
        },
        "1280": {
            "items": 4,
            "slideBy": 1,
        }
    }
};

const optionsAllSlider = Object.assign({}, optionsSlider);
optionsAllSlider.container = '.all-slider-cover';
const prevAll = document.querySelector('.four-block .prev-item');
const nextAll = document.querySelector('.four-block .next-item');
if (prevAll && nextAll) {
    optionsAllSlider.prevButton = prevAll;
    optionsAllSlider.nextButton = nextAll;
} else {
    optionsAllSlider.controls = false;
}

try{
    const allSlider = tns(optionsAllSlider);
}catch (e){}

const optionsPopularSlider = Object.assign({}, optionsSlider);
optionsPopularSlider.container = '.popular-slider-cover';
const prevPopular = document.querySelector('.second-block .prev-item');
const nextPopular = document.querySelector('.second-block .next-item');
if (prevPopular && nextPopular) {
    optionsPopularSlider.prevButton = prevPopular;
    optionsPopularSlider.nextButton = nextPopular;
} else {
    optionsPopularSlider.controls = false;
}

try{
    const popularSlider = tns(optionsPopularSlider);
}catch (e){}

const optionsCategorySlider = Object.assign({}, optionsSlider);
optionsCategorySlider.container = '.tour-category-slider-cover';
optionsCategorySlider.loop = false;
const prevCategory = document.querySelector('.third-block .prev-item');
const nextCategory = document.querySelector('.third-block .next-item');
if (prevCategory && nextCategory) {
    optionsCategorySlider.prevButton = prevCategory;
    optionsCategorySlider.nextButton = nextCategory;
} else {
    optionsCategorySlider.controls = false;
}

try{
    window['category-slider'] = tns(optionsCategorySlider);
} catch (e){}

function addActive(selector) {
    if (selector) jQuery("[data-item=\""+selector+"\"]").addClass('active');
    jQuery('.overlay').addClass('active');
}

function removeActive(selector, hideOverlay = false) {
    if (selector) jQuery("[data-item=\""+selector+"\"]").removeClass('active');
    if (hideOverlay) jQuery('.overlay').removeClass('active');
}

function setValue(value,input) {
    if (!value || !input) return;
    input.value = value;

}

function showMessage(message) {
    var messageCover = document.querySelector('.message.message-cover');
    var messageContent = document.querySelector('.message .message-cover-content');
    if (message && messageCover) {
        messageContent.innerHTML = message;
        messageCover.classList.add('active');
        var timer = setTimeout(function () {
            _clear();
        }, 2500);
        messageCover.addEventListener('click', function () {
            clearTimeout(timer);
            _clear();
        })
    }
    function _clear() {
        messageContent.innerHTML = '';
        messageCover.classList.remove('active');
    }
}

jQuery("[data-close]").click(function (e){
    const selector = jQuery(this).attr("data-close-target");
    if (selector) {
        const selectorArray = selector.split('|');
        for (let i = 0; i < selectorArray.length; i++) {
            jQuery("[data-item=\""+selectorArray[i]+"\"]").removeClass('active');
        }
    }
    if (!this.hasAttribute("data-action")) jQuery('.overlay').removeClass('active');
    e.preventDefault();
});

jQuery("[data-action]").click(function (e){
    const selector = jQuery(this).attr("data-target");
    if (selector) jQuery("[data-item=\""+selector+"\"]").addClass('active');
    jQuery('.overlay').addClass('active');
    jQuery('.mobile-menu').removeClass('active');
    e.preventDefault();
});

jQuery('.show-password').click(function (e){
    const toggleText = jQuery(this).attr("data-toggle-text");
    const innerText = jQuery(this).html();
    jQuery(this).html(toggleText);
    jQuery(this).attr("data-toggle-text",innerText);
    const password = jQuery(this).parent().find('[type="password"]');
    const text = jQuery(this).parent().find('[type="text"]');
    if (password) password.attr("type","text");
    if (text) text.attr("type","password");
    e.preventDefault();
});

jQuery('[data-like-select]').each(function (index,element) {
    const target = jQuery(element).data('like-select-target');
    const input = jQuery('[data-like-select-item="'+target+'"]');
    const pattern = input.data('like-select-pattern');
    const placeholder = jQuery(element).find('.like-select-placeholder');
    jQuery(element).find('.like-select-options').click(function (e) {
        const content = jQuery(this).html();
        jQuery(element).find('.like-select-options').removeClass('active-select');
        jQuery(this).addClass('active-select');
        if (placeholder) {
            placeholder.html(content);
            placeholder.addClass('value');
        }
        if (input) {
            if (pattern) {
                checkValueExist(target,pattern,input);
            }
        }
    });
});

function checkValueExist(target,pattern,input) {
   const likeSelect = jQuery('[data-like-select-target="'+target+'"]');
   let validate = 0;
   let value = pattern;
   likeSelect.each(function (index,element) {
       let checkValue = jQuery(element).find('.active-select');
       const elementKey = jQuery(element).data('like-select');
       if (checkValue.length == 0) {
           validate++;
           jQuery(element).removeClass('success');
       }
       else
       {
           jQuery(element).addClass('success');
           const content = checkValue[0].innerHTML;
           value = setDataByPattern(value,elementKey,content);
       }
   });
   if (value !== pattern && validate == 0)
   {
       input.val(value);
       //let data = Date.parse(value).toISOString();
       var event = new Event('input', {
           bubbles: true,
           cancelable: true,
       });
       input[0].dispatchEvent(event);
   }
}

function setDataByPattern(pattern,replace,replaceTo) {
    if (pattern && replace) {
        let regExp = new RegExp('\\{'+replace+'\\}','gi');
        pattern = pattern.replace(regExp,replaceTo);
    }
    return pattern;
}

function checkHeaderPosition() {
    var scrollTop = jQuery(this).scrollTop();
    var header = jQuery('.header-cover');
    var headerHeight = (header.length > 0) ? header.height() : 0;
    var firstBlock = jQuery('.full-size-block');
    var firstBlockHeight = (firstBlock.length > 0) ? firstBlock.height() : 0;
    if (firstBlock.length > 0)
    {
        if ((firstBlockHeight - headerHeight) >= scrollTop) {
            header.removeClass('active-header');
        } else {
            header.addClass('active-header');
        }
    }
}
jQuery(window).scroll(checkHeaderPosition);
checkHeaderPosition();

$(function() {

    const ua = navigator.userAgent.toLowerCase();
    const isAndroid = ua.indexOf("android") > -1; //&& ua.indexOf("mobile");
    if(isAndroid) {
      $('[data-item="login"] input, [data-item="sign"] input').toArray().forEach(input => $(input).attr('autocomplete', 'off'));
    }

    $(".mobile-menu-trigger").click(_ => {
        $(".mobile-menu").toggleClass('active');
    });

    $('.button-goto-search').click(() => {
        window.location.href = $('.form-search').attr('action');
    });

    tippy('.tippy', {
    });

    $('.option-box .option-item').click(function() {
        $(this)
            .addClass('active')
            .siblings().removeClass('active');
    });

    $(".checkbox").click(function() {
        $(this).find("input").get(0).checked = !$(this).find("input").get(0).checked;
    });

    if ( $('.detail-tour-container .slider-wrapper ul li').length > 3 ) {
        const $detailTourSlider = $('.detail-tour-container .slider-wrapper ul').lightSlider({
            item: 3,
            loop: true,
            slideMove: 1,
            easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
            pager: false,    
            controls: false,        
            speed: 600,
            responsive : [
                {
                    breakpoint: 1200,
                    settings: {
                        item: 1,
                        slideMove: 1,
                        // slideMargin: 6,
                      }
                },
                {
                    breakpoint: 992,
                    settings: {
                        item: 1,
                        slideMove: 1,
                        // slideMargin: 0,
                      }
                },
                {
                    breakpoint: 768,
                    autoWidth:true,
                    settings: {
                        item: 1,
                        slideMove: 1,
                        slideMargin: 0,
                      }
                },
            ]
        });  

        $('.detail-tour-container .control-item.prev').click(function(){
            $detailTourSlider.goToPrevSlide(); 
        });
        $('.detail-tour-container .control-item.next').click(function(){
            $detailTourSlider.goToNextSlide(); 
        });
    } else {
        $('.detail-tour-container .control-item.prev').hide();
        $('.detail-tour-container .control-item.next').hide();

        $('.detail-tour-section .tour-info').addClass('has-' + $('.detail-tour-container .slider-wrapper ul li').length);
        if ($('.detail-tour-container .slider-wrapper ul li').length === 2) {
            $(".tour-info").prepend(`<p class="h1 mb-24">${ $(".bottom-bar .h1").text().trim() }</p>`);
        }
    }

    const getParameterByName = (name, url) => {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, '\\$&');
        var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, ' '));
    }

    if ( getParameterByName('login') === "true" ) {
        $('[data-action][href="#login"]').click();
    }

    if ( getParameterByName('signup') === "true" ) {
        $('[data-action][href="#sign"]').click();
    }

    if ( getParameterByName('search-tour') ) {
        $('input[name="search-tour"]').val( getParameterByName('search-tour') );
    }

    // window.history.pushState({}, document.title, window.location.href.split('?')[0]);

    $('.open-modal').magnificPopup({
        type:'inline',
        mainClass: 'mfp-fade',
        midClick: true,
        removalDelay: 300,
        callbacks: {
            close: function() {
                // $('form input[name="redirect_to"]').val("/tools/");
            }
        }
    });
});
