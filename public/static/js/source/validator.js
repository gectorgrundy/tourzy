function Validate(selector,coverClass) {
    if (!selector) return false;
    this.errors = {};
    this.fields = [];
    this.selector = '';
    this._init = function (selector) {
        if ( !selector ) return;
        var querySelector = '';
        if (coverClass) querySelector += coverClass + ' ';
        querySelector += '['+selector+']';
        var elemets = document.querySelectorAll(querySelector);
        if (elemets) {
            this._forEach.call(this,elemets,selector);
        }
        this.selector = selector;
        return this.errors;
    };
    this._forEach = function (elemets,selector) {
        if ( this != document && !Array.isArray(elemets) && !selector ) return;
        for (var i = 0; i < elemets.length; i++) {
            var method = elemets[i].getAttribute(selector);
            if (method) {
                if (this._chekMethodExist(method)) {
                    var name = elemets[i].getAttribute('name');
                    if (name) this.errors[name] = true;
                    this._addListener.call(this,elemets[i],method,name);
                    this.fields.push(elemets[i]);
                }
            }
        }
    };
    this._addListener = function(element,method) {
        if ( !method && !element && !name ) return false;
        element.addEventListener('blur', this[method].bind(element,this,name));
        element.removeAttribute('required');
    };
    this._chekMethodExist = function(method) {
        if ( !method ) return false;
        if ( typeof this[method] == 'function' ) {
            return true;
        }
        return false;
    };
    this._addError = function() {
        if ( this == document ) return;
        this.classList.add('error');
        this.classList.remove('success');
        this.parentNode.classList.add('error');
        this.parentNode.classList.remove('success');
    };
    this._removeError = function() {
        if ( this == document ) return;
        this.classList.remove('error');
        this.classList.add('success');
        this.parentNode.classList.remove('error');
        this.parentNode.classList.add('success');
    };
    this._validateEmail = function (object,name) {
        if ( this == document && !object && !name) return;
        var reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (!reg.test(this.value)) {
            object.errors[name] = true;
            object._addError.call(this);
        } else {
            delete object.errors[name];
            object._removeError.call(this);
        }
    };
    this._validateFile = function (object,name) {
        if ( this == document && !object && !name) return;
        var status = false;
        var extensionAccept = this.getAttribute('data-accept');
        if (this.files.length != 0){
            if(extensionAccept){
                extensionAccept = extensionAccept.split(',');
                if (extensionAccept.indexOf(this.files[0].type) !== -1) {
                    status = true;
                } else {
                    this.value = "";
                }
            } else {
                status = true;
            }
        }
        if (!status) {
            object.errors[name] = true;
            object._addError.call(this);
        } else {
            delete object.errors[name];
            object._removeError.call(this);
        }
    };
    this._validateChecked = function (object,name) {
        if ( this == document && !object && !name) return;
        if (!this.checked){
            object.errors[name] = true;
            object._addError.call(this);
        } else {
            delete object.errors[name];
            object._removeError.call(this);
        }
    };
    this._validateLength = function (object,name) {
        if ( this == document && !object && !name) return;
        var minValue = this.getAttribute('min');
        var minLength = (minValue) ? minValue : 0;
        if (this.value.length < minLength){
            object.errors[name] = true;
            object._addError.call(this);
        } else {
            delete object.errors[name];
            object._removeError.call(this);
        }
    };
    this._validateTextArea = function (object,name) {
        if ( this == document && !object && !name) return;
        var minValue = this.getAttribute('min');
        var minLength = (minValue) ? minValue : 0;
        if (this.value.length <= minLength){
            object.errors[name] = true;
            object._addError.call(this);
        } else {
            delete object.errors[name];
            object._removeError.call(this);
        }
    }
    this.setError = function (errorName,errorValue) {
        this.errors[errorName] = errorValue;
    }
    this.deleteError = function (errorName) {
        delete this.errors[errorName];
    }
    this.checkValidate = function () {
        if ( !this.fields && this.fields.length == 0 && !this.selector ) return;
        for (var i = 0; i < this.fields.length; i++) {
            var method = this.fields[i].getAttribute(this.selector);
            if (method) {
                if (this._chekMethodExist(method)) {
                    var name = this.fields[i].getAttribute('name');
                    this[method].call(this.fields[i],this,name);
                }
            }
        }
    }
    this._init(selector);
    return this;
};