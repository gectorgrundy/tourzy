function initStep9() {
    $('#step9').show();

    $('#verifyStep9').click(function () {

        const equipmentRequiredType = $('input[name=equipmentRequiredType]:checked').val();

        if(equipmentRequiredType === undefined){
            console.log('invalid');
            return
        }

        $.post(window.tour_api.step9, {
            id: currentTour.id,
            equipmentRequiredType: equipmentRequiredType,
            whatToBring: $('#whatToBring').val(),
        }, function (response) {
            currentStep = currentStep + 1;
            initByStep(currentStep)
        });
    })
}