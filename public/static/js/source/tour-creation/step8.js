function initStep8() {
    $('#step8').show();

    const groupMaxSize = $('#groupMaxSize');
    const groupMinAge = $('#groupMinAge');
    const alcoholInvolved = $('#alcoholInvolved');
    groupMaxSize.empty();
    groupMinAge.empty();

    for (let i = 1; i <= 10; i++ ){
        groupMaxSize.append($("<option></option>").attr("value",i).text(i));
    }
    for (let i = 6; i <= 21; i++ ){
        groupMinAge.append($("<option></option>").attr("value",i).text(i));
    }

    alcoholInvolved.change(function () {
        const checked = $(this).is(':checked');

        if(checked){
            groupMinAge.val(21);

            groupMinAge.prop('disabled', 'disabled');
        }else{
            groupMinAge.prop('disabled', false);
        }
        console.log(checked)
    });

    $('#verifyStep8').click(function () {
        $.post( window.tour_api.step8, {
            id: currentTour.id,
            groupMaxSize: groupMaxSize.val(),
            groupMinAge: groupMinAge.val(),
        }, function (response) {
            currentStep = currentStep + 1;
            initByStep(currentStep)
        });
    })
}