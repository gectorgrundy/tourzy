<?php

use App\Http\Controllers\Auth\VerifyEmailController;
use App\Http\Controllers\Auth\LoginController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\TourController;
use App\Http\Controllers\StripeController;
use App\Http\Controllers\BookingController;
use App\Http\Controllers\ReviewController;
use App\Http\Controllers\PayoutController;

Route::group(['middleware' => 'guest'], function () {
    Route::get('/reset-password/{token}', function ($token) {
        return redirect(route('home'))
            ->with('reset-password-token', $token)
            ->with('reset-password-email', request()->get('email'));
    })->name('password.reset');
});

Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('search', [TourController::class, 'search'])->name('search');
Route::view('policy', 'pages.policy.index')->name('policy');
Route::view('refund', 'pages.refund.index')->name('refund');

Route::group(['prefix' => 'tour'], function () {
    Route::get('detail/{tour}', [TourController::class, 'detail'])->name('tour-detail');
});
Route::group(['prefix' => 'stripe', 'as' => 'stripe.'], function () {
    Route::get('payment-process/{booking}', [StripeController::class, 'paymentProcess'])->name('payment-process');
    Route::post('web-hook', [StripeController::class, 'webHook']);
});

Route::middleware(['auth'])->group(function () {
    Route::group(['prefix' => 'auth'], function () {
        Route::get('/logout', [LoginController::class, 'logout'])->name('logout');
    });

    Route::get('/verify-email/{id}/{hash}', [VerifyEmailController::class, '__invoke'])->name('verification.verify');

    Route::view('settings', 'pages.settings.index')->name('settings');
    Route::view('tour-verification', 'pages.tour-verification.index')->name('tour-verification');
    Route::view('tour-instruction', 'pages.tour-instruction.index')->name('tour-instruction');

    Route::get('tour-payment', [TourController::class, 'payment'])->name('tour-payment');

    Route::post('review-create/{booking}', [ReviewController::class, 'create'])->name('review-create');

    Route::group(['prefix' => 'my'], function () {

        Route::group(['prefix' => 'tour'], function () {
            Route::view('/', 'pages.my-tour.list')->name('my-tour-list');
            Route::view('edit', 'pages.my-tour.edit')->name('my-tour-edit');

            Route::get('create', [TourController::class, 'myTourCreate'])->name('my-tour-create');
            Route::get('detail', [TourController::class, 'myTourDetail'])->name('my-tour-detail');
        });

        Route::get('host', [TourController::class, 'host'])->name('host');
        Route::get('payments', [PayoutController::class, 'myPayments'])->name('my-payments');
        Route::view('schedule', 'pages.my-schedule.index')->name('my-schedule');

        Route::get('purchased-tours', [BookingController::class, 'myBookings'])->name('my-bookings');
        Route::get('purchased-tour/{booking}', [BookingController::class, 'myBooking'])->name('my-booking');
        Route::get('purchased-tour/{booking}/qr-code', [BookingController::class, 'activationQRCode'])->name('my-booking-qr-code');
        Route::get('purchased-tour/{token}/activate', [BookingController::class, 'activate'])->name('my-booking-activate');
    });

    Route::group(['prefix' => 'admin', 'as' => 'admin.'], function () {
        Route::get('/', [AdminController::class, 'home'])->name('home');
        Route::get('/user', [AdminController::class, 'users'])->name('users');
        Route::get('/user/{user}', [AdminController::class, 'user'])->name('user');
        Route::get('/tour', [AdminController::class, 'tours'])->name('tours');
        Route::get('/payout', [AdminController::class, 'payouts'])->name('payouts');
    });
});