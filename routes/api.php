<?php

use App\Http\Controllers\StripeController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\ResetPasswordController;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\TourController;
use App\Http\Controllers\Api\FileController;
use App\Http\Controllers\Api\TourVerificationController;
use App\Http\Controllers\Api\TourScheduleController;
use App\Http\Controllers\Api\BookingController;
use App\Http\Controllers\Api\PayoutController;
use App\Http\Controllers\Api\ReviewController;
use App\Http\Controllers\Api\Admin\UserController as AdminUserController;
use App\Http\Controllers\Api\Admin\TourController as AdminTourController;
use App\Http\Controllers\Api\Admin\TourVerificationController as AdminTourVerificationController;
use App\Http\Controllers\Api\Admin\PayoutController as AdminPayoutController;

Route::group(['prefix' => 'auth'], function () {
    Route::post('/login', [LoginController::class, 'login']);
    Route::post('/register', [RegisterController::class, 'index']);

    Route::post('/forgot-password', [ResetPasswordController::class, 'forgot']);
    Route::post('/reset-password', [ResetPasswordController::class, 'index']);
});

Route::post('tour/search', [TourController::class, 'search']);

Route::group(['prefix' => 'stripe'], function () {
    Route::post('create-payment-intent', [StripeController::class, 'createPaymentIntent']);
    Route::post('web-hook-dev', [StripeController::class, 'webHookDev']);
});

Route::middleware(['auth'])->group(function () {
    Route::post('contact-host', [UserController::class, 'contactHost']);
    Route::get('review', [ReviewController::class, 'index']);

    Route::group(['prefix' => 'settings'], function () {
        Route::get('/', [UserController::class, 'index']);
        Route::post('/', [UserController::class, 'save']);
    });
    Route::group(['prefix' => 'payout'], function () {
        Route::post('withdrawal', [PayoutController::class, 'withdrawal']);
    });
    Route::group(['prefix' => 'tour'], function () {
        Route::get('my-tour', [TourController::class, 'getMyTour']);
        Route::put('save', [TourController::class, 'save']);
    });
    Route::group(['prefix' => 'tour-verification'], function () {
        Route::get('/', [TourVerificationController::class, 'index']);
        Route::put('/', [TourVerificationController::class, 'save']);
    });
    Route::group(['prefix' => 'tour-schedule'], function () {
        Route::get('/', [TourScheduleController::class, 'index']);
        Route::put('/', [TourScheduleController::class, 'save']);
    });
    Route::group(['prefix' => 'booking'], function () {
        Route::post('/', [BookingController::class, 'create']);
        Route::post('cancel', [BookingController::class, 'cancel']);
        Route::post('activate', [BookingController::class, 'activate']);
    });

    Route::group(['prefix' => 'file'], function () {
        Route::get('/', [FileController::class, 'index']);
        Route::post('/upload', [FileController::class, 'upload']);
        Route::delete('/', [FileController::class, 'delete']);
    });

    Route::group(['prefix' => 'admin'], function () {
        Route::group(['prefix' => 'user'], function () {
            Route::get('list', [AdminUserController::class, 'getList']);
            Route::get('single', [AdminUserController::class, 'getSingle']);
            Route::post('toggle-block', [AdminUserController::class, 'toggleBlock']);
        });

        Route::group(['prefix' => 'tour-verification'], function () {
            Route::post('approve', [AdminTourVerificationController::class, 'approve']);
            Route::post('decline', [AdminTourVerificationController::class, 'decline']);
        });

        Route::group(['prefix' => 'tour'], function () {
            Route::get('list', [AdminTourController::class, 'getList']);
            Route::post('approve', [AdminTourController::class, 'approve']);
            Route::post('decline', [AdminTourController::class, 'decline']);
        });

        Route::group(['prefix' => 'payout'], function () {
            Route::get('list', [AdminPayoutController::class, 'getList']);
            Route::post('host-withdrawal/accept', [AdminPayoutController::class, 'hostWithdrawalAccept']);
            Route::post('host-withdrawal/decline', [AdminPayoutController::class, 'hostWithdrawalDecline']);
            Route::post('cancel-booking/accept', [AdminPayoutController::class, 'cancelBookingAccept']);
            Route::post('cancel-booking/decline', [AdminPayoutController::class, 'cancelBookingDecline']);

        });
    });
});