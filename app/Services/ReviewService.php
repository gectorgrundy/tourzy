<?php

namespace App\Services;

use App\Models\Booking;
use App\Models\Review;
use App\Models\Tour;
use DateTime;

class ReviewService
{
    public function getReviewsByTour($tourId, $page = 1)
    {
        return Review::with('user.avatar')
            ->leftJoin('bookings', 'bookings.id', '=', 'reviews.booking_id')
            ->where('bookings.tour_id', $tourId)
            ->paginate(3, ['*'], 'page', $page);
    }

}