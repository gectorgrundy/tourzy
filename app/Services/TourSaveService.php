<?php

namespace App\Services;

use App\Models\Category;
use App\Models\Tour;
use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

class TourSaveService
{
    public function save(Request $request)
    {
        $step = $request->get('step', 11);
        $tour = Tour::query()->find($request->get('id'));

        $step = $step > 11 ? 11 : $step;

        for ($i = 1; $i <= $step; $i++) {
            $stepSaveMethod = "step{$i}";

            $this->$stepSaveMethod($tour, $request);
        }
        if ($tour->step === 11) {
            $tour->status = Tour::STATUS_VALIDATING_BY_VERIFY_ID;
        }

        if ($tour->verification_status === Tour::VERIFICATION_STATUS_DECLINED) {
            $tour->verification_status = Tour::VERIFICATION_STATUS_PENDING;
        }

        $tour->step = $step + 1;
        $tour->save();

        return $tour;
    }

    public function step1(Tour &$tour, Request $request)
    {
        $request->validate([
            'city_location' => 'required'
        ]);

        $tour->city_location = $request->get('city_location');
    }

    public function step2(Tour $tour, Request $request)
    {
        $request->validate([
            'category' => [Rule::requiredIf(!$request->get('custom_category_title'))],
            'custom_category_title' => [
                Rule::requiredIf(!$request->get('category')), 'max:25'
            ],
        ]);

        if ($category = $request->get('category')) {
            $tour->category_id = $category['id'];
            $tour->custom_category_title = null;
        } else {
            $tour->category_id = null;
            $tour->custom_category_title = $request->get('custom_category_title');
        }
    }

    public function step3(Tour $tour, Request $request)
    {
        $request->validate([
            'name' => 'required|min:5|max:25'
        ]);

        $tour->name = $request->get('name');
    }

    public function step4(Tour $tour, Request $request)
    {
        $request->validate([
            'experience_description' => 'required|max:450',
            'places_description' => 'required|max:450'
        ]);

        $tour->experience_description = $request->get('experience_description');
        $tour->places_description = $request->get('places_description');
    }

    public function step5(Tour $tour, Request $request)
    {
        $request->validate([
            'cover_photo' => 'required',
        ]);

        $tour->cover_photo_id = $request->get('cover_photo')['id'];

        $tour->gallery_photos()->detach();
        $tour->gallery_photos()->attach(collect($request->get('gallery_photos'))->pluck('id'));

        if ($youtubeVideoLink = $request->get('youtube_video_link')) {
            $tour->youtube_video_link = $youtubeVideoLink;
        }
    }

    public function step6(Tour $tour, Request $request)
    {
        $request->validate([
            'country_meet' => 'required',
            'address_meet' => 'required',
            'city_meet' => 'required',
            'directions_description' => 'max:450',
        ]);

        $tour->country_meet = $request->get('country_meet');
        $tour->city_meet = $request->get('city_meet');
        $tour->address_meet = $request->get('address_meet');
        $tour->state_meet = $request->get('state_meet');
        $tour->app_meet = $request->get('app_meet');
        $tour->latitude = $request->get('latitude');
        $tour->longitude = $request->get('longitude');
        $tour->directions_description = $request->get('directions_description');
    }

    public function step7(Tour $tour, Request $request)
    {
        $request->validate([
            'group_max_size' => 'required',
            'group_min_age' => 'required',
            'additional_requirements' => 'max:450',
        ]);

        $tour->group_max_size = $request->get('group_max_size');
        $tour->group_min_age = $request->get('group_min_age');
        $tour->additional_requirements = $request->get('additional_requirements');
        $tour->has_alcohol = $request->get('has_alcohol');
    }

    public function step8(Tour $tour, Request $request)
    {
        $request->validate([
            'equip_required_type' => 'required',
            'equipment_what_to_bring' => 'max:450',
        ]);

        $tour->equip_required_type = $request->get('equip_required_type');
        $tour->equipment_what_to_bring = $request->get('equipment_what_to_bring');
    }

    public function step9(Tour $tour, Request $request)
    {
        $request->validate([
            'notes' => 'max:450',
        ]);

        $tour->notes = $request->get('notes');

    }

    public function step10(Tour $tour, Request $request)
    {
        $request->validate([
            'price' => 'required',
        ]);

        $tour->price = doubleval($request->get('price'));
    }

    public function step11(Tour $tour, Request $request)
    {
        $request->validate([
            'duration' => 'required',
        ]);

        $tour->duration = floatval($request->get('duration'));
    }
}