<?php

namespace App\Services;

use App\Http\Requests\Request;
use App\Models\Booking;
use App\Models\Tour;
use App\Models\TourVerification;
use DateTime;
use Illuminate\Database\Eloquent\Builder;
use function Symfony\Component\String\u;

class TourService
{
    public function tourBaseQuery()
    {
        return Tour::query()
            ->leftJoin('users as user', 'tours.user_id', '=', 'user.id')
            ->leftJoin('tour_verifications as verification', 'tours.id', '=', 'verification.tour_id')
            ->where('verification_status', Tour::VERIFICATION_STATUS_ACCEPTED)
            ->where('verification.status', TourVerification::STATUS_ACCEPTED)
            ->where('user.is_blocked', false);
    }

    public function search(Request $request)
    {
        return $this->tourBaseQuery()
            ->with('user', 'cover_photo', 'category')
            ->withCount('reviews')
            ->when($search = $request->get('search'), function (Builder $query) use ($search){
                return $query->where('city_location', 'like', "%$search%");
            })
            ->when($price = $request->get('price'), function (Builder $query) use ($price){
                return $query->whereBetween('price', [$price['from'], $price['to']]);
            })
            ->when($date = $request->get('date'), function (Builder $query) use ($date){
                return $query->whereBetween('verification.submitted_at', [$date['from'], $date['to']]);
            })
            ->when($guestCount = $request->get('guest_count'), function (Builder $query) use ($guestCount){
                return $query->where('group_max_size', '>=', $guestCount);
            })
            ->when($category = $request->get('category'), function (Builder $query) use ($category){
                return $query->where('category_id',  $category);
            })
            ->when($categories = $request->get('categories'), function (Builder $query) use ($categories){
                return $query->where(function (Builder $query) use ($categories){
                    if (in_array(-1, $categories)){
                        $query->whereNotNull('custom_category_title');
                    }

                    if ($categories = array_values(array_diff($categories, [-1]))) {
                        $query->orWhereIn('category_id',  $categories);
                    }
                });
            })
            ->orderBy($request->get('sort_by', 'visit_count'), $request->get('sort_direction', 'desc'))
            ->paginate(9, ['*'], 'page', $request->get('page', 1));
    }


    public function getFormattedSchedule($tour)
    {
        $schedule = $tour->schedule;

        if (!$schedule) {
            return collect([]);
        }

        $bookings = $tour->bookings->filter(function (Booking $booking) {
            return $booking->activation_status === Booking::ACTIVATION_STATUS_PENDING && $booking->status === Booking::STATUS_SUCCESS;
        });

        return collect($tour->schedule->data)
            ->filter(fn($item) => new DateTime('yesterday') < new DateTime($item['date']))
            ->map(function ($date) use ($bookings, $tour) {

                $dateBookings = $bookings->filter(function (Booking $booking) use ($date) {
                    return $booking->date->format('Y-m-d') === $date['date'];
                });

                $timeSlots = collect($date['scheduledTimeSlots']);

                return [
                    'date' => $date['date'],
                    'scheduledTimeSlots' => $timeSlots->map(function ($time) use ($dateBookings, $tour) {
                        $sum = $dateBookings->filter(function (Booking $booking) use ($time){
                            return $booking->time === $time;
                        })
                            ->sum('guest_count');

                        return [
                            'time' => $time,
                            'availableSlots' => $tour->group_max_size - $sum
                        ];
                    })
                ];
            })->values();
    }

    public function recalculateRating(Tour $tour)
    {
        $rating = \DB::select("SELECT avg(r.rating) AS rating FROM reviews r LEFT JOIN bookings b ON r.booking_id = b.id WHERE b.tour_id = $tour->id");

        $tour->rating = $rating[0]->rating;
        $tour->save();
    }
}