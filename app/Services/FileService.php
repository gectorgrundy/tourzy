<?php

namespace App\Services;


use App\Models\File;
use Intervention\Image\Facades\Image;
use Storage;

class FileService
{
    public function uploadFile($file, $extension, $userId)
    {
        $disk = Storage::disk('uploads');
        $destination_path = 'files/' . $userId;
        $filename = md5($file . time()) . '.' . $extension;

        $image = Image::make($file)->orientate()->resize(1000, 1000, function ($constraint) {
            $constraint->aspectRatio();
        })->encode($extension, 100);

        $small = config('image.sizes.small');
        $medium = config('image.sizes.medium');
        $big = config('image.sizes.big');
        [$width, $height] = explode('x', $small);
        $thumb270x250 = Image::make($file)->fit($width, $height)->encode($extension, 100);

        list($width, $height) = explode('x', $medium);
        $thumb288x460 = Image::make($file)->fit($width, $height)->encode($extension, 100);

        list($width, $height) = explode('x', $big);
        $thumb1230х820= Image::make($file)->fit($width, $height)->encode($extension, 100);

        $disk->put("$destination_path/$filename", $image->stream());
        $disk->put("$destination_path/thumb-$small-$filename", $thumb270x250->stream());
        $disk->put("$destination_path/thumb-$medium-$filename", $thumb288x460->stream());
        $disk->put("$destination_path/thumb-$big-$filename", $thumb1230х820->stream());

        return $filename;
    }
}