<?php

namespace App\Services;

use App\Models\Booking;
use App\Models\Payout;
use DateTime;

class BookingService
{
    public function activate(Booking $booking)
    {
        $booking->activation_status = Booking::ACTIVATION_STATUS_SUCCESS;
        $booking->finished_at = now();

        $payout = new Payout();
        $payout->booking_id = $booking->id;
        $payout->user_id = $booking->tour->user_id;
        $payout->amount = $booking->price * $booking->guest_count;
        $payout->type = Payout::TYPE_HOST_EARNING;
        $payout->status = Payout::STATUS_SUCCESS;

        $user = $booking->tour->user;
        $user->balance = $user->balance + $payout->amount;

        $payout->save();
        $user->save();
        $booking->save();

        return $booking;
    }

    public function calculateMoneyBack(?Booking $booking)
    {
        if (!$booking) {
            return null;
        }

        $bookingDatetime = $booking->date->setTime($booking->time, 0);
        $cancelDateTime = new DateTime();

        $cancelPayout = Payout::query()
            ->where('booking_id', $booking->id)
            ->where('type', Payout::TYPE_USER_BOOKING_CANCEL)
            ->where('status', Payout::STATUS_PENDING)
            ->orderBy('created_at', 'desc')
            ->first();

        if ($cancelPayout) {
            $cancelDateTime = $cancelPayout->created_at;
        }

        $hoursDiff = iterator_count(new \DatePeriod($cancelDateTime, new \DateInterval('PT1H'), $bookingDatetime));
        $amount = $booking->price * $booking->guest_count;
        $percent = 100;

        if ($hoursDiff > 24 && $hoursDiff < 48) {
            $percent = 50;
        } elseif ($hoursDiff < 24) {
            $percent = 0;
        }

        return compact('bookingDatetime', 'cancelDateTime', 'amount', 'percent');
    }
}