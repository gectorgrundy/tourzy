<?php

namespace App\Services;

use App\Models\Booking;
use App\Models\BookingStripeData;
use App\Models\StripeLog;
use DB;

class StripeService
{
    public function getBookingStripeDataByBooking(Booking $booking)
    {
        $bookingStripeData = BookingStripeData::query()->whereBookingId($booking->id)->first();

        if (!$bookingStripeData) {
            $bookingStripeData = new BookingStripeData();
            $bookingStripeData->booking_id = $booking->id;
            $bookingStripeData->save();
        }

        return $bookingStripeData;
    }

    public function getBookingStripeDataByStripeId(string $stripeId)
    {
        return BookingStripeData::query()
            ->where(DB::raw('JSON_EXTRACT(`intent_data`, "$.id")'), '=', $stripeId)
            ->first();
    }

    public function saveLog($data)
    {
        StripeLog::query()->create([
            'data' => $data
        ]);
    }
}