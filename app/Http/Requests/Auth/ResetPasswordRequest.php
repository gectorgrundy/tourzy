<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;
use Spatie\Permission\Models\Role;

class ResetPasswordRequest extends Request
{
    public function rules(): array
    {
        return [
            'email' => 'required|string|exists:users,email',
        ];
    }

    public function messages()
    {
        return [
            'email.exists' => 'It seems that this e-mail is not registered within our platform'
        ];
    }

}
