<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;
use Spatie\Permission\Models\Role;

class RegisterRequest extends Request
{
    public function rules(): array
    {
        return [
            'email' => 'required|string|unique:users,email',
        ];
    }

    public function messages()
    {
        return [
            'email.unique' => 'User already exists'
        ];
    }

}
