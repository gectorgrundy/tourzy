<?php

namespace App\Http\Requests\Settings;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;
use Spatie\Permission\Models\Role;

class SaveRequest extends Request
{
    public function rules(): array
    {
        return [
            'first_name' => 'required|string',
            'email' => 'required|string',
            'role_id' => 'required|exists:roles,id',
            'broker_id' => [Rule::requiredIf(function () {
                $role = Role::query()->find($this->get('role_id'));

                return $role && $role->name === 'broker';
            }), 'exists:brokers,id'],
            'password' => 'required|string',
            'password_repeat' => 'required|same:password',
        ];
    }

}
