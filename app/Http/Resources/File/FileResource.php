<?php

namespace App\Http\Resources\File;

use Illuminate\Http\Resources\Json\JsonResource;

class FileResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'size' => $this->size,
            'extension' => $this->extension,
            'alt' => $this->alt,
            'created_at' => $this->created_at,
            'src' => image_url($this->resource),
            'small_src' => image_url($this->resource, 'small'),
            'medium_src' => image_url($this->resource, 'medium'),
            'big_src' => image_url($this->resource, 'big'),
        ];
    }
}
