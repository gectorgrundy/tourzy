<?php

namespace App\Http\Resources\TourVerification;

use App\Http\Resources\File\FileResource;
use App\Models\TourVerification;
use Illuminate\Http\Resources\Json\JsonResource;

class TourVerificationResource extends JsonResource
{
    public function toArray($request)
    {
        if (!$this->resource) {
            return null;
        }

        $photos = $this->photos;

        return [
            'id' => $this->id,
            'type' => $this->type,
            'status' => $this->status,

            'front_photo' => FileResource::make($photos->get(TourVerification::PHOTO_TYPE_FRONT_INDEX)),
            'back_photo' => FileResource::make($photos->get(TourVerification::PHOTO_TYPE_BACK_INDEX)),
            'with_id_photo' => FileResource::make($photos->get(TourVerification::PHOTO_TYPE_WITH_ID_INDEX)),
        ];
    }
}
