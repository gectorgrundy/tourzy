<?php

namespace App\Http\Resources\User;

use App\Http\Resources\Category\CategoryResource;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'email' => $this->email,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'phone' => $this->phone,
            'is_blocked' => $this->is_blocked,
            'is_login_verified' => $this->is_login_verified,
            'block_message' => $this->block_message,
        ];
    }
}
