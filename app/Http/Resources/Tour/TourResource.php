<?php

namespace App\Http\Resources\Tour;

use App\Http\Resources\Category\CategoryResource;
use App\Http\Resources\File\FileResource;
use App\Http\Resources\User\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;

class TourResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'step' => $this->step,
            'status' => $this->status,
            'verification_status' => $this->verification_status,
            'name' => $this->name,
            'city_location' => $this->city_location,
            'experience_description' => $this->experience_description,
            'places_description' => $this->places_description,
            'directions_description' => $this->directions_description,
            'country_meet' => $this->country_meet,
            'city_meet' => $this->city_meet,
            'state_meet' => $this->state_meet,
            'address_meet' => $this->address_meet,
            'app_meet' => $this->app_meet,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'group_min_age' => $this->group_min_age,
            'group_max_size' => $this->group_max_size,
            'has_alcohol' => $this->has_alcohol,
            'additional_requirements' => $this->additional_requirements,
            'equip_required_type' => $this->equip_required_type,
            'equipment_what_to_bring' => $this->equipment_what_to_bring,
            'duration' => $this->duration,
            'notes' => $this->notes,
            'price' => $this->price,
            'youtube_video_link' => $this->youtube_video_link,
            'custom_category_title' => $this->custom_category_title,
            'decline_message' => $this->decline_message,

            'user' => UserResource::make($this->user),
            'category' => CategoryResource::make($this->category),
            'cover_photo' => FileResource::make($this->cover_photo),
            'gallery_photos' => $this->gallery_photos->map(fn($file) => FileResource::make($file)),
            'verification' => $this->verification,
            'schedule' => $this->schedule,
        ];
    }
}
