<?php

namespace App\Http\Resources\Category;

use App\Http\Resources\BaseCollection;

class CategoryCollection extends BaseCollection
{
    public function toArray($request)
    {
        return [
            'data' => CategoryResource::collection($this->collection),
        ];
    }
}
