<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth as R;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Auth, Hash, Session;

class RegisterController extends Controller
{
    public function index(R\RegisterRequest $request)
    {
        $birthday = Carbon::parse( $request->get('birthday'));

        $user = User::query()->create([
            'email' => $request->get('email'),
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'password' => Hash::make($request->get('password')),
            'day_of_birth' => $birthday->format('d'),
            'month_of_birth' => $birthday->format('m'),
            'year_of_birth' => $birthday->format('Y'),
            'last_login_at' => now(),
        ]);

        Auth::login($user);
        Auth::user()->sendEmailVerificationNotification();

        Session::put('check-email', true);

        return new JsonResponse();
    }

}
