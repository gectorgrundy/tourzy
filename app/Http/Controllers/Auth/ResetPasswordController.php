<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth as R;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Http\JsonResponse;
use Auth, Hash;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Password;

class ResetPasswordController extends Controller
{
    public function index(Request $request)
    {
        $credentials = $request->only('email', 'password', 'password_confirmation', 'token');

        $status = Password::reset($credentials, function ($user) use ($request){
            $user->forceFill([
                'password' => Hash::make($request->get('password')),
            ])->save();
        });

        return $status == Password::PASSWORD_RESET
            ? new JsonResponse()
            : new JsonResponse([
                'message' => __($status)
            ], Response::HTTP_UNPROCESSABLE_ENTITY) ;
    }
    public function forgot(R\ResetPasswordRequest $request)
    {
        $status = Password::sendResetLink($request->only('email'));

        return new JsonResponse($status);
    }
}
