<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Request;
use App\Models\User;
use Auth;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        $email = $request->get('email');
        $password = $request->get('password');

        if (Auth::attempt(['email' => $email, 'password' => $password], true)) {

            Auth::user()->update(['last_login_at' => now()]);

            return new JsonResponse(['message' => 'Login Successful']);
        }

        return new JsonResponse(['message' => 'Bad Credentials'], ResponseAlias::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function logout()
    {
        Auth::logout();

        return redirect('/');
    }
}
