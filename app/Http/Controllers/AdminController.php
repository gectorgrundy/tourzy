<?php

namespace App\Http\Controllers;

use App\Models\User;

class AdminController extends Controller
{
    public function home()
    {
        return view('admin.pages.home');
    }

    public function users()
    {
        return view('admin.pages.users');
    }

    public function user(User $user)
    {
        return view('admin.pages.user', compact('user'));
    }

    public function tours()
    {
        return view('admin.pages.tours');
    }

    public function payouts()
    {
        return view('admin.pages.payouts');
    }
}
