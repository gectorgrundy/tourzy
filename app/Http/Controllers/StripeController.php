<?php

namespace App\Http\Controllers;

use App\Http\Requests\Request;
use App\Models\Booking;
use App\Services\StripeService;
use Session;
use Stripe\PaymentIntent;
use Stripe\Stripe;
use Symfony\Component\HttpFoundation\JsonResponse;

class StripeController extends Controller
{
    private $stripeService;

    public function __construct(StripeService $stripeService)
    {
        $this->stripeService = $stripeService;
    }

    public function paymentProcess(Booking $booking)
    {
        $tour = $booking->tour;

        $amount = $tour->price * $booking->guest_count;

        return view('pages.stripe.index', compact('amount', 'booking'));
    }

    public function createPaymentIntent(Request $request)
    {
        $booking = Booking::query()->find($request->get('booking_id'));
        $tour = $booking->tour;

        $amount = floatval($tour->price * $booking->guest_count) * 100;

        Stripe::setApiKey(env('STRIPE_SECRET_KEY'));

        $intentData = [
            'amount' => $amount,
            'currency' => 'usd'
        ];

        try{
            $intent = PaymentIntent::create($intentData);

            $result = $intent->toArray();
            $result['publishableKey'] = env('STRIPE_PUBLISHABLE_KEY');

            $stripeData = $this->stripeService->getBookingStripeDataByBooking($booking);

            $stripeData->intent_data = $result;
            $stripeData->save();

            return new JsonResponse($result);

        }catch (\Exception $exception){
            return new JsonResponse([
                "success" => false,
                "message" => $exception->getMessage(),
                "trace" => $exception->getTraceAsString()
            ]);
        }
    }

    public function webHook(Request $request)
    {
        try{
            $data = $request->all();

            $this->stripeService->saveLog($data);

            if(isset($data['type'])){
                $type = $data['type'];

                if (strpos($type, 'charge.') !== false) {

                    $stripeData = $this->stripeService->getBookingStripeDataByStripeId($data['data']['object']['payment_intent']);

                    if(!$stripeData){
                        return new JsonResponse(['success' => true, 'message' => $data['id']." is not found"], 200);
                    }

                    $booking = $stripeData->booking;
                    $booking->status = $type === 'charge.succeeded' ? Booking::STATUS_SUCCESS : Booking::STATUS_FAILED;

                    $booking->save();

                    return new JsonResponse(['success' => true, 'id' => $stripeData->getId()]);
                } else{
                    return new JsonResponse(['success' => false, 'message' => $type." is not supported"], 200);
                }
            }

            return new JsonResponse(['success' => false, 'message' => "invalid request"], 200);
        }catch (\Exception $exception){
            return new JsonResponse(['trace' => $exception->getTrace(), "message" => $exception->getMessage()], 200);
        }
    }

    public function webHookDev(Request $request)
    {
        $paymentIntent = $request->get('payment_intent');

        $stripeData = $this->stripeService->getBookingStripeDataByStripeId($paymentIntent['id']);

        if(!$stripeData){
            return new JsonResponse(['success' => true, 'message' => $paymentIntent['id']." is not found"], 200);
        }

        $booking = $stripeData->booking;
        $booking->status = $paymentIntent['status'] === 'succeeded' ? Booking::STATUS_SUCCESS : Booking::STATUS_FAILED;

        $booking->save();

        Session::put('booking-congratulation', $booking->id);


        return new JsonResponse(['url' => route('my-bookings')]);
    }
}
