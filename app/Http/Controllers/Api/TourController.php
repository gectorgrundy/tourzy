<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Request;
use App\Http\Resources\Category\CategoryResource;
use App\Http\Resources\Tour\TourResource;
use App\Models\Booking;
use App\Models\Category;
use App\Models\Tour;
use App\Services\BookingService;
use App\Services\TourSaveService;
use App\Services\TourService;
use Illuminate\Http\JsonResponse;

class TourController extends Controller
{
    private $tourService;
    private $tourSaveService;
    private $bookingService;

    public function __construct(
        TourService $tourService,
        TourSaveService $tourSaveService,
        BookingService $bookingService
    )
    {
        $this->tourService = $tourService;
        $this->tourSaveService = $tourSaveService;
        $this->bookingService = $bookingService;
    }

    public function getMyTour()
    {
        $tour = \Auth::user()->currentTour;

        return new JsonResponse([
            'tour' => TourResource::make($tour),
            'categories' => CategoryResource::collection(Category::all())
        ]);
    }

    public function save(Request $request)
    {
        $tour = $this->tourSaveService->save($request);

        return new JsonResponse([
            'tour' => TourResource::make($tour),
            'url' => route('my-tour-list')
        ]);
    }

    public function search(Request $request)
    {
        $tours = $this->tourService->search($request);

        return new JsonResponse([
            'items' => $tours->getCollection()->map(function (Tour $tour){
               return [
                   'id' => $tour->id,
                   'name' => $tour->name,
                   'rating' => $tour->rating,
                   'duration' => $tour->duration,
                   'price' => $tour->price,
                   'city_location' => $tour->city_location,
                   'reviews_count' => $tour->reviews_count,
                   'category' => $tour->category,
                   'custom_category_title' => $tour->custom_category_title,
                   'cover_photo_src' => image_url($tour->cover_photo, 'small')
               ];
            }),
            'more' => $tours->currentPage() < $tours->lastPage()
        ]);
    }
}
