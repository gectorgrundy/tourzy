<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Request;
use App\Models\BankData;
use App\Models\Booking;
use App\Models\Payout;
use App\Models\Tour;
use App\Services\BookingService;
use App\Services\TourSaveService;
use App\Services\TourService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Str;
use DateTime;

class BookingController extends Controller
{
    private $bookingService;

    public function __construct(BookingService $bookingService)
    {
        $this->bookingService = $bookingService;
    }

    public function create(Request $request)
    {
        $user = \Auth::user();

        $tour = Tour::query()->find($request->get('tour_id'));
        $dateTime = DateTime::createFromFormat('Y-m-d H:i', "{$request->get('date')} {$request->get('time')}");

        $booking = new Booking();
        $booking->guest_count = $request->get('guest_count');
        $booking->date = $dateTime;
        $booking->time = $dateTime->format('H');
        $booking->status = Booking::STATUS_PENDING;
        $booking->activation_status = Booking::ACTIVATION_STATUS_PENDING;
        $booking->confirmation_token = Str::random(15);

        $booking->user_id = $user->id;
        $booking->tour_id = $tour->id;
        $booking->name = $tour->name;
        $booking->price = $tour->price;
        $booking->duration = $tour->duration;
        $booking->experience_description = $tour->experience_description;
        $booking->places_description = $tour->places_description;
        $booking->equip_required_type = $tour->equip_required_type;
        $booking->equipment_what_to_bring = $tour->equipment_what_to_bring;

        $booking->save();

        return new JsonResponse([
            'booking' => $booking,
        ]);
    }

    public function cancel(Request $request)
    {
        $booking = Booking::query()->find($request->get('id'));

        $payout = Payout::query()->create([
            'booking_id' => $booking->id,
            'user_id' => $booking->user_id,
            'type' => Payout::TYPE_USER_BOOKING_CANCEL,
            'amount' => $booking->price * $booking->guest_count,
            'status' => Payout::STATUS_PENDING,
            'cancel_reason' => $request->get('reason'),
            'cancel_message' => $request->get('message'),
        ]);

        BankData::query()->create([
            'payout_id' => $payout->id,
            'bank_name' => $request->get('bank_name'),
            'account_number' => $request->get('account_number'),
            'routing_number' => $request->get('routing_number'),
            'account_holder_name' => $request->get('account_holder_name'),
        ]);

        $booking->activation_status = Booking::ACTIVATION_STATUS_CANCELED;
        $booking->cancel_reason = $request->request->get('reason');
        $booking->cancel_message = $request->request->get('message');

        $booking->save();

        return new JsonResponse(['success' => true]);
    }

    public function activate(Request $request)
    {
        $token = $request->get('code');
        $booking = Booking::query()->find($request->get('id'));

        if ($booking && $booking->confirmation_token === $token && $booking->tour->user_id === \Auth::id()) {
            $this->bookingService->activate($booking);

            return new JsonResponse([
                'success' => true,
            ]);
        }

        return new JsonResponse([
            'success' => false,
        ]);
    }
}
