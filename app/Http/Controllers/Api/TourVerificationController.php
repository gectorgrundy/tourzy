<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Request;
use App\Http\Resources\TourVerification\TourVerificationResource;
use App\Models\Tour;
use App\Models\TourVerification;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;

class TourVerificationController extends Controller
{
    public function index()
    {
        $tour = \Auth::user()->currentTour;

        $verification = $tour->verification ?? $tour->verification()->create([])->fresh();

        return new JsonResponse(TourVerificationResource::make($verification));
    }

    public function save(Request $request)
    {
        $tour = \Auth::user()->currentTour;
        $verification = $tour->verification;

        $verification->type = $request->get('type');
        $verification->submitted_at = Carbon::now();
        $verification->status = TourVerification::STATUS_PENDING;

        $verification->photos()->detach($verification->photos->pluck('id'));

        $verification->photos()->sync([
            TourVerification::PHOTO_TYPE_FRONT_INDEX => $request->get('front_photo')['id'],
            TourVerification::PHOTO_TYPE_BACK_INDEX => $request->get('back_photo')['id'],
            TourVerification::PHOTO_TYPE_WITH_ID_INDEX => $request->get('with_id_photo')['id'],
        ]);

        $tour->status = Tour::STATUS_FINISHED;

        $tour->save();
        $verification->save();

        return new JsonResponse([
            'url' => route('my-tour-list')
        ]);
    }
}
