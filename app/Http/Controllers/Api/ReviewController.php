<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Request;
use App\Http\Resources\Category\CategoryCollection;
use App\Http\Resources\Category\CategoryResource;
use App\Http\Resources\File\FileResource;
use App\Http\Resources\Tour\TourResource;
use App\Models\Category;
use App\Models\File;
use App\Models\Review;
use App\Services\BookingService;
use App\Services\ReviewService;
use App\Services\TourSaveService;
use Illuminate\Http\JsonResponse;
use Storage;

class ReviewController extends Controller
{
    private $reviewService;

    public function __construct(ReviewService $reviewService)
    {
        $this->reviewService = $reviewService;
    }

    public function index(Request $request)
    {
        $result = $this->reviewService->getReviewsByTour($request->get('tour_id'), $request->get('page'));

        return new JsonResponse([
            'reviews' => $result->getCollection()->map(function (Review $review){
                return [
                    'id' => $review->id,
                    'rating' => $review->rating,
                    'text' => $review->text,
                    'created_at' => $review->created_at->format('d M Y'),
                    'user' => [
                        'first_name' => $review->user->first_name,
                        'last_name' => $review->user->last_name,
                        'avatar' => $review->user->avatar,
                    ]
                ];
            }),
            'more' => $result->currentPage() < $result->lastPage()
        ]);
    }
}
