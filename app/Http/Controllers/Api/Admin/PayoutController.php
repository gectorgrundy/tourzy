<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Request;
use App\Http\Resources\TourVerification\TourVerificationResource;
use App\Models\Booking;
use App\Models\Payout;
use App\Models\TourVerification;
use App\Models\User;
use App\Services\BookingService;
use App\Services\ReviewService;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;

class PayoutController extends Controller
{
    private $bookingService;

    public function __construct(BookingService $bookingService)
    {
        $this->bookingService = $bookingService;
    }

    public function getList(Request $request)
    {
        $result = Payout::with('user', 'booking', 'bank_data')
            ->whereIn('type', [Payout::TYPE_USER_BOOKING_CANCEL, Payout::TYPE_HOST_WITHDRAWAL])
            ->when($statusFilter = $request->get('statusFilter'), function (Builder $query) use ($statusFilter) {
                return $query->where('status', $statusFilter);
            })
            ->orderBy($request->get('sortParam'), $request->get('sortBy'))
            ->paginate(10);

        return new JsonResponse([
            'items' => $result->getCollection()->map(function (Payout $payout) {
                return [
                    'id' => $payout->id,
                    'amount' => $payout->amount,
                    'type' => $payout->type,
                    'status' => $payout->status,
                    'cancel_reason' => $payout->cancel_reason,
                    'cancel_message' => $payout->cancel_message,
                    'created_at' => $payout->created_at,
                    'user' => $payout->user,
                    'bank_data' => $payout->bank_data,
                    'money_back' => $this->bookingService->calculateMoneyBack($payout->booking),
                ];
            }),
            'total' => $result->total()
        ]);
    }

    public function hostWithdrawalAccept(Request $request)
    {
        $payout = Payout::query()->find($request->get('id'));

        $payout->user->balance = $payout->user->balance - $payout->amount;
        $payout->status = Payout::STATUS_SUCCESS;

        $payout->user->save();
        $payout->save();

        return new JsonResponse();
    }

    public function hostWithdrawalDecline(Request $request)
    {
        $payout = Payout::query()->find($request->get('id'));

        $payout->status = Payout::STATUS_DECLINE;
        $payout->decline_message = $request->get('reason');
        $payout->save();

        return new JsonResponse();
    }

    public function cancelBookingAccept(Request $request)
    {
        $payout = Payout::query()->find($request->get('id'));
        $booking = $payout->booking;
        $user = $booking->user;

        $earningPayout = Payout::query()->create([
            'type' => Payout::TYPE_HOST_EARNING,
            'amount' => ($booking->price * $booking->guest_count) * 40 / 100,
            'booking_id' => $booking->id,
            'user_id' => $user->id,
            'status' => Payout::STATUS_SUCCESS,
        ]);

        $payout->status = Payout::STATUS_SUCCESS;
        $payout->save();

        $booking->status = Booking::STATUS_CANCELED;
        $booking->finished_at = now();
        $booking->save();

        $user->balance = $user->balance + $earningPayout->amount;
        $user->save();

        return new JsonResponse();
    }

    public function cancelBookingDecline(Request $request)
    {
        $payout = Payout::query()->find($request->get('id'));

        $payout->booking->activation_status = Booking::ACTIVATION_STATUS_PENDING;
        $payout->booking->save();

        $payout->status = Payout::STATUS_DECLINE;
        $payout->decline_message = $request->get('reason');
        $payout->save();

        return new JsonResponse();
    }
}
