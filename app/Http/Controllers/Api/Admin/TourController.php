<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Request;
use App\Models\Tour;
use Illuminate\Http\JsonResponse;

class TourController extends Controller
{
    public function getList(Request $request)
    {
        $result = Tour::with('user')
            ->orderBy($request->get('sortParam'), $request->get('sortBy'))
            ->paginate(10);

        return new JsonResponse([
            'items' => $result->items(),
            'total' => $result->total()
        ]);
    }

    public function approve(Request $request)
    {
        $tour = Tour::query()->find($request->get('id'));

        $tour->verification_status = Tour::VERIFICATION_STATUS_ACCEPTED;
        $tour->save();

        return new JsonResponse(['status' => $tour->status]);
    }

    public function decline(Request $request)
    {
        $tour = Tour::query()->find($request->get('id'));

        $tour->verification_status = Tour::VERIFICATION_STATUS_DECLINED;
        $tour->decline_message = $request->get('reason');
        $tour->save();

        return new JsonResponse(['status' => $tour->status]);
    }
}
