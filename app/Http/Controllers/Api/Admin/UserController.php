<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Request;
use App\Http\Resources\TourVerification\TourVerificationResource;
use App\Models\TourVerification;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;

class UserController extends Controller
{
    public function getList(Request $request)
    {
        $result = User::query()
            ->select(
                'users.id',
                'users.email',
                'users.first_name',
                'users.last_name',
                'users.is_blocked',
                'users.created_at',
                'verification.id as verification_id',
                'verification.status as verification_status',
                'verification.submitted_at as verification_submitted_at',
            )
            ->leftJoin('tours as tour', 'users.id', '=', 'tour.user_id')
            ->leftJoin('tour_verifications as verification', 'tour.id', '=', 'verification.tour_id')
            ->when($statusFilter = $request->get('statusFilter'), function (Builder $query) use ($statusFilter){
                return $query->where('verification.status', $statusFilter);
            })
            ->when($emailFilter = $request->get('emailFilter'), function (Builder $query) use ($emailFilter){
                return $query->where('users.email','like', "%$emailFilter%");
            })
            ->orderBy($request->get('sortParam'), $request->get('sortBy'))
            ->paginate(10);

        return new JsonResponse([
            'items' => $result->items(),
            'total' => $result->total()
        ]);
    }

    public function getSingle(Request $request)
    {
        $user = User::query()->find($request->get('id'));
        $tourVerification = data_get($user->currentTour, 'verification');

        return new JsonResponse([
            'id' => $user->id,
            'phone' => $user->phone,
            'email' => $user->email,
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'is_blocked' => $user->is_blocked,
            'last_login_at' => $user->last_login_at,
            'created_at' => $user->created_at,
            'day_of_birth' => $user->day_of_birth,
            'month_of_birth' => $user->month_of_birth,
            'year_of_birth' => $user->year_of_birth,
            'avatar' => $user->avatar,
            'verification' => TourVerificationResource::make($tourVerification)
        ]);
    }

    public function toggleBlock(Request $request)
    {
        $user = User::query()->find($request->get('id'));

        if (!$user->is_blocked) {
            $user->block_message = $request->get('reason');
        }

        $user->is_blocked = !$user->is_blocked;
        $user->save();

        return new JsonResponse();
    }

    public function verificationApprove(Request $request)
    {
        $user = User::query()->find($request->get('id'));

        $tourVerification = data_get($user->currentTour, 'verification');

        $tourVerification->status = TourVerification::STATUS_ACCEPTED;
        $tourVerification->save();

        return new JsonResponse(['status' => $tourVerification->status]);
    }

    public function verificationDecline(Request $request)
    {
        $user = User::query()->find($request->get('id'));

        $tourVerification = data_get($user->currentTour, 'verification');

        $tourVerification->status = TourVerification::STATUS_DECLINED;
        $tourVerification->decline_message = $request->get('reason');
        $tourVerification->save();

        return new JsonResponse(['status' => $tourVerification->status]);
    }
}
