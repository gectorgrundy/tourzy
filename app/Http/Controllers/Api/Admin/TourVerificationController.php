<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Request;
use App\Http\Resources\TourVerification\TourVerificationResource;
use App\Models\TourVerification;
use App\Models\User;
use Illuminate\Http\JsonResponse;

class TourVerificationController extends Controller
{
    public function approve(Request $request)
    {
        $tourVerification = TourVerification::query()->find($request->get('id'));

        $tourVerification->status = TourVerification::STATUS_ACCEPTED;
        $tourVerification->save();

        return new JsonResponse([
            'status' => $tourVerification->status
        ]);
    }

    public function decline(Request $request)
    {
        $tourVerification = TourVerification::query()->find($request->get('id'));

        $tourVerification->status = TourVerification::STATUS_DECLINED;
        $tourVerification->decline_message = $request->get('reason');
        $tourVerification->save();

        return new JsonResponse([
            'status' => $tourVerification->status
        ]);
    }
}
