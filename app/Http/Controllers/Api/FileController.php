<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Request;
use App\Http\Resources\File\FileResource;
use App\Models\File;
use App\Services\FileService;
use Illuminate\Http\JsonResponse;
use Auth;

class FileController extends Controller
{
    private $fileService;

    public function __construct(FileService $fileService)
    {
        $this->fileService = $fileService;
    }

    public function index()
    {
        return new JsonResponse(FileResource::collection(\Auth::user()->files));
    }

    public function upload(Request $request)
    {
        $file = $request->file('file');
        $extension = $file->getClientOriginalExtension();
        $filename = $this->fileService->uploadFile($request->file('file'), $extension, Auth::id());
        $file =  File::query()->create([
            'user_id' => \Auth::id(),
            'name' => $file->getClientOriginalName(),
            'extension' => $extension,
            'size' => $file->getSize(),
            'filename' => $filename,
        ]);

        return new JsonResponse(FileResource::make($file));
    }

    public function delete(Request $request)
    {
        $file = File::find($request->get('id'));
        $file->delete();

        return new JsonResponse();
    }
}
