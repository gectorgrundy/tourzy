<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Request;
use App\Http\Resources\File\FileResource;
use App\Mail\ContactHost;
use App\Models\Tour;
use Illuminate\Http\JsonResponse;

class UserController extends Controller
{
    public function index()
    {
        $user = \Auth::user()->load('avatar');

        return new JsonResponse([
            'email' => $user->email,
            'phone' => $user->phone,
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'gender' => $user->gender,
            'day_of_birth' => $user->day_of_birth,
            'month_of_birth' => $user->month_of_birth,
            'year_of_birth' => $user->year_of_birth,
            'avatar' => $user->avatar ? FileResource::make($user->avatar) : null
        ]);
    }

    public function save(Request $request)
    {
        $user = \Auth::user();
        $avatar = $request->get('avatar');

        $user->update([
            'email' => $request->get('email'),
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'gender' => $request->get('gender'),
            'phone' => $request->get('phone'),
            'day_of_birth' => $request->get('day_of_birth'),
            'month_of_birth' => $request->get('month_of_birth'),
            'year_of_birth' => $request->get('year_of_birth'),
            'avatar_id' => $avatar ? $avatar['id'] : null
        ]);

        return new JsonResponse();
    }

    public function contactHost(Request $request)
    {
        $tour = Tour::query()->find($request->get('tour_id'));

        \Mail::to($tour->user)
            ->send(new ContactHost($request->get('message')));

        return new JsonResponse();
    }
}
