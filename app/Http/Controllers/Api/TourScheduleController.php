<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Request;
use App\Http\Resources\TourVerification\TourVerificationResource;
use App\Models\Booking;
use App\Models\Tour;
use App\Models\TourVerification;
use App\Services\TourService;
use Carbon\Carbon;
use DateTime;
use Illuminate\Http\JsonResponse;

class TourScheduleController extends Controller
{
    private $tourService;

    public function __construct(TourService $tourService)
    {
        $this->tourService = $tourService;
    }

    public function index()
    {
        $tour = current_tour();
        $schedule = $tour->schedule;

        $scheduleData = [];

        if ($schedule) {
            $bookings = $tour->bookings->filter(function (Booking $booking) {
                return $booking->activation_status === Booking::ACTIVATION_STATUS_PENDING &&  $booking->status === Booking::STATUS_SUCCESS;
            });

            $scheduleData = collect($tour->schedule->data)
                ->filter(fn($item) => new DateTime('yesterday') < new DateTime($item['date']))
                ->map(function ($date) use ($bookings) {

                    $dateBookings = $bookings->filter(function (Booking $booking) use ($date) {
                        return $booking->date->format('Y-m-d') === $date['date'];
                    });

                    $timeSlots = collect($date['scheduledTimeSlots']);

                    return [
                        'date' => $date['date'],
                        'scheduledTimeSlots' => $timeSlots->map(function ($time) use ($dateBookings) {
                            return [
                                'time' => $time,
                                'guests' => $dateBookings
                                    ->filter(function (Booking $booking) use ($time) {
                                        return $booking->time === $time;
                                    })
                                    ->map(function (Booking $booking) {
                                        return [
                                            'booking_id' => $booking->id,
                                            'guestName' => $booking->user->first_name . " " . $booking->user->last_name,
                                            'additionalGuestsCount' => $booking->guest_count - 1,
                                            'contactEmail' => $booking->user->email,
                                            'contactPhone' => $booking->user->phone_number,
                                        ];
                                    })
                                    ->values()
                            ];
                        })
                    ];
                })->values();
        }

        return new JsonResponse($scheduleData);
    }

    public function save(Request $request)
    {
        $tour = current_tour();
        $schedule = $tour->schedule;

        $data = collect($request->get('schedules'))
            ->filter(fn($item) => count($item['scheduledTimeSlots']))
            ->map(function ($item) {
                $timeSlots = collect($item['scheduledTimeSlots'])->map(fn($timeSlot) => $timeSlot['time']);

                return [
                    'date' => $item['date'],
                    'scheduledTimeSlots' => $timeSlots->toArray()
                ];
            })
            ->values();

        if (!$schedule) {
            $schedule = $tour->schedule()->create([]);
        }

        if ($data->count()) {
            $schedule->date_from = new DateTime(min($data->map(fn($timeSlot) => $timeSlot['date'])->toArray()));
            $schedule->date_to = new DateTime(max($data->map(fn($timeSlot) => $timeSlot['date'])->toArray()));
        }

        $schedule->data = $data->toArray();
        $schedule->save();

        return new JsonResponse();
    }
}
