<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Request;
use App\Models\BankData;
use App\Models\Payout;
use Illuminate\Http\JsonResponse;

class PayoutController extends Controller
{
    public function withdrawal(Request $request)
    {
        $payout = Payout::query()->create([
            'user_id' => \Auth::id(),
            'amount' => $request->get('withdraw_sum'),
            'type' => Payout::TYPE_HOST_WITHDRAWAL,
            'status' => Payout::STATUS_PENDING
        ]);

        BankData::query()->create([
            'payout_id' => $payout->id,
            'bank_name' => $request->get('bank_name'),
            'account_number' => $request->get('account_number'),
            'routing_number' => $request->get('routing_number'),
            'account_holder_name' => $request->get('account_holder_name'),
        ]);

        return new JsonResponse();
    }
}
