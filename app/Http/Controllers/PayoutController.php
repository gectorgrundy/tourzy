<?php

namespace App\Http\Controllers;

use App\Models\Payout;
use Auth;

class PayoutController extends Controller
{
    public function myPayments()
    {
        $user = Auth::user();

        $payouts = Payout::query()
            ->where('user_id', $user->id)
            ->whereIn('type', [Payout::TYPE_HOST_EARNING, Payout::TYPE_HOST_WITHDRAWAL])
            ->get();

        $pendingWithdrawalSum = \DB::table('payouts')->selectRaw('SUM(amount) as value')
            ->where('user_id', $user->id)
            ->where('status', Payout::STATUS_PENDING)
            ->where('type', Payout::TYPE_HOST_WITHDRAWAL)
            ->first();

        $availableAmount = $user->balance - $pendingWithdrawalSum->value;

        return view('pages.my-payments.index', compact('payouts', 'availableAmount'));
    }
}
