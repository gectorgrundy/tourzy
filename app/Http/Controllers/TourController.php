<?php

namespace App\Http\Controllers;

use App\Http\Requests\Request;
use App\Models\Booking;
use App\Services\ReviewService;
use App\Services\TourService;
use App\Models\Tour;
use Auth, DB;
use Session;

class TourController extends Controller
{
    private $tourService;
    private $reviewService;

    public function __construct(TourService $tourService, ReviewService $reviewService)
    {
        $this->tourService = $tourService;
        $this->reviewService = $reviewService;
    }

    public function search(Request $request)
    {
        $tours = $this->tourService->search($request);

        $minPrice = DB::select('SELECT min(price) as value FROM tours')[0]->value;
        $maxPrice = DB::select('SELECT max(price) as value  FROM tours')[0]->value;

        $minDate = DB::select('SELECT min(date_from) as value  FROM tour_schedules')[0]->value;
        $maxDate = DB::select('SELECT max(date_to) as value  FROM tour_schedules')[0]->value;

        return view('pages.search.index', [
            'tours' => $tours,
            'minPrice' => $minPrice,
            'maxPrice' => $maxPrice,
            'minDate' => $minDate,
            'maxDate' => $maxDate,
        ]);
    }

    public function host()
    {
        $tour = Auth::user()->current_tour;

        if (!$tour) {
            return redirect(route('tour-instruction'));
        }

        if ($tour->status === Tour::STATUS_CREATING) {

            return redirect(route('my-tour-create'));
        }

        return redirect(route('my-tour-list'));
    }

    public function detail(Tour $tour)
    {
        $reviews = $this->reviewService->getReviewsByTour($tour->id);

        $tour->update([
            'visit_count' => $tour->visit_count + 1
        ]);

        return view('pages.tour-detail.index', [
            'tour' => $tour,
            'reviews' => $reviews,
            'scheduleData' => $this->tourService->getFormattedSchedule($tour)
        ]);
    }

    public function myTourDetail()
    {
        $tour = Auth::user()->currentTour;
        $reviews = $this->reviewService->getReviewsByTour($tour->id);

        return view('pages.tour-detail.index', [
            'tour' => $tour,
            'reviews' => $reviews,
            'scheduleData' => $this->tourService->getFormattedSchedule($tour)
        ]);
    }

    public function myTourCreate()
    {
        $user = Auth::user();
        $tour = $user->currentTour;

        if (!$tour) {
            $tour = new Tour();
            $tour->user_id = $user->id;
            $tour->status = Tour::STATUS_CREATING;

            $tour->save();
        } else {
            if ($tour->status == Tour::STATUS_VALIDATING_BY_VERIFY_ID) {
                return redirect(route('tour-verification'));
            }
            if ($tour->status == Tour::STATUS_FINISHED) {
                return redirect(route('tour-detail', ['id' => $tour->id]));
            }
        }

        return view('pages.my-tour.create');
    }

    public function myTourEdit()
    {
        return view('pages.my-tour.create');
    }

    public function payment(Request $request)
    {
        $payMethod = $request->query->get('pay_method');
        $bookingId = $request->query->get('booking_id');

        return redirect(route('stripe.payment-process', ['booking' => $bookingId]));
    }

}
