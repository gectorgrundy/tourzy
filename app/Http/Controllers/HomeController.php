<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Tour;
use App\Models\TourVerification;
use App\Services\BookingService;
use App\Services\ReviewService;
use App\Services\TourService;
use Illuminate\Database\Eloquent\Builder;

class HomeController extends Controller
{
    private $tourService;

    public function __construct(TourService $tourService)
    {
        $this->tourService = $tourService;
    }

    public function index()
    {
        if (\Auth::check()) {
            return redirect(route('search'));
        }

        $city = current_city();

        $baseQuery = $this->tourService->tourBaseQuery()->orderBy('tours.visit_count', 'desc');

        $popularTours = (clone $baseQuery)->where('tours.city_location', $city)->limit(4)->get();
        $restTours = (clone $baseQuery)->where('tours.city_location', $city)->whereNotIn('tours.id', $popularTours->pluck('id'))->limit(10)->get();

        if (!$popularTours->count()) {
            $popularTours = (clone $baseQuery)->limit(4)->get();
        }
        if (!$restTours->count()) {
            $restTours = (clone $baseQuery)->whereNotIn('tours.id', $popularTours->pluck('id'))->limit(10)->get();
        }

        $categories = Category::query()->whereHas('tours', function (Builder $query){
            return $query
                ->leftJoin('tour_verifications as verification', 'tours.id', '=', 'verification.tour_id')
                ->leftJoin('users as user', 'tours.user_id', '=', 'user.id')
                ->where('verification.status', TourVerification::STATUS_ACCEPTED)
                ->where('verification_status', Tour::VERIFICATION_STATUS_ACCEPTED)
                ->where('user.is_blocked', false);
        })->get();

        return view('pages.home.index', compact('popularTours', 'restTours', 'categories'));
    }
}
