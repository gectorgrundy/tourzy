<?php

namespace App\Http\Controllers;

use App\Http\Requests\Request;
use App\Models\Booking;
use App\Models\Review;
use App\Services\TourService;

class ReviewController extends Controller
{
    private $tourService;

    public function __construct(TourService $tourService)
    {
        $this->tourService = $tourService;
    }

    public function create(Booking $booking, Request $request)
    {
        Review::query()->create([
            'user_id' => \Auth::id(),
            'booking_id' => $booking->id,
            'rating' => $request->get('rating'),
            'text' => $request->get('review'),
        ]);

        $this->tourService->recalculateRating($booking->tour);

        \Session::flash('notification', [
            'text' => "Your review was successfully submitted. Thank you for your feedback!",
            'type' => 'success'
        ]);

        return redirect(route('my-booking', ['booking' => $booking->id]));
    }
}
