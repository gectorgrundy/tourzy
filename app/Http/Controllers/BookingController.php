<?php

namespace App\Http\Controllers;

use App\Http\Requests\Request;
use App\Models\Booking;
use App\Services\BookingService;
use App\Services\ReviewService;
use Illuminate\Database\Eloquent\Builder;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Auth;

class BookingController extends Controller
{
    private $bookingService;
    private $reviewService;

    public function __construct(BookingService $bookingService, ReviewService $reviewService)
    {
        $this->bookingService = $bookingService;
        $this->reviewService = $reviewService;
    }

    public function myBookings()
    {
        $user = Auth::user();

        $baseQuery = Booking::with('current_user_review')
            ->select('bookings.*')
            ->where('user_id', $user->id);

        $future = (clone $baseQuery)
            ->where('status', Booking::STATUS_SUCCESS)
            ->where(function (Builder $query) {
                $query->where('activation_status', Booking::ACTIVATION_STATUS_PENDING)
                    ->orWhere('activation_status', Booking::ACTIVATION_STATUS_CANCELED);
            })
            ->orderBy('created_at', 'desc')
            ->get();

        $past = (clone $baseQuery)
            ->addSelect(\DB::raw('CASE WHEN finished_at is null THEN "2000-01-01 00:00:00.000000" ELSE finished_at END as finished_order'))

            ->where(function (Builder $query) {
                $query
                    ->where(function (Builder $query) {
                        $query->where('status', Booking::STATUS_SUCCESS)
                            ->where('activation_status', Booking::ACTIVATION_STATUS_SUCCESS);
                    })
                    ->orWhere('status', Booking::STATUS_CANCELED)
                    ->orWhere('status', Booking::STATUS_OUTDATED);
            })
            ->orderBy('finished_order', 'DESC')
            ->get();

        return view('pages.my-purchased-tours.index', compact('future', 'past'));
    }

    public function myBooking(Booking $booking)
    {
        if ($booking->user->id !== Auth::id()) {
            return redirect(route('search'));
        }

        $reviews = $this->reviewService->getReviewsByTour($booking->tour_id);

        return view('pages.my-purchased-tour.index', [
            'booking' => $booking,
            'tour' => $booking->tour,
            'reviews' => $reviews,
            'moneyBackData' => $this->bookingService->calculateMoneyBack($booking),
        ]);
    }

    public function activate(Request $request)
    {
        $confirmationToken = $request->route('token');
        $user = \Auth::user();

        if (!$user) {
            return redirect(route('home'))->with('need-login');
        }

        $booking = Booking::query()->where('confirmation_token', $confirmationToken)->first();

        if ($booking && $booking->tour->user->id === $user->id) {

            $booking = $this->bookingService->activate($booking);

            \Session::flash('notification', [
                'text' => "Tour $booking->name  was successfully activated",
                'type' => 'success'
            ]);

            return redirect(route('tour-detail', ['tour' => $booking->tour_id]));
        }

        \Session::flash('notification', [
            'text' => "Tour $booking->name cannot be activated",
            'type' => 'success'
        ]);

        return redirect(route('tour-detail', ['tour' => $booking->tour_id]));
    }


    public function activationQRCode(Booking $booking)
    {
        header("Cache-Control: public");
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=tour_{$booking->tour->id}.png");
        header("Content-Type: application/zip");
        header("Content-Transfer-Encoding: binary");

        echo QrCode::format('png')
            ->size(230)->errorCorrection('H')
            ->generate(route('my-booking-activate', ['token' => $booking->confirmation_token] ));
    }
}
