<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class TourSchedule extends Model {

    protected $fillable = [
        'tour_id',
        'data',
        'status',
        'decline_message',
        'submitted_at',
    ];

    protected $casts = [
        'data' => 'json'
    ];

    public function photos():BelongsToMany
    {
        return $this->belongsToMany(File::class, 'tour_verification_photo');
    }
}