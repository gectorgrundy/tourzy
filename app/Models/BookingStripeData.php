<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BookingStripeData extends Model
{
    protected $fillable = [
        'booking_id',
        'intent_data',
        'data',
        'intent_at',
    ];

    protected $casts = [
        'intent_at' => 'datetime:Y-m-d H:i',
        'intent_data' => 'json',
        'data' => 'json'
    ];

    public function booking()
    {
        return $this->belongsTo(Booking::class);
    }
}