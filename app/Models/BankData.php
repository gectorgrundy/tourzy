<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BankData extends Model
{
    use HasFactory;

    protected $fillable = [
        'payout_id',
        'bank_name',
        'account_number',
        'routing_number',
        'account_holder_name',
    ];

    public function Payout()
    {
        return $this->belongsTo(Payout::class);
    }
}