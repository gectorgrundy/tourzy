<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payout extends Model
{
    const STATUS_PENDING = 1;
    const STATUS_SUCCESS = 2;
    const STATUS_DECLINE = 3;
    const STATUS_FAILED = 3;

    const TYPE_HOST_EARNING = 1;
    const TYPE_HOST_WITHDRAWAL = 2;
    const TYPE_USER_BOOKING_CANCEL = 3;

    protected $fillable = [
        'user_id',
        'booking_id',
        'amount',
        'type',
        'status',
        'cancel_reason',
        'cancel_message',
        'decline_message',
        'finished_at',
    ];

    protected $casts = [
        'finished_at' => 'datetime'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function booking()
    {
        return $this->belongsTo(Booking::class);
    }

    public function bank_data()
    {
        return $this->hasOne(BankData::class);
    }
}