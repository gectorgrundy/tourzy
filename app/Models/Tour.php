<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tour extends Model
{
    use  HasFactory;

    const EQUIP_TYPE_I_WILL_PROVIDE_EQUIPMENT = 1;
    const EQUIP_TYPE_DOES_NOT_EQUIPMENT = 2;
    const EQUIP_TYPE_REQUIRED = 3;

    const STATUS_CREATING = 0;
    const STATUS_VALIDATING_BY_VERIFY_ID = 1;
    const STATUS_VALIDATING_BY_ADMIN = 2;
    const STATUS_FINISHED = 3;

    const VERIFICATION_STATUS_PENDING = 0;
    const VERIFICATION_STATUS_ACCEPTED = 1;
    const VERIFICATION_STATUS_DECLINED = 2;

    protected $fillable = [
        'user_id',
        'category_id',
        'cover_photo_id',
        'status',
        'verification_status',
        'name',
        'youtube_video_link',
        'custom_category_title',
        'country_meet',
        'city_meet',
        'state_meet',
        'address_meet',
        'app_meet',
        'latitude',
        'longitude',
        'group_max_size',
        'group_min_age',
        'equip_required_type',
        'equipment_what_to_bring',
        'price',
        'duration',
        'city_location',
        'notes',
        'experience_description',
        'places_description',
        'directions_description',
        'additional_requirements',
        'has_alcohol',
        'decline_message',
        'step',
        'rating',
        'visit_count',
    ];

    protected $casts = [
        'has_alcohol' => 'boolean'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function cover_photo()
    {
        return $this->belongsTo(File::class, 'cover_photo_id');
    }

    public function verification()
    {
        return $this->hasOne(TourVerification::class);
    }

    public function schedule()
    {
        return $this->hasOne(TourSchedule::class);
    }

    public function gallery_photos()
    {
        return $this->belongsToMany(File::class, 'tour_gallery_photos');
    }

    public function bookings()
    {
        return $this->hasMany(Booking::class);
    }

    public function reviews()
    {
        return $this->hasManyThrough(Review::class, Booking::class);

    }
}