<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'avatar_id',
        'first_name',
        'last_name',
        'day_of_birth',
        'month_of_birth',
        'year_of_birth',
        'phone',
        'gender',
        'email',
        'password',
        'is_login_verified',
        'is_blocked',
        'last_login_at',
        'email_verified_at'
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
        'last_login_at' => 'datetime',
    ];

    public function avatar()
    {
        return $this->belongsTo(File::class, 'avatar_id');
    }

    public function tours()
    {
        return $this->hasMany(Tour::class);
    }

    public function files()
    {
        return $this->hasMany(File::class);
    }

    public function getHasFinishedTourAttribute()
    {
        return $this->tours()
            ->where('verification_status', Tour::VERIFICATION_STATUS_ACCEPTED)
            ->whereHas('verification', function (Builder $query){
                return $query->where('status', TourVerification::STATUS_ACCEPTED);
            })
            ->exists();
    }

    public function getCurrentTourAttribute()
    {
        return $this->tours()->first();
    }
}
