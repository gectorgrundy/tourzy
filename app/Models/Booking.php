<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    const STATUS_PENDING = 1;
    const STATUS_SUCCESS = 2;
    const STATUS_FAILED = 3;
    const STATUS_CANCELED = 4;
    const STATUS_OUTDATED = 5;

    const ACTIVATION_STATUS_PENDING = 1;
    const ACTIVATION_STATUS_SUCCESS = 2;
    const ACTIVATION_STATUS_FAILED = 3;
    const ACTIVATION_STATUS_CANCELED = 4;

    protected $fillable = [
        'tour_id',
        'user_id',
        'finished_at',
        'status',
        'activation_status',
        'guest_count',
        'date',
        'time',
        'confirmation_token',
        'cancel_message',
        'cancel_reason',

        'name',
        'price',
        'duration',
        'experience_description',
        'places_description',
        'equip_required_type',
        'equipment_what_to_bring',
    ];

    protected $casts = [
        'finished_at' => 'datetime:Y-m-d H:i',
        'date' => 'date'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function tour()
    {
        return $this->belongsTo(Tour::class);
    }

    public function reviews()
    {
        return $this->hasMany(Review::class);
    }

    public function current_user_review()
    {
        return $this->hasOne(Review::class)->where('user_id', \Auth::id());
    }
}