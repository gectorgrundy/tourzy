<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class TourVerification extends Model
{

    const TYPE_DRIVER_LICENSE = 1;
    const TYPE_PASSPORT = 2;
    const TYPE_IDENTITY_CARD = 3;

    const STATUS_PENDING = 1;
    const STATUS_ACCEPTED = 2;
    const STATUS_DECLINED = 3;

    const PHOTO_TYPE_FRONT_INDEX = 0;
    const PHOTO_TYPE_BACK_INDEX = 1;
    const PHOTO_TYPE_WITH_ID_INDEX = 2;

    protected $fillable = [
        'tour_id',
        'type',
        'status',
        'decline_message',
        'submitted_at',
    ];

    public function photos(): BelongsToMany
    {
        return $this->belongsToMany(File::class, 'tour_verification_photo');
    }
}