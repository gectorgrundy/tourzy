<?php

use App\Models\Booking;
use App\Models\Category;
use App\Models\Tour;
use App\Models\File;

if (!function_exists('current_city')) {

    function current_city()
    {
        $result = \Location::get();

        return data_get($result, 'cityName', data_get($result, 'city', 'Kuwait'));
    }
}
if (!function_exists('current_tour')) {

    function current_tour(): Tour
    {
        return \Auth::user()->currentTour;
    }
}

if (!function_exists('get_categories')) {

    function get_categories()
    {
        return Category::all()
            ->map(function (Category $category) {
                return [
                    'id' => $category->id,
                    'title' => $category->title
                ];
            })
            ->prepend(['id' => -1, 'title' => 'Others']);
    }
}
if (!function_exists('google_calendar_link')) {

    function google_calendar_link(Booking $booking)
    {
        $base = 'http://www.google.com/calendar/event?';

        /** @var \Carbon\Carbon $start */
        $start = $booking->date->setHour($booking->time);

        return $base . http_build_query([
                'action' => 'TEMPLATE',
                'text' => $booking->name,
                'location' => $booking->tour->city_meet . ' ' . $booking->tour->address_meet,
                'dates' => $start->format('Ymd\\THi\\0\\0') . '/' . $start->add($booking->duration . ' hour')->format('Ymd\\THi\\0\\0'),
            ], '', '&', \PHP_QUERY_RFC3986);
    }
}

if (!function_exists('image_url')) {

    function image_url(File $file = null, $size = null)
    {
        if (!$file) {
            return null;
        }

        $disk = Storage::disk('uploads');
        $destination_path = 'files/' . $file->user_id;
        $filename = $file->filename;
        $path = $destination_path . '/' . ($size ? "thumb-" . config("image.sizes.$size") . "-" : '') . $filename;

        return $disk->exists($path) ? $disk->url($path) : config('image.default.tour');
    }
}
